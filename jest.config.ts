import type { Config } from '@jest/types';

const config: Config.InitialOptions = {
  testEnvironment: 'jsdom',
  collectCoverage: true,
  collectCoverageFrom: ['!**/*reportWebVitals.ts'],
  coverageReporters: ['lcov'],
  coveragePathIgnorePatterns: ['node_modules', 'dist', '/.*json', '/*.config.js'],
  coverageDirectory: './coverage',
  testPathIgnorePatterns: ['node_modules', 'dist'],
  transform: {
    '^.+\\.(j|t)sx?$': 'babel-jest',
  },
  moduleNameMapper: {
    '\\.(css|scss)$': 'identity-obj-proxy',
  },
  setupFilesAfterEnv: ['<rootDir>/.jest/setupTests.js'],
};

export default config;
