import { rest } from 'msw';
import addLineResponse from './responses/addLine.json';
import shopNowCartResponse from './responses/cartWithLines.json';
import customerTokenDetailsResponse from './responses/customerTokenDetails.json';
import productResponse from './responses/devices.json';
import exchangeTokenResponse from './responses/exchangeTokenResponse.json';
import accessoriesResponse from './responses/iphone13-accessories.json';
import deviceProtectionResponse from './responses/optionalServicesResponse.json';
import secureTokenResponse from './responses/secureTokenResponse.json';
import shippingOptionsResponse from './responses/shippingOptionsResponse.json';
import profileAddressResponse from './responses/shippingProfileAddressResponse.json';
import submitOrderResponse from './responses/submit.order.response.json';
import {
  default as addAccessoriesResponse,
  default as catalogAddAccessoriesResponse,
} from './responses/tms.add.accessories.response.json';
import cartResponse from './responses/tms.cart.response.json';
import deleteAccessoriesResponse from './responses/tms.delete.accessories.response.json';
import deleteDeviceResponse from './responses/tms.delete.device.response.json';
import msisdnResponse from './responses/tms.msisdns.response.json';
import verifyAddressResponse from './responses/tms.verify.address.response.json';
import upgradeEligibilityResponse from './responses/upgradeEligibility.json';

export const handlers = [
  // rest.post('*/eligibility/upgrade', (req, res, ctx) => {
  //   return res(ctx.json(upgradeEligibilityResponse));
  // }),
  rest.post('*/token', (req, res, ctx) => {
    return res(ctx.json(exchangeTokenResponse));
  }),
  rest.get('*/secure-token/*', (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(secureTokenResponse));
  }),
  rest.post('*/extractCustomerToken', (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(customerTokenDetailsResponse));
  }),
  // rest.get('*/carts', (req, res, ctx) => {
  //   return res(ctx.json(cartResponse));
  // }),
  // rest.post('*/lines', (req, res, ctx) => {
  //   return res(ctx.json(addLineResponse));
  // }),

  // SHOP-NOW
  rest.post('*/eligibility/upgrade', (req, res, ctx) => {
    return res(ctx.json(upgradeEligibilityResponse));
  }),
  rest.post('*/token', (req, res, ctx) => {
    return res(ctx.json(exchangeTokenResponse));
  }),
  rest.get('*/secure-token/*', (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(secureTokenResponse));
  }),
  rest.get('*/carts', (req, res, ctx) => {
    return res(ctx.json(shopNowCartResponse));
  }),
  rest.post('*/lines', (req, res, ctx) => {
    return res(ctx.json(addLineResponse));
  }),

  // CATALOG
  rest.post(`*/devices`, (req, res, ctx) => {
    return res(ctx.json(productResponse));
  }),
  rest.post(`/cart/accessories`, (req, res, ctx) => {
    return res(ctx.json(catalogAddAccessoriesResponse));
  }),
  rest.get(`*/accessories*`, (req, res, ctx) => {
    return res(ctx.json(accessoriesResponse));
  }),
  rest.post(`*/cart/accessories*`, (req, res, ctx) => {
    return res(ctx.status(201));
  }),
  rest.delete(`*/cart/accessories*`, (req, res, ctx) => {
    return res(ctx.status(200));
  }),
  rest.post(`*/cart/devices`, (req, res, ctx) => {
    return res(ctx.status(200));
  }),

  // SUMMARY
  rest.get(`*/summary/services*`, (req, res, ctx) => {
    return res(ctx.json(deviceProtectionResponse));
  }),

  rest.post('*/summary/services*', (req, res, ctx) => {
    return res(ctx.status(201));
  }),

  // TRADE-IN

  // CART
  rest.get(`*/cart`, (req, res, ctx) => {
    return res(ctx.json(cartResponse));
  }),
  rest.post(`*/cart/accessories`, (req, res, ctx) => {
    return res(ctx.json(addAccessoriesResponse));
  }),
  rest.delete(`*/cart/accessories/:id`, (req, res, ctx) => {
    return res(ctx.json(deleteAccessoriesResponse));
  }),

  rest.delete(`*/cart/devices/:id`, (req, res, ctx) => {
    return res(ctx.json(deleteDeviceResponse));
  }),

  // CHECKOUT
  rest.post(`*/address/verfiy`, (req, res, ctx) => {
    return res(ctx.json(verifyAddressResponse));
  }),
  rest.post('*/orders/submit', (req, res, ctx) => {
    return res(ctx.json(submitOrderResponse));
  }),
  rest.get(`*/carts/*/quotes`, (req, res, ctx) => {
    return res(ctx.json(cartResponse));
  }),
  rest.get(`*profile/address*`, (req, res, ctx) => {
    return res(ctx.json(profileAddressResponse));
  }),
  rest.get(`*shipping/shipping-options*`, (req, res, ctx) => {
    return res(ctx.json(shippingOptionsResponse));
  }),
  rest.get(`*/secure-token/checkout-service`, (req, res, ctx) => {
    return res(ctx.json(secureTokenResponse));
  }),
  rest.get(`*/carts/*/quotes`, (req, res, ctx) => {
    return res(ctx.json(cartResponse));
  }),
  rest.get(`*/msisdns`, (req, res, ctx) => {
    return res(ctx.json(msisdnResponse));
  }),
  rest.post(`*/shipping/notification-preferences`, (req, res, ctx) => {
    return res(ctx.status(201));
  }),
  rest.post(`*/carts/*/notifications`, (req, res, ctx) => {
    return res(ctx.status(201));
  }),
  rest.post(`*/carts/*/consents`, (req, res, ctx) => {
    return res(ctx.status(201));
  }),
  rest.post(`*/carts/*/payments`, (req, res, ctx) => {
    return res(ctx.status(201));
  }),
  rest.post(`*shipping/shipping-options*`, (req, res, ctx) => {
    return res(ctx.status(200));
  }),
];
