/* eslint-disable @typescript-eslint/no-explicit-any */
export const setupMockStateStore = () => {
  const state = new Map<string, any>([
    ['app.channel', 'care'],
    [
      'app.repProfile',
      {
        profile: {
          repNTId: 'cdlegcaresuper',
          canReassign: false,
          dealerCode: '0000002',
          accountType: 'Default Category',
          repLastName: 'cdlegcaresuper',
          repFirstName: 'cdlegcaresuper',
          hasTeam: false,
          repStoreId: '',
          salesChannelCode: 'Support',
          salesSubChannelCode: 'Back Office and Support',
          subChannelCategory: 'Default Category',
          termsConditions: {
            accepted: false,
          },
          permissions: {
            roles: ['Legacy Super User Care'],
            defaultselectedRole: 'Legacy Super User Care',
          },
        },
        token:
          'eyJpbnRlcmFjdGlvbklkIjoiY2FyZWNkbGVnY2FyZXN1cGVyQU5PTjE2MTUzMjE5MzI2OTYiLCJ0eXAiOiJKV1QiLCJzZXNzaW9uSWQiOiIzZjlkMjBjNjYzYjgwZTFkZDViYTVmNzhiMzdlZDNlOSIsInN0b3JlSWQiOnsiZW1wdHkiOnRydWUsInByZXNlbnQiOmZhbHNlfSwiYWN0aXZpdHlJZCI6IjE2MTUzMjE5MzI2OTY5NDY5MzMyODg2MTc1NjMwMDAwIiwic2VuZGVySWQiOiJUTU8iLCJkZWFsZXJjb2RlIjoiMDAwMDAwMiIsInNjb3BlIjoiYW5vbnltb3VzLCBhc3Npc3RlZCwgbG9nZ2VkLWluLCBlcGNEYXRhLCBsb2ciLCJhcHBsaWNhdGlvblVzZXJJZCI6ImNkbGVnY2FyZXN1cGVyIiwiYXBwbGljYXRpb25JZCI6IkdMT0JBTE5BViIsImFsZyI6IkhTMjU2IiwiY2hhbm5lbElkIjoiY2FyZSIsIndvcmtmbG93SWQiOiJBUElfOTk4X1NlcnZlcl9DYWxsIn0.eyJzYWxlc1N1YkNoYW5uZWxDb2RlIjoiQmFjayBPZmZpY2UgYW5kIFN1cHBvcnQiLCJzdWIiOiJyZWJlbGxpb24tZW1zLXRva2VuLWdlbmVyYXRpb24tc3ViamVjdC04YXNkZjgzIiwiYWJmcyI6IjAxMTExMDEwMDAwMDEwMTAxMTEwMDAxMTExMTExMTExMDExMTExMTExMTAxMTEwMTExMTExMTExMTExMTExMTExMTAxMTExMTExMTAwMDAwMDExMDExMTExMTExMTExMTExMTExMTExMDExMTExMTExMTExMTExMTExMTExMTEwMTExMDEwMTEwMDAxMTExMTExMTExMTExMTAwMTExMTExMTExMTExMTExMDExMTAxMDExMTExMTAwMTAwMTAxMTExMTExMTExMTEwMDExMTAwMDExMTExMTExMDAwMDAwMDExMTExMTEwMDEwMDAxMDAxMDEwMTExMTAxMTAwMTEwMTExMTAwMDAxMTAwMDExMTAwMDExMTAwMDAwMDEwMDAwMDAwMDAwMDAwMDExMTExMTExMTExMTExMTExMTExMTEwMTExMTAwMDAwMDAwMDAwMDAwMDAwMDAwMTExMTExMTExMTExMTExMDExMDAxMTAxMTExMTEiLCJ1c2Vyc2VsZWN0ZWRyb2xlIjoiTGVnYWN5IFN1cGVyIFVzZXIgQ2FyZSIsInNhbGVzQ2hhbm5lbENvZGUiOiJTdXBwb3J0IiwiY3JtQ2hhbm5lbENvZGUiOiJDQVJFIiwidXNlcnJvbGUiOiJMZWdhY3kgU3VwZXIgVXNlciBDYXJlIiwiZXhwIjoxNjE1MzUwNzMzLCJzdWJDaGFubmVsQ2F0ZWdvcnkiOiJEZWZhdWx0IENhdGVnb3J5IiwidXNlcm5hbWUiOiJjZGxlZ2NhcmVzdXBlciJ9.-NRsz1KtyvMawZBaz-AJW_bnHJE7jq1qDu4ORWw6_nw',
        customerInFocus: '',
        customers: [],
        expires_in: 28800,
        tempInteractionString: 'fcfe91ac-3a62-4d40-9b4d-06dd4acad572d0c970ed-c350-4fa5-9c6f-f9936f438955',
        selectedRole: 'Legacy Super User Care',
        isNBAUser: true,
        expertAccessInfo: {
          errors: [],
          data: {
            expertInfoByNtId: {
              applicationAccess: {
                nextBestAction: true,
                showAutoMemos: true,
              },
              careHeirarchy: {
                firstName: null,
                lastName: null,
                companyName: null,
                siteName: null,
                region: null,
                lineOfBusiness: null,
                departmentName: null,
                communityName: 'Augusta_3',
                jobTitle: null,
                dealerCode: null,
                samsonID: null,
                streamlineID: null,
              },
            },
          },
          extensions: null,
          dataPresent: true,
        },
        extensions: null,
        dataPresent: true,
        communityId: 'Augusta_3',
      },
    ],
    [
      'app.oktaToken',
      'eyJraWQiOiI0Nzc1M2UzZi0zOGFjLTQ1ODQtZDcxYy0zZDgxYTE1Nzk5NWMiLCJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJBejR0MWdacmVDYTVHRTd5RmE1N0dwYUsyNWlwa2R0aiIsInJ0Ijoie1wic2VnbWVudGF0aW9uSWRcIjpcIlBPTEFSSVNcIn0iLCJkZWFsZXJDb2RlIjoiIiwiaXNzIjoiaHR0cHM6XC9cL3FhdDAxLmFwaS50LW1vYmlsZS5jb21cL29hdXRoMlwvdjYiLCJtYXN0ZXJEZWFsZXJDb2RlIjoiIiwiYXV0aFRpbWUiOiIxNjQ4MjIwMTk5ODEwIiwic3RvcmVJZCI6IiIsInVzbiI6IjcxMmUzNzZlLTI3NTItMGVkOS01MDBjLTBhODY3NzYwZWYwOCIsImF1ZCI6IkF6NHQxZ1pyZUNhNUdFN3lGYTU3R3BhSzI1aXBrZHRqIiwic2VuZGVySWQiOiIiLCJuYmYiOjE2NDgyMjAxOTksInNjb3BlIjoiIiwiYXBwbGljYXRpb25JZCI6IkFDVUkiLCJleHAiOjE2NDgyMjM3OTksImlhdCI6MTY0ODIyMDE5OSwiY2hhbm5lbElkIjoiIiwianRpIjoiNzQ4NGRhNmItNGM1MC0yNmRlLTZkNjEtNjI3ODc4NjE4NGQxIn0.IKPrnl6wpINgovsFBugCxUpbSFMdJnSeN71bO5UU0BqwtOgDcj6K7y3Fk_msCD5NB9hF1Q1y4h4FkgKVGdho0q7uQ70zW707H6KYDk85szTv89RTNMfpPq9hfDtuH9XIcIWpsIZasA_hKYyabG2WPym_6uM7Q9AjHNWL3XpOB_QB0VRg55n9iiV6bX9636aRjkkE4-18VY7bN84w7yTBS81SF_cA8S1MbY5k1Ds6t8Enllb9OK5BCqVvJB3R74DTOtJoCG7xx_ZqRP6FkM10KIGrwAi_qXJd0jVW1Tptl77yfsV6lIyZoR75sUJkR2dl7aXKUFfcbWcMVqINzFDfSA',
    ],
    ['rep.activeaccountId', '618231531'],
    // ['app.config', { atlasMicroAppGatewayUrl: 'http://localhost:3001' }],
    ['rep.authToken', 'Bearer token'], // replace [token] with valid jwt if not running with mock service calls
    [
      'app.appsJson',
      {
        apps: [
          {
            path: 'shop-now',
            useAtlasAPIGWUrl: true,
          },
        ],
      },
    ],
    ['atlas-upgrades', { transaction_type: 'upgrades' }],
  ]);

  (globalThis as any).RepdashStore = {
    stateStore: {
      state,
      setState: (key: string, value: any) => state.set(key, value),
      getState: (key: string) => state.get(key),
      clearState: (key: string) => state.delete(key),
      isAtlasLiteEnabled: () => false,
      getCustomerInFocusToken: () => '',
      getGNInitialLoadContext: () => null,
      launchMicroAppInGlobalNav: () => null,
      getMigrationCustomerDetails: () => undefined,
      getActiveAccount: () => {
        return { zipCode: '30346' };
      },
      getExchangeToken: () =>
        'eyJraWQiOiI0Nzc1M2UzZi0zOGFjLTQ1ODQtZDcxYy0zZDgxYTE1Nzk5NWMiLCJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJBejR0MWdacmVDYTVHRTd5RmE1N0dwYUsyNWlwa2R0aiIsInJ0Ijoie1wic2VnbWVudGF0aW9uSWRcIjpcIlBPTEFSSVNcIn0iLCJkZWFsZXJDb2RlIjoiIiwiaXNzIjoiaHR0cHM6XC9cL3FhdDAxLmFwaS50LW1vYmlsZS5jb21cL29hdXRoMlwvdjYiLCJtYXN0ZXJEZWFsZXJDb2RlIjoiIiwiYXV0aFRpbWUiOiIxNjQ4MjIwMTk5ODEwIiwic3RvcmVJZCI6IiIsInVzbiI6IjcxMmUzNzZlLTI3NTItMGVkOS01MDBjLTBhODY3NzYwZWYwOCIsImF1ZCI6IkF6NHQxZ1pyZUNhNUdFN3lGYTU3R3BhSzI1aXBrZHRqIiwic2VuZGVySWQiOiIiLCJuYmYiOjE2NDgyMjAxOTksInNjb3BlIjoiIiwiYXBwbGljYXRpb25JZCI6IkFDVUkiLCJleHAiOjE2NDgyMjM3OTksImlhdCI6MTY0ODIyMDE5OSwiY2hhbm5lbElkIjoiIiwianRpIjoiNzQ4NGRhNmItNGM1MC0yNmRlLTZkNjEtNjI3ODc4NjE4NGQxIn0.IKPrnl6wpINgovsFBugCxUpbSFMdJnSeN71bO5UU0BqwtOgDcj6K7y3Fk_msCD5NB9hF1Q1y4h4FkgKVGdho0q7uQ70zW707H6KYDk85szTv89RTNMfpPq9hfDtuH9XIcIWpsIZasA_hKYyabG2WPym_6uM7Q9AjHNWL3XpOB_QB0VRg55n9iiV6bX9636aRjkkE4-18VY7bN84w7yTBS81SF_cA8S1MbY5k1Ds6t8Enllb9OK5BCqVvJB3R74DTOtJoCG7xx_ZqRP6FkM10KIGrwAi_qXJd0jVW1Tptl77yfsV6lIyZoR75sUJkR2dl7aXKUFfcbWcMVqINzFDfSA',
      getSelectedLines: () => [
        {
          lineId: '7209840134',
          subscriberName: 'Test A. Test',
          status: 'Active',
          productType: 'Voice',
        },
      ],
    },
  };
};
