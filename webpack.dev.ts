import CopyWebpackPlugin from 'copy-webpack-plugin';
import webpack, { Configuration as WebpackConfiguration } from 'webpack';
import { Configuration as WebpackDevServerConfiguration } from 'webpack-dev-server';
import { merge } from 'webpack-merge';
import commonConfig from './webpack.common';
// eslint-disable-next-line @typescript-eslint/no-var-requires
const { SERVICE_WORKER_BUILD_PATH } = require('msw/config/constants');

interface Configuration extends WebpackConfiguration {
  devServer?: WebpackDevServerConfiguration;
}

const devConfig: Configuration = merge(commonConfig, {
  // Set the mode to development or production
  mode: 'development',

  // Control how source maps are generated
  devtool: 'inline-source-map',

  // Spin up a server for quick development
  devServer: {
    historyApiFallback: true,
    open: true,
    compress: true,
    hot: true,
    port: 8086,
  },
  module: {
    rules: [
      // Styles: Inject CSS into the head with source maps
      {
        test: /\.(scss|css)$/,
        use: [
          'style-loader',
          {
            loader: 'css-loader',
            options: { sourceMap: true, importLoaders: 1 },
          },
          { loader: 'sass-loader', options: { sourceMap: true } },
        ],
      },
    ],
  },
  plugins: [
    new CopyWebpackPlugin({
      patterns: [{ from: SERVICE_WORKER_BUILD_PATH }],
    }),
    new webpack.DefinePlugin({
      CATALOG_URL: JSON.stringify('http://localhost:8081'),
      SUMMARY_URL: JSON.stringify('http://localhost:8082'),
      TRADE_IN_URL: JSON.stringify('http://localhost:8083'),
      CART_URL: JSON.stringify('http://localhost:8080'),
      PAYMENT_URL: JSON.stringify('https://tma-payments-access-management-atlas-dev02.apps.px-npe02b.cf.t-mobile.com/'),
    }),
  ],
});

export default devConfig;
