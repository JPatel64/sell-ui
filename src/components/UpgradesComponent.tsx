import { SAVE_AND_COMPLETE, storageUtil } from '@tmobile/atlas-util';
import { Steps } from 'primereact/steps';
import React, { useEffect, useRef, useState } from 'react';
import { IRoute } from '../models/routes';
import useStore from '../store';
import './UpgradesComponent.scss';

function UpgradesComponent() {
  let routes: IRoute[];
  const [activeIndex, setActiveIndex] = useState(0);
  const stateRef = useRef<number>(activeIndex);
  stateRef.current = activeIndex;
  const flowType = storageUtil.getTransactionType() || 'upgrade';

  if (flowType === 'upgrade') {
    routes = useStore((state) => state.upgradeRoutes);
  } else if (flowType === 'jump') {
    routes = useStore((state) => state.jumpRoutes);
  } else {
    routes = useStore((state) => state.jodRoutes);
  }

  routes.forEach((route) => {
    const routeIndex = routes.indexOf(route);

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    route.template = (item: any, options: any) => (
      <>
        <span
          className={`p-steps-number ${options.props.activeIndex > routeIndex ? 'p-step-completed' : ''} ${
            options.active ? 'p-step-active' : ''
          }`}
        >
          {options.props.activeIndex > routeIndex ? <i className='pi pi-check' /> : ''}
        </span>
        <span className='p-steps-title'>{item.name}</span>
      </>
    );
  });

  useEffect(() => {
    addEventListener(SAVE_AND_COMPLETE, () => handleComplete());
    return () => {
      // unsubscribe event
      removeEventListener(SAVE_AND_COMPLETE, () => handleComplete());
    };
  }, []);

  // Get content based on which step is active
  const getStepContent = (step: number) => {
    const route = routes[step];
    if (route) {
      const RouteComponent = route.ComponentName;
      return <RouteComponent></RouteComponent>;
    }
  };

  const handleComplete = () => {
    setActiveIndex(stateRef.current + 1);
  };

  return (
    <>
      <div>
        <h2 className='pl-4'>{routes[activeIndex].name}</h2>
        <Steps className='stepper' model={routes} activeIndex={activeIndex} readOnly={false} />
      </div>
      <br></br>

      <br></br>

      <div className='content'>{getStepContent(activeIndex)}</div>
    </>
  );
}

export default UpgradesComponent;
