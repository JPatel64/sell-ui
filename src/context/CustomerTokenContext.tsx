import { secureTokenService } from '@tmobile/atlas-util';
import React, { createContext, FC, useContext } from 'react';
import { useAsync } from 'react-async-hook';
import tokenApi from '../api/token';
import { ICustomerTokenDetails } from '../models/customer-token-details.model';
import { getAuthConfig } from '../util/auth.util';

interface ICustomerTokenContext {
  customerTokenDetails: ICustomerTokenDetails;
}

const CustomerTokenContext = createContext<ICustomerTokenContext>({
  customerTokenDetails: {} as ICustomerTokenDetails,
});

interface IProviderProps {
  children: React.ReactNode;
}

const getTokenDetails = async () => {
  await secureTokenService.getToken(getAuthConfig());
  return await tokenApi.getCustomerTokenDetails();
};

const CustomerTokenProvider: FC<IProviderProps> = ({ children }) => {
  const { error, loading, result: customerTokenDetails } = useAsync(getTokenDetails, []);

  return (
    <>
      <CustomerTokenContext.Provider value={{ customerTokenDetails }}>
        {!loading && <>{!error ? children : <p>Could not get token details</p>}</>}
      </CustomerTokenContext.Provider>
    </>
  );
};

const useCustomerTokenContext = () => {
  return useContext(CustomerTokenContext);
};

export { CustomerTokenContext, CustomerTokenProvider, useCustomerTokenContext };
