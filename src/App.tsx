import React from 'react';
import ShopNowView from './views/shop-now/ShopNowView';

const App = () => {
  return <ShopNowView />;
};

export default App;
