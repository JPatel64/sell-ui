export interface IAccessoryRequest {
  sku: string;
  offerId?: string;
  quantity: number;
  pricingOption: string;
}
