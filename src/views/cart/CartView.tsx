import React from 'react';
import CartReview from './components/cart-review/CartReview';

const CartView = () => {
  return <CartReview />;
};

export default CartView;
