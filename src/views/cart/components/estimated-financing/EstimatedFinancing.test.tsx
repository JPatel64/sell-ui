import { render, screen } from '@testing-library/react';
import React from 'react';
import mockCartData from '../../../../../mocks/responses/tms.cart.response.json';
import { formatCurrency } from '../../../../util/format.util';
import { ICart } from '../../interfaces/ICart';
import EstimatedFinancing from './EstimatedFinancing';

describe('Estimated Financing component', () => {
  it('should render without crashing', () => {
    const mockProps: ICart = mockCartData as unknown as ICart;

    render(<EstimatedFinancing cart={mockProps} estimatedFinancing={undefined} />);

    expect(screen.getByTestId('down-payment')).toHaveTextContent('Additional down payment');
    expect(screen.getByTestId('down-payment-amount')).toHaveTextContent(
      formatCurrency(mockProps.additionalDownPayment),
    );

    expect(screen.getByTestId('monthly-payment')).toHaveTextContent('Monthly payment');
    expect(screen.getByTestId('monthly-payment-amount')).toHaveTextContent(formatCurrency(mockProps.monthlyPayment));

    expect(screen.getByTestId('financed-amount')).toHaveTextContent('Total financed amount');
    expect(screen.getByTestId('financed-amount-value')).toHaveTextContent(formatCurrency(mockProps.financedAmount));

    expect(screen.getByTestId('Current-Equipment-Credit-limit')).toHaveTextContent('Current Equipment Credit limit');
    expect(screen.getByTestId('Current-Equipment-Credit-limit-amount')).toHaveTextContent(formatCurrency(0));

    expect(screen.getByTestId('Equipment-Credit-balance')).toHaveTextContent('Equipment Credit balance');
    expect(screen.getByTestId('Equipment-Credit-balance-amount')).toHaveTextContent(formatCurrency(0));

    expect(screen.getByTestId('Equipment-Credit-available')).toHaveTextContent('Equipment Credit available');
    expect(screen.getByTestId('Equipment-Credit-available-amount')).toHaveTextContent(formatCurrency(0));

    expect(screen.getByTestId('Line-contribution')).toHaveTextContent('Line contribution');
    expect(screen.getByTestId('Line-contribution-amount')).toHaveTextContent(formatCurrency(0));

    expect(screen.getByTestId('Maximum-potential-Equipment-Credit')).toHaveTextContent(
      'Maximum potential Equipment Credit',
    );
    expect(screen.getByTestId('Maximum-potential-Equipment-Credit-amount')).toHaveTextContent(formatCurrency(0));

    // uncomment these when we revisit monthly recurring charges section
    // expect(screen.getByTestId('Services-and-features')).toHaveTextContent('Services and features');
    // expect(screen.getByTestId('Services-and-features-amount')).toHaveTextContent(mockProps.servicesAndFeatures);

    // expect(screen.getByTestId('Discounts-and-credit')).toHaveTextContent('Discounts and credit');
    // expect(screen.getByTestId('Discounts-and-credit-amount')).toHaveTextContent(mockProps.discountsAndCredit);

    expect(screen.getByTestId('Total-due-monthly')).toHaveTextContent('Total due monthly');
    expect(screen.getByTestId('Total-due-monthly-amount')).toHaveTextContent(formatCurrency(mockProps.monthlyPayment));
  });
});
