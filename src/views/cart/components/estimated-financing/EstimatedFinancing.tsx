import { Accordion, AccordionTab } from 'primereact/accordion';
import React, { FunctionComponent } from 'react';
import {
  ADDITIONAL_DOWN_PAYMENT,
  CURRENT_EC_CREDIT_LIMIT,
  EC_AVAILABLE,
  EC_BALANCE,
  ESTIMATED_FINANCING,
  LINE_CONTRIBUTION,
  MAX_POTENTIAL_EC,
  MONTHLY_PAYMENT,
  TOTAL_DUE_MONTHLY,
  TOTAL_FINANCED_AMOUNT,
} from '../../../../util/app.constants';
import { formatCurrency } from '../../../../util/format.util';
import { Financing, ICart } from '../../interfaces/ICart';
import styles from './EstimatedFinancing.module.scss';

interface IEstimatedFinancingProps {
  cart: ICart | undefined;
  estimatedFinancing: Financing | undefined;
}

const EstimatedFinancing: FunctionComponent<IEstimatedFinancingProps> = ({ cart, estimatedFinancing }) => {
  return (
    <>
      {cart && (
        <div className={`${styles['estimated-financing-container']}`}>
          <Accordion collapseIcon={'pi pi-chevron-up'} expandIcon={'pi pi-chevron-down'} activeIndex={0}>
            <AccordionTab header={ESTIMATED_FINANCING}>
              <div data-testid='Current-Equipment-Credit-limit' className='p-mb-2'>
                {CURRENT_EC_CREDIT_LIMIT}
                <span data-testid='Current-Equipment-Credit-limit-amount' className={`${styles['financing-amount']}`}>
                  {formatCurrency(estimatedFinancing?.equipmentCredit.limit)}
                </span>
              </div>
              <div data-testid='Equipment-Credit-balance' className='p-mb-2'>
                {EC_BALANCE}
                <span data-testid='Equipment-Credit-balance-amount' className={`${styles['financing-amount']}`}>
                  {formatCurrency(estimatedFinancing?.equipmentCredit.balance)}
                </span>
              </div>
              <div data-testid='Equipment-Credit-available' className='p-mb-2'>
                {EC_AVAILABLE}
                <span data-testid='Equipment-Credit-available-amount' className={`${styles['financing-amount']}`}>
                  {formatCurrency(estimatedFinancing?.equipmentCredit.available)}
                </span>
              </div>
              <div data-testid='Line-contribution' className='p-mb-2'>
                {LINE_CONTRIBUTION}
                <span data-testid='Line-contribution-amount' className={`${styles['financing-amount']}`}>
                  {formatCurrency(estimatedFinancing?.equipmentCredit.lineContribution)}
                </span>
              </div>
              <div data-testid='Maximum-potential-Equipment-Credit' className='p-mb-2'>
                {MAX_POTENTIAL_EC}
                <span
                  data-testid='Maximum-potential-Equipment-Credit-amount'
                  className={`${styles['financing-amount']}`}
                >
                  {formatCurrency(estimatedFinancing?.equipmentCredit.maximumCredit)}
                </span>
              </div>
              <hr className={`${styles['financing-divider']}`} />
            </AccordionTab>
          </Accordion>
          <div className={`${styles['down-payment-container']} px-4`}>
            <div data-testid='down-payment' className='p-mb-2'>
              {ADDITIONAL_DOWN_PAYMENT}
              <span data-testid='down-payment-amount' className={`${styles['financing-amount']}`}>
                {formatCurrency(cart?.additionalDownPayment)}
              </span>
            </div>
            <div data-testid='monthly-payment' className='p-mb-2'>
              {MONTHLY_PAYMENT}
              <span data-testid='monthly-payment-amount' className={`${styles['financing-amount']}`}>
                {formatCurrency(cart?.monthlyPayment)}
              </span>
            </div>
            <div data-testid='financed-amount' className='p-mb-4'>
              {TOTAL_FINANCED_AMOUNT}
              <span data-testid='financed-amount-value' className={`${styles['financing-amount']}`}>
                {formatCurrency(cart?.financedAmount)}
              </span>
            </div>
          </div>

          {/* hiding monthly recurring charges until we figure out details on fetching this data */}
          {/* <Accordion activeIndex={0}>
				<AccordionTab header={HTMLConstants.MONTHLY_RECURRING_CHARGES}>
					<div data-testid="Services-and-features" className="p-mb-2">
						{HTMLConstants.SERVICES_AND_FEATURES}
						<span data-testid="Services-and-features-amount" className="financing-amount">
							{formatCurrency(estimatedFinancing.servicesAndFeatures)}
						</span>
					</div>
					<div data-testid="Discounts-and-credit" className="p-mb-2">
						{HTMLConstants.DISCOUNTS_AND_CREDITS}
						<span data-testid="Discounts-and-credit-amount" className="financing-amount">
							{formatCurrency(estimatedFinancing.discountsAndCredit)}
						</span>
					</div>
				</AccordionTab>
			</Accordion>
			<hr className={`${styles['financing-divider']}`} /> */}
          <hr className={`${styles['financing-total-divider']}`} />
          <div data-testid='Total-due-monthly' className={`${styles['total-due']} p-mb-2`}>
            {TOTAL_DUE_MONTHLY}
            <span data-testid='Total-due-monthly-amount' className={`${styles['financing-amount']}`}>
              {formatCurrency(cart?.monthlyPayment)}
            </span>
          </div>
        </div>
      )}
    </>
  );
};

export default EstimatedFinancing;
