import { render, screen } from '@testing-library/react';
import React from 'react';
import ConfirmationModal from './ConfirmationModal';

describe('ConfirmationModal component', () => {
  it('renders without crashing', () => {
    render(
      <ConfirmationModal
        modalText={'text'}
        header={'header'}
        isVisible={true}
        handleContinueBtn={() => {
          return undefined;
        }}
        handleCancelBtn={() => {
          return undefined;
        }}
      />,
    );
    expect(screen.getByTestId('modalText')).toHaveTextContent('text');
    expect(screen.getByTestId('modalHeader')).toHaveTextContent('header');
  });
});
