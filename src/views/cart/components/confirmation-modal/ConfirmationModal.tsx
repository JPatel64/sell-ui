import { Dialog } from 'primereact/dialog';
import React, { FunctionComponent } from 'react';
import styles from './ConfirmationModal.module.scss';
interface IConfirmationModal {
  modalText: string;
  header: string;
  isVisible: boolean;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  handleContinueBtn: any;
  handleCancelBtn: (e: boolean) => void;
}

const ConfirmationModal: FunctionComponent<IConfirmationModal> = ({
  header,
  modalText,
  isVisible,
  handleContinueBtn,
  handleCancelBtn,
}) => {
  return (
    <>
      <Dialog showHeader={false} visible={isVisible} onHide={() => handleCancelBtn(false)}>
        <div className={`${styles['delete-device-Container']} pt-5 px-4`}>
          <img className={`${styles['tmo-icon']}`} src='../../assets/tmoIcon.png' alt='t-mobile icon' />
          <i className={`pi ${styles['pi-times']}`} onClick={() => handleCancelBtn(false)}></i>
          <h3 className='pt-3' data-testid='modalHeader'>
            {header}
          </h3>
          <p className='pt-3' data-testid='modalText'>
            {modalText}
          </p>
          <div className='col-12 buttons-container button-spacing'>
            <div className='button-box'>
              <button className='btn btn-secondary' type='button' onClick={() => handleCancelBtn(false)}>
                Cancel
              </button>
            </div>
            <div className='button-box'>
              <button
                className='btn btn-primary'
                type='button'
                onClick={() => {
                  handleContinueBtn();
                }}
              >
                Continue
              </button>
            </div>
          </div>
        </div>
      </Dialog>
    </>
  );
};

export default ConfirmationModal;
