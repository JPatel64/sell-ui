import { render, screen } from '@testing-library/react';
import React from 'react';
import mockCartData from '../../../../../mocks/responses/tms.cart.response.json';
import { formatCurrency } from '../../../../util/format.util';
import { ICart } from '../../interfaces/ICart';
import EstimatedDueToday from './EstimatedDueToday';

describe('Estimated Due Today component', () => {
  it('should render without crashing', () => {
    const mockProps: ICart = mockCartData as unknown as ICart;

    render(<EstimatedDueToday cart={mockProps} />);

    screen.findByDisplayValue('Estimated due today');
    expect(screen.getByTestId('subtotal-label')).toHaveTextContent('Subtotal (pre-tax)');
    expect(screen.getByTestId('subtotal-value')).toHaveTextContent(`${formatCurrency(mockProps.subTotal)}`);

    expect(screen.getByTestId('discounts-label')).toHaveTextContent('Discounts and credit');
    expect(screen.getByTestId('discounts-value')).toHaveTextContent(
      `(${formatCurrency(mockProps.credit.oneTimeCredit)})`,
    );

    // expect(screen.getByTestId('deposits-label')).toHaveTextContent('Deposits and charges');
    // expect(screen.getByTestId('deposits-value')).toHaveTextContent(`${formatCurrency(40)}`);

    expect(screen.getByTestId('tax-label')).toHaveTextContent('Tax');
    expect(screen.getByTestId('tax-value')).toHaveTextContent(`${formatCurrency(mockProps.tax?.total)}`);

    expect(screen.getByTestId('total-label')).toHaveTextContent('Total due today (estimated)');
    expect(screen.getByTestId('total-value')).toHaveTextContent(`${formatCurrency(mockProps.total)}`);
  });
});
