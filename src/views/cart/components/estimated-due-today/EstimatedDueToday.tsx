import React, { FunctionComponent, useEffect, useState } from 'react';
import {
  DEPOSITS_CHARGES,
  DISCOUNTS_AND_CREDITS,
  ESTIMATED_DUE_TODAY,
  SUBTOTAL_PRETAX,
  TAX,
  TOTAL_DUE_TODAY,
} from '../../../../util/app.constants';
import { formatCurrency } from '../../../../util/format.util';
import { ICart } from '../../interfaces/ICart';
import styles from './EstimatedDueToday.module.scss';
interface IEstimatedToday {
  cart: ICart | undefined;
}

const EstimatedDueToday: FunctionComponent<IEstimatedToday> = ({ cart }) => {
  const [totalCharges, setTotalCharges] = useState(0);

  useEffect(() => {
    let total = 0;
    cart?.lines?.forEach((line) =>
      line.charges?.forEach((charge) => {
        total += charge.amount;
      }),
    );

    setTotalCharges(total);
  }, [cart]);

  return (
    <>
      {cart && (
        <div className={`card p-px-3`} style={{ backgroundColor: '#ffffff', border: '1px solid #bebebe' }}>
          <div className='p-grid p-my-2'>
            <div className={`${styles['card-title']} p-col`}>{ESTIMATED_DUE_TODAY}</div>
          </div>
          <hr className='p-mt-0 p-mb-3'></hr>
          <div className='p-grid'>
            <div className='p-col' data-testid='subtotal-label'>
              {SUBTOTAL_PRETAX}
            </div>
            <div className='p-col p-text-right' data-testid='subtotal-value'>
              {formatCurrency(cart?.subTotal)}
            </div>
          </div>
          <div className='p-grid'>
            <div className='p-col' data-testid='discounts-label'>
              {DISCOUNTS_AND_CREDITS}
            </div>
            <div className='p-col p-text-right' data-testid='discounts-value'>
              ({formatCurrency(cart?.credit.oneTimeCredit)})
            </div>
          </div>
          <div className='p-grid'>
            <div className='p-col' data-testid='deposits-label'>
              {DEPOSITS_CHARGES}
            </div>
            <div className='p-col p-text-right' data-testid='deposits-value'>
              {formatCurrency(totalCharges)}
            </div>
          </div>
          <div className='p-grid'>
            <div className='p-col' data-testid='tax-label'>
              {TAX}
            </div>
            <div className='p-col p-text-right' data-testid='tax-value'>
              {formatCurrency(cart?.tax?.total)}
            </div>
          </div>
          <hr className='p-mb-0 p-mt-3'></hr>
          <div className={`${styles['total']} p-my-2 p-grid`}>
            <div className='p-col' data-testid='total-label'>
              {TOTAL_DUE_TODAY}
            </div>
            <div className='p-col p-text-right' data-testid='total-value'>
              {formatCurrency(cart?.total)}
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default EstimatedDueToday;
