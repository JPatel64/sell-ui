import { fireEvent, render, screen, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { GlobalNavUtil } from '@tmobile/atlas-util';
import React from 'react';
import CartReview from './CartReview';

describe('Cart review component', () => {
  beforeEach(() => {
    render(<CartReview />);
  });

  it('should return the initial quantity of accessories', async () => {
    await waitFor(() => {
      expect(screen.getByTestId('txtQtyge4tinjxmq2wkljvmm3dsljuge4tcllbme3dsllggyyteyjvgq3gcytghe=')).toHaveValue(3);
    });
  });

  it('should increase the quantity of accessories to 4', async () => {
    await screen.findByTestId('btnPlusge4tinjxmq2wkljvmm3dsljuge4tcllbme3dsllggyyteyjvgq3gcytghe=');
    userEvent.click(screen.getByTestId('btnPlusge4tinjxmq2wkljvmm3dsljuge4tcllbme3dsllggyyteyjvgq3gcytghe='));

    await waitFor(() => {
      expect(screen.getByTestId('txtQtyge4tinjxmq2wkljvmm3dsljuge4tcllbme3dsllggyyteyjvgq3gcytghe=')).toHaveValue(4);
    });
  });

  it('should decrease the quantity of accessories to 2', async () => {
    await screen.findByTestId('btnMinusge4tinjxmq2wkljvmm3dsljuge4tcllbme3dsllggyyteyjvgq3gcytghe=');
    userEvent.click(screen.getByTestId('btnMinusge4tinjxmq2wkljvmm3dsljuge4tcllbme3dsllggyyteyjvgq3gcytghe='));
    await waitFor(() => {
      expect(screen.getByTestId('txtQtyge4tinjxmq2wkljvmm3dsljuge4tcllbme3dsllggyyteyjvgq3gcytghe=')).toHaveValue(2);
    });
  });

  it('should launch checkout app on click of Checkout button', async () => {
    jest.spyOn(GlobalNavUtil, 'launchMicroApp');
    await screen.findByTestId('checkout-button');
    fireEvent.click(screen.getByTestId('checkout-button'));
    await waitFor(() => {
      expect(GlobalNavUtil.launchMicroApp).toHaveBeenCalledWith('checkout-app-atlas');
    });
  });

  it('should launch shop-now app on click of Cancel button', async () => {
    jest.spyOn(GlobalNavUtil, 'launchMicroApp');
    await screen.findByTestId('cancel-button');
    fireEvent.click(screen.getByTestId('cancel-button'));
    await waitFor(() => {
      expect(GlobalNavUtil.launchMicroApp).toHaveBeenCalledWith('shop-now');
    });
  });

  it('should launch shop-now app on click of upgrade another line button', async () => {
    jest.spyOn(GlobalNavUtil, 'launchMicroApp');
    await screen.findByTestId('upgrade-button');
    fireEvent.click(screen.getByTestId('upgrade-button'));
    await waitFor(() => {
      expect(GlobalNavUtil.launchMicroApp).toHaveBeenCalledWith('shop-now');
    });
  });
});
