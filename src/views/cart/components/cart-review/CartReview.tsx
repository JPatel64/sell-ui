import { http } from '@tmobile/atlas-util';
import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router';
import { CART_URL } from '../../../../util/app.constants';
import { getGatewayUrl } from '../../../../util/auth.util';
import { ICart, Line } from '../../interfaces/ICart';
import EstimatedDueToday from '../estimated-due-today/EstimatedDueToday';
import EstimatedFinancing from '../estimated-financing/EstimatedFinancing';
import LineBlade from '../lineBlades/LineBlade';
import styles from './CartReview.module.scss';

const CartReview = () => {
  const [cartData, setCartData] = useState<ICart | undefined>(undefined);
  const [isLoading, setIsLoading] = useState(true);
  const navigate = useNavigate();

  useEffect(() => {
    http
      .get<ICart>(`${getGatewayUrl()}${CART_URL}?includeTax=true`, {})
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      .then((res: any) => {
        setCartData(res.data);
        setIsLoading(false);
      })
      .catch((e) => console.error(e.message));
  }, []);

  const navigateToCheckout = () => {
    navigate('/checkout/shipping');
  };

  const navigateToShopNow = () => {
    navigate('/');
  };

  return (
    <div id={`${styles['cart-review']}`}>
      {!isLoading && ( // we'll probably want to add a global loading spinner in the future
        <div data-testid='cart-review'>
          {cartData?.lines.map((line: Line, index: number) => (
            <LineBlade key={index} line={line} cart={cartData} setCartData={setCartData} />
          ))}

          <div className={`${styles['financing-container']} p-grid p-mx-5`}>
            <div className='p-col'>
              <EstimatedFinancing cart={cartData} estimatedFinancing={undefined} />
            </div>

            <div className='p-col'>
              <EstimatedDueToday cart={cartData}></EstimatedDueToday>
            </div>
          </div>
          <div className={`${styles['buttons']} mt-4 mb-6`}>
            <button
              data-testid='cancel-button'
              className='btn btn-primary mr-6'
              type='button'
              onClick={() => navigateToShopNow()}
            >
              Cancel
            </button>
            <button
              data-testid='upgrade-button'
              className='btn btn-primary mr-6'
              type='button'
              onClick={() => navigateToShopNow()}
            >
              Upgrade another line
            </button>
            <button
              data-testid='checkout-button'
              className='btn btn-primary'
              type='button'
              onClick={() => navigateToCheckout()}
            >
              Checkout
            </button>
          </div>
        </div>
      )}
    </div>
  );
};

export default CartReview;
