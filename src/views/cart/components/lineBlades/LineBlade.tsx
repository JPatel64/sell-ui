import { http, storageUtil } from '@tmobile/atlas-util';
import { Dialog } from 'primereact/dialog';
import React, { FunctionComponent, useEffect, useState } from 'react';
import overrideIcon from '../../../../assets/override.svg';
import trashIcon from '../../../../assets/trash.svg';
import {
  ACCESSORIES_URL,
  ACCESSORY,
  CART_URL,
  CHARGES,
  DELETE_CONFIRMATION_PROMPT,
  DETAILS,
  DEVICES_URL,
  DEVICE_DELETED_PROMPT,
  DEVICE_PENDING_PROMPT,
  DISCARD_WARNING_PROMPT,
  DOWN_PAYMENT,
  DUE_MONTHLY,
  DUE_TODAY,
  EIP,
  FINANCE,
  FINANCED_AMOUNT,
  FULL_RETAIL_PRICE,
  LINES_URL,
  MONTH,
  MONTHS,
  NA,
  NAME,
  PRICE,
  PROTECTION,
  PROTECTION_DECLINE,
  QUANTITY,
  SUPPORT_FEE,
  TERM,
  TMOBILE,
  UPGRADE,
} from '../../../../util/app.constants';
import { getGatewayUrl } from '../../../../util/auth.util';
import { formatCurrency, formatMobileNumber } from '../../../../util/format.util';
import { IAccessoryRequest } from '../../interfaces/IAccessoryRequest';
import { Accessory, Device, ICart, Line } from '../../interfaces/ICart';
import ConfirmationModal from '../confirmation-modal/ConfirmationModal';
import styles from './LineBlade.module.scss';

interface ILineBladeProps {
  line: Line;
  cart: ICart;
  setCartData: (e: ICart) => void;
}

const LineBlade: FunctionComponent<ILineBladeProps> = ({ line, cart, setCartData }) => {
  const [devicesState, setDevicesState] = useState<Device[]>([]);
  const [accessoriesState, setAccessoriesState] = useState<Accessory[][]>([]);
  const [isLoading, setIsLoading] = useState(false);
  const devices = cart.devices;
  const accessories = cart.accessories;
  const supportFee = line.charges.find((c) => c.type === 'Activation');
  const [deleteModal, setDeleteModal] = useState(false);
  const [showDeleteConfirmationModal, setShowDeleteConfirmationModal] = useState(false);

  useEffect(() => {
    const tempDevices: Device[] = [];

    devices.forEach((a) => {
      if (line.id === a.lineId) {
        tempDevices.push(a);
      }
    });

    const accessoryObj: {
      [key: string]: Accessory[];
    } = {};
    for (const item of accessories) {
      if (item.lineId === line.id) {
        if (accessoryObj[item.upc] === undefined) {
          accessoryObj[item.upc] = [item];
        } else {
          accessoryObj[item.upc].push(item);
        }
      }
    }

    const tempAccGroups: Accessory[][] = [];
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    for (const [_, item] of Object.entries(accessoryObj)) {
      tempAccGroups.push(item);
    }
    setAccessoriesState(tempAccGroups);
    setDevicesState(tempDevices);
  }, [devices, accessories]);

  const handleQuantityChange = async (operation: string, accessory: Accessory) => {
    setIsLoading(true);
    switch (operation) {
      case 'plus':
        try {
          await addAccessory(accessory.upc, accessory.lineId);
          setIsLoading(false);
        } catch (e) {
          setIsLoading(false);
          alert('failed to add the accessory');
          throw e;
        }
        break;
      case 'minus':
        try {
          await deleteAccessory(accessory.id, accessory.lineId);
          setIsLoading(false);
        } catch (e) {
          setIsLoading(false);
          alert('failed to delete the accessory');
          throw e;
        }
        break;
      default:
        break;
    }
  };

  const addAccessory = async (sku: string, lineId: string) => {
    const accessoryModel: IAccessoryRequest = {
      sku: sku,
      offerId: '',
      quantity: 1,
      pricingOption: 'FULL',
    };
    try {
      const cartId = storageUtil.getCartId();
      const { data } = await http.post<ICart>(
        `${getGatewayUrl()}${CART_URL}${cartId}${LINES_URL}${lineId}${ACCESSORIES_URL}`,
        accessoryModel,
      );
      setCartData(data);
    } catch (e) {
      console.error(e);
      throw e;
    }
  };

  const deleteAccessory = async (accessoryId: string, lineId: string) => {
    try {
      const cartId = storageUtil.getCartId();
      const { data } = await http.delete<ICart>(
        `${getGatewayUrl()}${CART_URL}${cartId}${LINES_URL}${lineId}${ACCESSORIES_URL}${accessoryId}`,
      );
      setCartData(data);
    } catch (e) {
      console.error(e);
      throw e;
    }
  };

  const deleteDevice = async (deviceId: string, lineId: string) => {
    try {
      const cartId = storageUtil.getCartId();
      const { data } = await http.delete<ICart>(
        `${getGatewayUrl()}${CART_URL}${cartId}${LINES_URL}${lineId}${DEVICES_URL}${deviceId}`,
      );
      setCartData(data);
    } catch (e) {
      console.error(e);
      throw e;
    }
  };

  const handleTrashClick = (event: React.SyntheticEvent) => {
    event.preventDefault();
    setDeleteModal(true);
  };

  const handleDeleteConfirmation = (deviceId: string, lineId: string) => {
    setDeleteModal(false);
    setShowDeleteConfirmationModal(true);
    deleteDevice(deviceId, lineId);
  };

  return (
    <>
      <div className={`${styles['line-blade-container']}`}>
        {isLoading && (
          <div className={`${styles['spinner']}`}>
            <div>
              <i className={`pi pi-spin ${styles['pi-spinner']}`}></i>
            </div>
          </div>
        )}
        <div data-testid='misdin' className={`${styles['misdin']}`}>
          {formatMobileNumber(line.phoneNumber)}
        </div>
        <div className={`${styles['blade-content']}`}>
          {devicesState?.map((device: Device, index: number) => (
            <div key={index} className={`p-grid ${styles['blade-device-content']}`}>
              <div className='p-col'>
                <div data-testid='upgrade' className={`${styles['header']}`}>
                  {UPGRADE}
                </div>
                <div
                  className={`${styles['blade-details']}`}
                >{`${device?.familyName} ${device?.options?.memory}${device?.options?.memoryUnit} ${device?.options?.color} - ${TMOBILE}`}</div>
                <div className={`${styles['blade-details']}`}>
                  {PRICE} <span>{formatCurrency(device?.listPrice)}</span>
                </div>
                {supportFee ? (
                  <div>
                    {SUPPORT_FEE} <span>{formatCurrency(supportFee.amount)}</span>
                  </div>
                ) : null}
              </div>
              <div className='p-col'>
                <div data-testid='finance' className={`${styles['header']}`}>
                  {FINANCE}
                </div>
                {device?.monthlyPrice ? (
                  <div>
                    <div className={`${styles['blade-details']}`}>{`${EIP} ${formatCurrency(
                      device?.monthlyPrice,
                    )}/${MONTH}`}</div>
                    <div className={`${styles['blade-details']}`}>{`${TERM} ${device?.contractTerm} ${MONTHS}`}</div>

                    {device?.downPaymentAmount !== null && (
                      <div className={`${styles['blade-details']}`}>
                        {`${DOWN_PAYMENT} ${formatCurrency(device?.downPaymentAmount)}`}
                      </div>
                    )}
                    {device?.financedAmount !== null && (
                      <div className={`${styles['blade-details']}`}>
                        {`${FINANCED_AMOUNT} ${formatCurrency(device?.financedAmount)}`}
                      </div>
                    )}
                  </div>
                ) : (
                  <div>{`${FULL_RETAIL_PRICE}`}</div>
                )}
              </div>
              <div className='p-col'>
                <div data-testid='details' className={`${styles['header']}`}>
                  {DETAILS}
                </div>
                {line.services && line.services.length > 0 ? (
                  <div>
                    {PROTECTION} <span>{`${formatCurrency(line.services[0].monthlyPrice)}/${MONTH}`}</span>
                    <div>
                      {NAME} <span>{`${line.services[0].name}`}</span>
                    </div>
                  </div>
                ) : (
                  <div>
                    {PROTECTION} <span>{NA}</span>
                    <div>
                      {NAME} <span>{PROTECTION_DECLINE}</span>
                    </div>
                  </div>
                )}
              </div>
              <div className='p-col'>
                <div data-testid='charges' className={`${styles['header']}`}>
                  {CHARGES}
                </div>
                {device?.monthlyPrice ? (
                  <div className={`${styles['blade-details']}`}>
                    <div>
                      {DUE_TODAY}{' '}
                      <span className={`${styles['finance-amount']}`}>{formatCurrency(device?.downPaymentAmount)}</span>
                    </div>
                    {`${DUE_MONTHLY}`}
                    <span className={`${styles['finance-amount']}`}>{formatCurrency(device?.monthlyPrice)}</span>
                  </div>
                ) : (
                  <div className={`${styles['blade-details']}`}>
                    {DUE_TODAY}{' '}
                    <span className={`${styles['finance-amount']}`}>{formatCurrency(device?.listPrice)}</span>
                  </div>
                )}
              </div>
              <div className={`p-col icons`}>
                <a data-testid='override' href=''>
                  <img src={overrideIcon} alt='override' />
                </a>
                <a data-testid='trash-device' href=''>
                  <img src={trashIcon} alt='trash' onClick={(e) => handleTrashClick(e)} />
                </a>
              </div>
              <ConfirmationModal
                modalText={DEVICE_PENDING_PROMPT + formatMobileNumber(line.phoneNumber) + DISCARD_WARNING_PROMPT}
                header={DELETE_CONFIRMATION_PROMPT}
                isVisible={deleteModal}
                handleContinueBtn={() => handleDeleteConfirmation(device.id, line.id)}
                handleCancelBtn={setDeleteModal}
              />
            </div>
          ))}
          {accessoriesState?.map((accessory: Accessory[], index: number) => (
            <div key={line.id + 'acc' + index} className={`${styles['blade-device-content']}`}>
              {devicesState.length > 0 ? <hr className='p-mb-3' /> : null}
              <div className='p-grid'>
                <div className='p-col'>
                  <div data-testid='accessory' className={`${styles['header']}`}>
                    {ACCESSORY}
                  </div>
                  <div
                    className={`${styles['blade-details']}`}
                  >{`${accessory[0].familyName} ${accessory[0].options?.memory}${accessory[0].options?.memoryUnit} ${accessory[0].options?.color}`}</div>
                  <div className={`${styles['blade-details']}`}>
                    {PRICE} <span>{formatCurrency(accessory[0].listPrice)}</span>
                  </div>
                </div>
                <div className='p-col'>
                  <div data-testid='finance' className={`${styles['header']}`}>
                    {FINANCE}
                  </div>
                  <div data-testid='financeNA' className={`${styles['blade-details']}`}>
                    {NA}
                  </div>
                </div>
                <div className='p-col'>
                  <div data-testid='details' className={`${styles['header']}`}>
                    {DETAILS}
                  </div>
                  <div data-testid='quantity' className={`${styles['blade-details']}`}>
                    {QUANTITY}
                  </div>
                  <div>
                    <div className='col-12'>
                      <button
                        data-testid={'btnMinus' + accessory[0].id}
                        className={`${styles['button-decrease']}`}
                        type='button'
                        disabled={accessory.length === 1}
                        onClick={() => handleQuantityChange('minus', accessory[0])}
                      >
                        <i className='pi pi-minus'></i>
                      </button>
                      <input
                        data-testid={'txtQty' + accessory[0].id}
                        className={`${styles['accessories-quantity-input']}`}
                        readOnly
                        type='number'
                        value={accessory.length}
                      />
                      <button
                        data-testid={'btnPlus' + accessory[0].id}
                        className={`${styles['button-increase']}`}
                        type='button'
                        disabled={accessory.length === 999}
                        onClick={() => handleQuantityChange('plus', accessory[0])}
                      >
                        <i className='pi pi-plus'></i>
                      </button>
                    </div>
                  </div>
                </div>
                <div className='p-col'>
                  <div data-testid='charges' className={`${styles['header']}`}>
                    {CHARGES}
                  </div>
                  <div className={`${styles['blade-details']}`}>
                    {DUE_TODAY}{' '}
                    <span className={`${styles['finance-amount']}`}>
                      {formatCurrency(accessory[0].listPrice * accessory.length)}
                    </span>
                  </div>
                </div>
                <div className={`p-col icons`}>
                  <a data-testid='override' href=''>
                    <img src={overrideIcon} alt='override' />
                  </a>
                  <a data-testid='trash' href=''>
                    <img src={trashIcon} alt='trash' />
                  </a>
                </div>
              </div>
            </div>
          ))}
          <Dialog
            showHeader={false}
            visible={showDeleteConfirmationModal}
            modal={false}
            style={{ width: '20vw' }}
            onHide={() => setShowDeleteConfirmationModal(false)}
            className={`pt-3 ${styles['delete-confirmation-modal']}`}
          >
            <i className={`pi ml-2 ${styles['pi-times']}`} onClick={() => setShowDeleteConfirmationModal(false)}></i>
            <i className={`pi pi-check`}>
              <span className={`ml-2 ${styles['confirmation-text']}`}>{DEVICE_DELETED_PROMPT}</span>
            </i>
          </Dialog>
        </div>
      </div>
    </>
  );
};

export default LineBlade;
