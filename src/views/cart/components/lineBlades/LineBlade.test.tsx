import { render, screen } from '@testing-library/react';
import React from 'react';
import mockCartData from '../../../../../mocks/responses/tms.cart.response.json';
import { ICart } from '../../interfaces/ICart';
import LineBlade from './LineBlade';

describe('Line Blades component', () => {
  const mockProps: ICart = mockCartData as unknown as ICart;
  it('renders without crashing', () => {
    // eslint-disable-next-line @typescript-eslint/no-empty-function
    render(<LineBlade line={mockProps.lines[0]} cart={mockProps} setCartData={() => {}} />);
    expect(screen.getByTestId('misdin')).toHaveTextContent('(770) 788-8930');
    expect(screen.getByTestId('txtQtyge4tinjxmq2wkljvmm3dsljuge4tcllbme3dsllggyyteyjvgq3gcytghe=')).toHaveValue(3);
  });
});
