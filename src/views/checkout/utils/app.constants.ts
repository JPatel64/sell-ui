export const API_GATEWAY_LOCAL = 'http://localhost:8110';
export const CARE = 'care';
export const CVF_TOKEN = 'cvfToken';
export const PROFILE_BASE_URL = '/profile';
export const SHIPPING_BASE_URL = '/shipping';
export const CART_BASE_URL = '/carts';
export const NOTIFICATIONS_URL = '/notifications';
export const SHIPPING_OPTIONS_URL = '/shipping-options?cartId=';
export const SHOP_NOW = 'shopNow';
export const VALIDATE_ZIPCODE_URL = '/getCityState?zipCode=';
export const VERIFY_ADDRESS_URL = '/address/verify';
export const QUOTES_URL = '/quotes';
export const GET_MSISDNS = '/msisdns?ban=';
export const PROFILE_ADDRESS_URL = '/address?profileId=';
export const MEG_GATEWAY_URL_EXTN = '/v1/checkout-service';
export const SUBMIT_ORDER_URL = '/orders/submit';
export const CONSENTS_URL = '/consents';
export const PAYMENTS_URL = '/payments';
export const BILLING_TYPE_POSTPAID = 'POSTPAID';
export const PAYMENT_METHOD_CREDIT = 'CREDIT';
export const CONSENT_TYPE_PAYMENT = 'PAYMENT';
export const ADDRESS_TYPE_PAYMENT = 'PAYMENT';
export const COUNTRY_DEFAULT = 'US';

export const CARD_TYPE_MAPPING: Record<string, string> = {
  VISA: 'card-visa',
  MASTERCARD: 'card-master',
  AMERICANEXPRESS: 'card-express',
  DISCOVER: 'card-discover',
};
