import { formatCurrency, parseName, splitExpirationDate } from './format.util';

describe('formatCurrency', () => {
  it('should format given number into formatted currency string', () => {
    expect(formatCurrency(10)).toBe('$10.00');
    expect(formatCurrency(1000)).toBe('$1,000.00');
  });
});

describe('parseName', () => {
  it('should return firstName and lastName given a fullName separated by spaces', () => {
    const { firstName, lastName } = parseName('peter parker');
    expect(firstName).toBe('peter');
    expect(lastName).toBe('parker');
  });
});

describe('splitExpirationDate', () => {
  it('should return month and year from given string of format MMYY', () => {
    const { month, year } = splitExpirationDate('0223');
    expect(month).toBe('02');
    expect(year).toBe('2023');
  });

  it('should throw error if input length is not 4 characters', () => {
    expect(() => splitExpirationDate('123123123')).toThrow('Invalid card expiration date provided: 123123123');
  });
});
