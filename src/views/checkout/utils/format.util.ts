export const formatCurrency = (amount = 0) => {
  const formatter = Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD',
  });
  return formatter.format(amount);
};

export const formatMobileNumber = (rawNum: string): string | null => {
  const cleaned = ('' + rawNum).replace(/\D/g, '');
  const match = cleaned.match(/^(\d{3})(\d{3})(\d{4})$/);
  if (match) {
    return '(' + match[1] + ') ' + match[2] + '-' + match[3];
  }

  return null;
};

export const splitExpirationDate = (cardExpiryMonthYear: string) => {
  if (cardExpiryMonthYear.length !== 4) {
    throw new Error(`Invalid card expiration date provided: ${cardExpiryMonthYear}`);
  } else {
    const month = cardExpiryMonthYear.slice(0, 2);
    const year = `20${cardExpiryMonthYear.slice(-2)}`;
    return { month, year };
  }
};

export const parseName = (fullName: string) => {
  const splitName = fullName.split(' ');
  return { firstName: splitName[0], lastName: splitName[splitName.length - 1] };
};
