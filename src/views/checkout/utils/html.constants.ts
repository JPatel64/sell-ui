const HTMLConstants: Record<string, string> = {
  SHIPPING_ADDRESS: 'Shipping address',
  EDIT: 'Edit',

  //shipping method
  SHIPPING_METHOD: 'Shipping method',
  WAIVE_SHIPPING_FEE: 'Waive shipping fee',

  //Order Summary
  ORDER_SUMMARY: 'Order summary',
  SUBTOTAL_PRETAX: 'Subtotal (pre-tax)',
  DISCOUNTS_CREDITS: 'Discounts and credits',
  DEPOSITS_CHARGES: 'Deposits and charges',
  TAX: 'Tax',
  SHIPPING: 'Shipping',
  TOTAL_DUE_TODAY: 'Total due today (estimated)',

  //Order Notification Preferences
  ORDER_NOTIFICATION_PREFERENCES: 'Order notification preferences',
  ENGLISH: 'English',
  SPANISH: 'Spanish',
  DELIVERY_OPTIONS: 'Delivery options',
  EMAIL_ADDRESS: 'Email address',
  INVALID_ADDRESS: 'Address is not valid',
  NOTIFICATIONS: 'Notifications',
  SMS_MESSAGE: 'SMS message',
  RECEIPTS: 'Receipts',
  BILLING_ADDRESS: 'Billing address',
  DOCUMENTS: 'Documents',
  DOCUMENTS_TOOLTIP_TEXT: 'Legal DocuSign documents for EIP, JOD, and Jump!',
  LANGUAGE_UPDATE: 'Language update',
  MODIFY_LANGUAGE_TEXT:
    'You have modified the language for financial disclosures and communications; do you wish to continue?',
  NOTIFICATION_MODE_HARDCOPY: 'HARDCOPY',
  NOTIFICATION_MODE_EMAIL: 'EMAIL',
  NOTIFICATION_MODE_SMS: 'SMS',
  NOTIFICATION_TYPE_RECEIPT: 'RECEIPT',
  NOTIFICATION_TYPE_NOTIFICATION: 'NOTIFICATION',
  EN: 'EN',
  ES: 'ES',

  //Payment T&C's
  PAYMENT: 'Payment',
  CUSTOMER_ADVISEMENT: 'Customer advisement',
  CUSTOMER_ACCEPTS_TC: 'The customer accepts these Terms & Conditions',
  //Order Confirmation
  ORDER_CONFIRMATION: 'Order confirmation',
  TRANSACTION_DETAILS: 'Transaction details',
  CONFIRMATION_NOTICE: 'Your order has been placed.',
  AMOUNT_CHARGE: 'Amount charge today',
  ORDER_NUMBER: 'Order number',

  //Edit Shipping
  FIRST_NAME: 'First name',
  MIDDLE_INITIAL: 'MI',
  LAST_NAME: 'Last name',
  ADDRESS_1: 'Address 1',
  ADDRESS_2: 'Address 2 (Optional)',
  ZIP_CODE: 'ZIP code',
  CITY: 'City',
  STATE: 'State',
  ADDRESS_TO_USE_QUESTION: 'Which address do you want to use?',
  ENTERED_ADDRESS: 'Address entered',
  VALIDATED_ADDRESS: 'Validated (Recommended)',
  URBANIZATION_CODE: 'Urbanization code',
};

export default HTMLConstants;
