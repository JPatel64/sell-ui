export interface IPaymentDetails {
  fullName: string;
  cardType: string;
  paymentMethodAlias: string;
  address1: string;
  address2: string;
  city: string;
  state: string;
  zipCode: string;
  paymentId: string;
  cardExpiryMonthYear: string;
}
