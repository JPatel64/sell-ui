export interface Cart {
  id: string;
  state: CartState;
  createdDate: string;
  accessories: Accessory[];
  devices: Device[];
  lines: Line[];
  total: number;
  subTotal: number;
  tax: Tax;
  monthlyPayment: number;
  additionalDownPayment: number;
  financedAmount: number;
  promotions: Promotion[];
  deposit: Deposit;
  credit: Credit;
  shipping: number;
}

enum CartState {
  VALID = 'Valid',
  INVALID = 'Invalid',
}
export interface Accessory {
  id: string;
  lineId: string;
  familyName: string;
  manufacturer: string;
  upc: string;
  description: string;
  quantity: number;
  options: Options;
  listPrice: number;
  monthlyPrice: number;
  contractTerm: number;
  contractFrequency: ContractFrequency;
  deposit: Deposit;
  credit: Credit;
  financedAmount: number;
  promotions: Promotion[];
  downPaymentAmount: number;
}

enum ContractFrequency {
  MONTHLY = 'MONTHLY',
}

export interface Options {
  color: string;
  memory: string;
  memoryUnit: string;
}

export interface Deposit {
  isRefundable: boolean;
  amount: number;
}

export interface Credit {
  recurringCredit: RecurringCredit;
  oneTimeCredit: number;
}

export interface RecurringCredit {
  term: number;
  frequency: RecurringCreditFrequency;
  amount: number;
}

enum RecurringCreditFrequency {
  DAILY = 'Daily',
  WEEKLY = 'Weekly',
  MONTHLY = 'Monthly',
  QUARTERLY = 'Quarterly',
  ANNUALLY = 'Annually',
}

export interface Promotion {
  id: string;
  groupId: string;
  name: string;
  description: string;
  code: string;
  amount: number;
}

export interface Device {
  id: string;
  lineId: string;
  familyName: string;
  manufacturer: string;
  upc: string;
  description: string;
  quantity: number;
  options: Options;
  listPrice: number;
  monthlyPrice: number;
  contractTerm: number;
  contractFrequency: string;
  deposit: Deposit;
  credit: Credit;
  promotions: Promotion[];
  financedAmount: number;
  downPaymentAmount: number;
}

export interface Financing {
  equipmentCredit: EquipmentCredit;
}

export interface EquipmentCredit {
  limit: number;
  balance: number;
  available: number;
  lineContribution: number; // revisit
  maximumCredit: number;
}

export interface Line {
  id: string;
  number: string;
  type: string;
  name: string;
  phoneNumber: string;
  listPrice: number;
  monthlyPrice: number;
  contractTerm: number;
  contractFrequency: string;
  downPaymentAmount: number;
  financedAmount: number;
  charges: Charge[];
  promotions: Promotion[];
  services: Service[];
  deposit: Deposit;
  credit: Credit;
}

export interface Charge {
  name: string;
  description: string;
  type: string;
  amount: number;
}

export interface Service {
  name: string;
  monthlyPrice: number;
  listPrice: number;
}

export interface Tax {
  total: number;
  county: number;
  federal: number;
  state: number;
  monthlyLease: number;
}
