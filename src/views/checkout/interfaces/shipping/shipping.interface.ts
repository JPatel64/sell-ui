import { Address } from '../index';

export interface IShippingOptionsResponse {
  shippingOptions: ShippingOption[];
}

export interface ShippingOption {
  selected: boolean;
  carrier: string;
  displayName: string;
  price: Price;
  cost: Cost;
  shipmentDetailsId: string;
  optionsId: string;
  tax: Cost;
}

export interface Cost {
  amount: number;
  display: string;
}

export interface Price {
  listPrice: Cost;
  salePrice: Cost;
}

export interface IPostShippingOptionsRequest {
  overrides?: Override[];
  shipmentDetailsId: string;
  optionsId: string;
  extCartId?: string;
  storeId: string;
  notes: string;
}

export interface Override {
  requestorId: string;
  approverId: string;
  type: string;
  overrideReasonCode: string;
  amount: number;
  approverToken: string;
}

export interface IPostShippingOptionsResponse {
  notification: Notification[];
  shippingOptions: ShippingOption[];
}

export interface Notification {
  code: string;
  userMessage: string;
  systemMessage: string;
  additionalProperties: AdditionalProperty[];
}

export interface AdditionalProperty {
  key: string;
  value: string;
}

export interface IProfileAddressResponse {
  addresses: Address[];
}
