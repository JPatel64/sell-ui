import { IVerifiedAddress } from './IVerifiedAddress';

export interface IVerifyAddressRequest {
  addressToVerify: IVerifiedAddress;
}
