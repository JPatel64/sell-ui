export interface IVerifiedAddress {
  addressFormatType?: string;
  addressClassification?: string;
  address1: string;
  address2?: string;
  address3?: string;
  city: string;
  state: string;
  zipCode: string;
  zipCodeExtension?: string;
  countryCode?: string;
  postalCode?: string;
  addressType?: string;
  latLong?: string;
  international?: boolean;
  geoCode?: string;
  uncertaintyInd?: number;
  urbanization?: string;
  attention?: string;
}
