import { IVerifiedAddress } from './IVerifiedAddress';
export interface IVerifyAddressResponse {
  verifiedAddress: IVerifiedAddress;
  isChangeInAddress: boolean;
}
