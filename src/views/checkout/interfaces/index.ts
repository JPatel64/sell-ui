export interface Address {
  firstName: string;
  middleName?: string;
  lastName: string;
  address1?: string;
  address2?: string;
  address3?: string;
  country?: string;
  state?: string;
  city?: string;
  zipCode?: string;
  addressTypes?: AddressType[];
  addressId?: string;
}

export interface AddressType {
  associationId: string;
  relationResourceId: string;
  type: string;
}

export interface INotificationDetailRequest {
  languagePreference: string;
  notificationDetails: INotificationDetails[];
}
export interface INotificationDetails {
  notificationMode: string;
  value?: string;
  notificationType: string;
}

export interface ISubmitOrderResponse {
  externalOrderNumber: string;
  orderNumber: string;
  orderId: string;
}
