import React from 'react';
import { Outlet } from 'react-router-dom';
import Stepper from './components/stepper/Stepper';

function CheckoutView() {
  return (
    <div className='checkout-container'>
      <h2 className='ml-3'>Checkout</h2>
      <Stepper />

      <Outlet />
    </div>
  );
}

export default CheckoutView;
