import { fireEvent, render, screen } from '@testing-library/react';
import React from 'react';
import Tooltip from './Tooltip';

describe('OrderNotificationPreferences component', () => {
  it('should render without crashing', () => {
    const displayText = 'Tooltip content';
    render(
      <Tooltip enable displayText={displayText}>
        <div>test div</div>
      </Tooltip>,
    );
    expect(screen.getByText(displayText)).toBeInTheDocument();
    expect(screen.getByText('test div')).toBeInTheDocument();
    const span = screen.getByTestId('tooltip-span');
    fireEvent.mouseEnter(span);
    fireEvent.mouseLeave(span);
    // expect(screen.getByTestId('tooltip-span')).toBeInTheDocument();
  });
});
