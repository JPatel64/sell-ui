import React, { FunctionComponent, useState } from 'react';
import './Tooltip.scss';

interface ITooltipProps {
  children?: React.ReactNode;
  displayText?: string;
  enable: boolean;
}

const Tooltip: FunctionComponent<ITooltipProps> = ({ children, displayText = '', enable = false }) => {
  const [show, setShow] = useState(false);

  return (
    <>
      {enable ? (
        <span data-testid='tooltip' className='tooltip-container'>
          <div className={`arrow-box ${show && 'visible'}`}>{displayText}</div>
          <span data-testid='tooltip-span' onMouseEnter={() => setShow(true)} onMouseLeave={() => setShow(false)}>
            {children}
          </span>
        </span>
      ) : (
        children
      )}
    </>
  );
};

export default Tooltip;
