import { render, screen } from '@testing-library/react';
import React from 'react';
import Stepper from './Stepper';

jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
  useLocation: () => ({
    pathname: 'shipping',
  }),
}));

test('renders without crashing in stepper', async () => {
  render(<Stepper />);
  await screen.findByText('Shipping');
});
