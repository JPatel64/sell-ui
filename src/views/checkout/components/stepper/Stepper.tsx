import { Steps } from 'primereact/steps';
import React, { useEffect, useState } from 'react';
import { useLocation } from 'react-router-dom';
import './Stepper.scss';
const STEPS = [
  {
    label: 'Shipping',
    url: 'shipping',
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    template: (item: any, options: any) => (
      <>
        <span
          className={`p-steps-number ${options.props.activeIndex > 0 ? 'p-step-completed' : ''} ${
            options.active ? 'p-step-active' : ''
          }`}
        >
          {options.props.activeIndex > 0 ? <i className='pi pi-check' /> : null}
        </span>
        <span className='p-steps-title'>{item.label}</span>
      </>
    ),
  },
  {
    label: 'Payment',
    url: 'payment',
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    template: (item: any, options: any) => (
      <>
        <span
          className={`p-steps-number  ${options.props.activeIndex > 1 ? 'p-step-completed' : ''} ${
            options.active ? 'p-step-active' : ''
          }`}
        >
          {options.props.activeIndex > 1 ? <i className='pi pi-check' /> : null}
        </span>
        <span className='p-steps-title'>{item.label}</span>
      </>
    ),
  },
];

function getIndexByRouteName(name: string) {
  let index = 0;
  STEPS.filter((item, i) => {
    if (name.includes(item.url)) {
      index = i;
      return;
    }
  });
  return index;
}
const Stepper = () => {
  const loc = useLocation();
  const [path, setPath] = useState(loc.pathname);
  const [activeIndex, setActiveIndex] = useState(getIndexByRouteName(loc.pathname));

  useEffect(() => {
    setActiveIndex(getIndexByRouteName(loc.pathname));
    setPath(loc.pathname);
  }, [loc.pathname]);

  return path != '/order-confirmation' ? (
    <div className='checkout-stepper'>
      <Steps className='stepper' model={STEPS} activeIndex={activeIndex} onSelect={(e) => setActiveIndex(e.index)} />
    </div>
  ) : (
    <></>
  );
};
export default Stepper;
