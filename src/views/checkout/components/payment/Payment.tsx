import { GlobalNavUtil, http, loadExternalApplicationByUrl, storageUtil } from '@tmobile/atlas-util';
import { Button } from 'primereact/button';
import React, { useEffect, useState } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import tc from '../../../../../mocks/responses/uiConfigurationData.json';
import { ISubmitOrderResponse } from '../../interfaces';
import { IPaymentDetails } from '../../interfaces/payment';
import {
  ADDRESS_TYPE_PAYMENT,
  BILLING_TYPE_POSTPAID,
  CART_BASE_URL,
  CONSENTS_URL,
  CONSENT_TYPE_PAYMENT,
  COUNTRY_DEFAULT,
  PAYMENTS_URL,
  PAYMENT_METHOD_CREDIT,
  SUBMIT_ORDER_URL,
} from '../../utils/app.constants';
import { getGatewayUrl } from '../../utils/auth.util';
import { parseName, splitExpirationDate } from '../../utils/format.util';
import PaymentDetails from '../PaymentDetails/PaymentDetails';
import PaymentTermsAndConditions from '../PaymentTermsAndConditions/PaymentTermsAndConditions';
import './Payment.scss';

const Payment = () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const location: any = useLocation();
  const [isAcceptedTC, setIsAcceptedTC] = useState(false);
  const totalCharges = location.state?.price;
  const navigate = useNavigate();
  const CART_ID = storageUtil.getCartId();
  const [paymentDetails, setPaymentDetails] = useState<IPaymentDetails>();

  useEffect(() => {
    const pamElement = document.querySelector('payments-access-management');
    const clientConfig = {
      auth: {
        repToken: GlobalNavUtil.getCustomerInFocusToken(),
        elementUrl: `${PAYMENT_URL}`,
        isNonTmoBan: false,
      },
      flow: 'collectPayment',
      general: {
        sourceApp: 'ATLAS_REGULAR_UPGRADE',
        applicationNameToDisplay: 'Upgrade',
        outputPaymentMethod: false,
      },
      methodValidation: {
        accountMigration: false,
        orderType: 'UPGRADE',
        productGroup: 'HARDGOODS',
      },
      wallet: {
        allowWallet: true,
      },
      tabs: {
        METHODS_ON_FILE: {
          access: {
            display: 'hide',
          },
        },
        NEW_BANK: {
          access: {
            display: 'hide',
          },
        },
        NEW_CARD: {
          access: {
            display: 'full',
          },
        },
      },
    };
    if (pamElement) {
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      (pamElement as any).clientConfig = clientConfig;
      pamElement.addEventListener('paymentMethodSelectAction', (event: Event) => paymentMethodSelectAction(event));
    }

    return () => {
      // unsubscribe event
      if (pamElement) {
        pamElement.removeEventListener('paymentMethodSelectAction', (event: Event) => paymentMethodSelectAction(event));
      }
    };
  });

  useEffect(() => {
    loadExternalApplicationByUrl('paymentarrangements', `${PAYMENT_URL}`);
  }, []);

  const getConsentsRequest = () => {
    return {
      consents: [
        {
          consentType: CONSENT_TYPE_PAYMENT,
          consentIndicator: true,
          consentTime: new Date().toJSON(),
        },
      ],
    };
  };

  const getPaymentsRequest = () => {
    if (paymentDetails) {
      const { firstName, lastName } = parseName(paymentDetails.fullName);
      const { month, year } = splitExpirationDate(paymentDetails.cardExpiryMonthYear);
      return {
        paymentMethod: PAYMENT_METHOD_CREDIT,
        billingType: BILLING_TYPE_POSTPAID,
        customerPresentIndicator: true,
        fraudCheckIndicator: true,
        isAuthRequired: true,
        isFullAuth: true,
        address: {
          firstName: firstName,
          lastName: lastName,
          address1: paymentDetails.address1,
          address2: paymentDetails.address2,
          city: paymentDetails.city,
          state: paymentDetails.state,
          zipCode: paymentDetails.zipCode,
          country: COUNTRY_DEFAULT,
          addressType: ADDRESS_TYPE_PAYMENT,
        },
        cardHolderFirstName: firstName,
        cardHolderLastName: lastName,
        cardNumberAlias: paymentDetails.paymentMethodAlias,
        typeCode: paymentDetails.cardType,
        paymentId: paymentDetails.paymentId,
        extCartId: storageUtil.getExtCartId(),
        expirationMonth: month,
        expirationYear: year,
        termsAgreementIndicator: true,
        termsAgreementTime: new Date().toJSON(),
      };
    }
  };

  const placeOrder = async () => {
    try {
      await http.post(`${getGatewayUrl()}${CART_BASE_URL}/${CART_ID}${CONSENTS_URL}`, getConsentsRequest());

      await http.post(`${getGatewayUrl()}${CART_BASE_URL}/${CART_ID}${PAYMENTS_URL}`, getPaymentsRequest());

      const { data } = await http.post<ISubmitOrderResponse>(`${getGatewayUrl()}` + SUBMIT_ORDER_URL, {
        cartId: CART_ID,
      });

      navigate('/order-confirmation', { state: { orderNumber: data.externalOrderNumber, totalCharges } });
    } catch (e) {
      console.error('Could not submit the order', e);
      throw e;
    }
    //Revisit, using internal router?
  };

  const paymentMethodSelectAction = (event: Event) => {
    if (event instanceof CustomEvent && event.detail) {
      setPaymentDetails(event.detail);
    }
  };

  return (
    <div className='payment-page p-3'>
      <div style={{ display: `${!paymentDetails ? 'contents' : 'none'}` }}>
        {/* eslint-disable-next-line @typescript-eslint/ban-ts-comment */}
        {/* @ts-ignore */}
        <payments-access-management data-testid='payment-element'></payments-access-management>
      </div>
      {paymentDetails && <PaymentDetails paymentDetails={paymentDetails} />}
      {tc && (
        <PaymentTermsAndConditions
          terms={{
            english: tc.uiConfigurationData['terms-and-conditions.payment.english'],
            spanish: tc.uiConfigurationData['terms-and-conditions.payment.spanish'],
          }}
          totalCharges={totalCharges}
          setIsAcceptedTC={setIsAcceptedTC}
        />
      )}

      <div className='btn-group p-grid'>
        <Button label='Cancel' className='p-button-rounded cancel-btn p-col mx-4' data-testid='cancel-button' />
        <Button
          data-testid='place-order'
          disabled={!paymentDetails || !isAcceptedTC}
          label='Place order'
          className={`p-button-rounded continue-btn p-col mx-4`}
          onClick={() => placeOrder()}
        />
      </div>
    </div>
  );
};
export default Payment;
