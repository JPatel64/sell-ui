import '@testing-library/jest-dom';
import { render, screen } from '@testing-library/react';
import React from 'react';
import Payment from './Payment';
const mockedNavigate = jest.fn();

jest.mock('@tmobile/atlas-util');

jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
  useLocation: () => ({
    pathname: 'payment',
  }),
  useNavigate: () => mockedNavigate,
}));

describe('Payment component', () => {
  it('should render without crashing', async () => {
    render(<Payment />);
    await screen.findByTestId('payment-element');
    await screen.findByTestId('terms-conditions');
    await screen.findByTestId('cancel-button');
    await screen.findByTestId('place-order');
  });
});
