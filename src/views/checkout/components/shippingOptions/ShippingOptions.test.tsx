import { fireEvent, render, screen } from '@testing-library/react';
import React from 'react';
import mockResponse from '../../../../../mocks/responses/shippingOptionsResponse.json';
import ShippingOptions from './ShippingOptions';

describe('Shipping method component', () => {
  it('should render without crashing', async () => {
    const mockShippingOptions = mockResponse.shippingOptions;
    render(
      <ShippingOptions
        setShippingAdditionalInfo={() => {
          return '';
        }}
        shippingAdditionalInfo={''}
        shippingOptions={mockShippingOptions}
        selectedOption={undefined}
        // eslint-disable-next-line @typescript-eslint/no-empty-function
        setSelectedOption={() => {
          return undefined;
        }}
        isShippingWaived={false}
        isShippingAdditionalInfo={false}
        // eslint-disable-next-line @typescript-eslint/no-empty-function
        setIsShippingWaived={() => {
          return undefined;
        }}
        setIsShippingMethodSelected={() => {
          return undefined;
        }}
      />,
    );

    expect(screen.getByTestId('shipping-method')).toHaveTextContent('Two Day - 2 to 3 business days');
    expect(screen.getByTestId('shipping-method')).toHaveTextContent('$14.99');
    expect(screen.getByTestId('shipping-method')).toHaveTextContent('Next Day - Up to 2 business days');
    expect(screen.getByTestId('shipping-method')).toHaveTextContent('$24.99');
    expect(screen.getByTestId('shipping-method')).toHaveTextContent('Ground - Up to 7 business days');
    expect(screen.getByTestId('shipping-method')).toHaveTextContent('$6.99');
    expect(screen.getByTestId('waive-shipping')).toHaveTextContent('Waive shipping fee');

    const checkbox = screen.getByTestId('waive-shipping').firstChild;
    expect(checkbox).not.toBeChecked();
    if (checkbox) fireEvent.click(checkbox, { target: { checked: true } });

    const radioButtons = screen.findAllByRole('radio');
    (await radioButtons).forEach((button) => {
      fireEvent.click(button, { target: { checked: true } });
    });
  });

  it('should render without crashing for PR', async () => {
    const mockShippingOptions = mockResponse.shippingOptions;
    render(
      <ShippingOptions
        setShippingAdditionalInfo={() => {
          return '';
        }}
        shippingAdditionalInfo={''}
        shippingOptions={mockShippingOptions}
        selectedOption={undefined}
        // eslint-disable-next-line @typescript-eslint/no-empty-function
        setSelectedOption={() => {
          return undefined;
        }}
        isShippingWaived={false}
        isShippingAdditionalInfo={true}
        // eslint-disable-next-line @typescript-eslint/no-empty-function
        setIsShippingWaived={() => {
          return undefined;
        }}
        setIsShippingMethodSelected={() => {
          return undefined;
        }}
      />,
    );
  });
});
