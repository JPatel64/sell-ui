import { Checkbox } from 'primereact/checkbox';
import { InputTextarea } from 'primereact/inputtextarea';
import { RadioButton } from 'primereact/radiobutton';
import React, { FunctionComponent, useEffect } from 'react';
import { ShippingOption } from '../../interfaces/shipping/shipping.interface';
import HTMLConstants from '../../utils/html.constants';
import './ShippingOptions.scss';

interface IShippingOptionsProps {
  setShippingAdditionalInfo: (e: string) => void;
  shippingAdditionalInfo: string;
  shippingOptions: ShippingOption[] | undefined;
  selectedOption: ShippingOption | undefined;
  setSelectedOption: (e: ShippingOption) => void;
  isShippingWaived: boolean;
  isShippingAdditionalInfo: boolean;
  setIsShippingWaived: (e: boolean) => void;
  setIsShippingMethodSelected: (e: boolean) => void;
}

const ShippingOptions: FunctionComponent<IShippingOptionsProps> = ({
  setShippingAdditionalInfo,
  shippingAdditionalInfo,
  shippingOptions,
  selectedOption,
  setSelectedOption,
  isShippingWaived,
  isShippingAdditionalInfo,
  setIsShippingWaived,
  setIsShippingMethodSelected,
}) => {
  const maxCount = 300;

  const handleInfoChange = (event: React.ChangeEvent) => {
    if ((event.target as HTMLInputElement).value.length > maxCount) {
      return;
    }
    setShippingAdditionalInfo((event.target as HTMLInputElement).value);
  };
  useEffect(() => {
    setIsShippingMethodSelected(selectedOption !== undefined);
  }, [selectedOption, setIsShippingMethodSelected]);

  useEffect(() => {
    if (isShippingAdditionalInfo) {
      shippingOptions &&
        shippingOptions
          .sort((option1, option2) => option1.cost.amount - option2.cost.amount)
          .map((shippingOption, index) => {
            if (index === 0) {
              setSelectedOption(shippingOption);
            }
          });
    }
  }, [isShippingAdditionalInfo]);
  return (
    <div className='shipping-method-container my-3' data-testid='shipping-method'>
      <div className='shipping-method-header my-3'>{HTMLConstants.SHIPPING_METHOD}</div>
      {shippingOptions &&
        !isShippingAdditionalInfo &&
        shippingOptions
          .sort((option1, option2) => option1.cost.amount - option2.cost.amount)
          .map((shippingOption, index) => {
            return (
              <div key={'option' + index} className='shipping-method-line my-3 grid' data-testid='shipping-options'>
                <RadioButton
                  inputId={shippingOption.displayName}
                  name='shippingOption'
                  value={shippingOption}
                  onChange={(event) => setSelectedOption(event.value)}
                  checked={selectedOption?.optionsId === shippingOption.optionsId}
                />
                <label className='ml-3 p-0 col' htmlFor={shippingOption.displayName}>
                  {shippingOption.displayName}
                  <span className='shipping-method-cost ml-3'>{shippingOption.cost?.display}</span>
                </label>
              </div>
            );
          })}

      <div className='waive-shipping-container' data-testid='additionalInfo-shipping'>
        {isShippingAdditionalInfo ? (
          <div className='d-flex addInfo'>
            {selectedOption ? (
              <div className='d-flex header'>
                <span className='title'>{selectedOption.displayName}</span>
                <span className='price'>{selectedOption.cost?.display}</span>
              </div>
            ) : (
              <></>
            )}
            <InputTextarea
              placeholder={'Additional Shipping instructions (Optional)'}
              cols={5}
              rows={5}
              value={shippingAdditionalInfo}
              onChange={handleInfoChange}
              autoResize
            />
            <strong>
              {shippingAdditionalInfo.length}/{maxCount}
            </strong>
          </div>
        ) : (
          <></>
        )}
      </div>

      <div className='waive-shipping-container' data-testid='waive-shipping'>
        <Checkbox
          inputId='waiveShipping'
          onChange={(event) => {
            setIsShippingWaived(event.checked);
          }}
          checked={isShippingWaived}
        ></Checkbox>
        <label htmlFor='waiveShipping' className='ml-3'>
          {HTMLConstants.WAIVE_SHIPPING_FEE}
        </label>
      </div>
    </div>
  );
};

export default ShippingOptions;
