import { fireEvent, render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import React from 'react';
import HTMLConstants from '../../utils/html.constants';
import OrderNotificationPreferences from './OrderNotificationPreferences';

describe('OrderNotificationPreferences component', () => {
  it('should render without crashing', () => {
    render(
      <OrderNotificationPreferences
        setIsEmailEntered={() => {
          return undefined;
        }}
        setIsNotificationsChecked={() => {
          return undefined;
        }}
        setIsReceiptsChecked={() => {
          return undefined;
        }}
        setNotificationPreferences={() => {
          return undefined;
        }}
        msisdns={[]}
      />,
    );

    expect(screen.getByTestId('order-notification')).toHaveTextContent('Order notification preferences');

    expect(screen.getByTestId('English')).toHaveTextContent('English');
    expect(screen.getByTestId('Spanish')).toHaveTextContent('Spanish');
    fireEvent.click(screen.getByTestId('English'), { target: { checked: true } });
    fireEvent.click(screen.getByTestId('Spanish'), { target: { checked: true } });
    const imgListItems = screen.getAllByRole('presentation');

    imgListItems.forEach((item) => {
      // You can import wihthin from @testing-library/react
      fireEvent.click(item);
    });

    expect(screen.getByTestId('email-box')).toHaveTextContent('Email address');

    expect(screen.getByTestId('notifications')).toHaveTextContent('Email address');
    expect(screen.getByTestId('notifications')).toHaveTextContent('SMS message');
    expect(screen.getByTestId('receipts')).toHaveTextContent('Email address');
    expect(screen.getByTestId('receipts')).toHaveTextContent('Billing address');
    expect(screen.getByTestId('documents')).toHaveTextContent('Email address');
  });

  it('should test valid email input', async () => {
    render(
      <OrderNotificationPreferences
        setIsEmailEntered={() => {
          return undefined;
        }}
        setIsNotificationsChecked={() => {
          return undefined;
        }}
        setIsReceiptsChecked={() => {
          return undefined;
        }}
        setNotificationPreferences={() => {
          return undefined;
        }}
        msisdns={[]}
      />,
    );
    await userEvent.dblClick(screen.getByTestId('email-input'));
    await userEvent.keyboard('test@gmail.com');
    expect(screen.getByTestId('email-input')).toHaveValue('test@gmail.com');
    expect(screen.getByTestId('email-notification')).toBeEnabled();

    const emailCheckbox = screen.getByTestId('email-notification').firstChild;
    expect(emailCheckbox).not.toBeChecked();
    if (emailCheckbox) fireEvent.click(emailCheckbox, { target: { checked: true } });

    expect(screen.getByTestId('sms-notification')).toBeEnabled();

    const checkbox = screen.getByTestId('sms-notification').firstChild;
    expect(checkbox).not.toBeChecked();
    if (checkbox) fireEvent.click(checkbox, { target: { checked: true } });

    // const radioButtons = screen.findAllByRole('radio');
    // (await radioButtons).forEach((button) => {
    //   fireEvent.click(button, { target: { checked: true } });
    // });
  });

  it('should test invalid email input', async () => {
    render(
      <OrderNotificationPreferences
        setIsEmailEntered={() => {
          return undefined;
        }}
        setIsNotificationsChecked={() => {
          return undefined;
        }}
        setIsReceiptsChecked={() => {
          return undefined;
        }}
        setNotificationPreferences={() => {
          return undefined;
        }}
        msisdns={[]}
      />,
    );
    await userEvent.dblClick(screen.getByTestId('email-input'));
    await userEvent.keyboard('invalid email address');
    expect(screen.getByText(HTMLConstants.INVALID_ADDRESS)).toBeInTheDocument();
  });
});
