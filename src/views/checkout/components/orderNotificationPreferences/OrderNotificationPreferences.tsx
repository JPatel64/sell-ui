import { Checkbox } from 'primereact/checkbox';
import { Dialog } from 'primereact/dialog';
import { InputText } from 'primereact/inputtext';
import { RadioButton } from 'primereact/radiobutton';
import React, { FunctionComponent, useEffect, useState } from 'react';
import Select from 'react-select';
import ClearIcon from '../../../../assets/clearIcon.svg';
import ErrorIcon from '../../../../assets/errorIcon.svg';
import TmoIcon from '../../../../assets/tmoIcon.png';
import TooltipIcon from '../../../../assets/tooltip.svg';
import Tooltip from '../../common/tooltip/Tooltip';
import { INotificationDetailRequest } from '../../interfaces';
import { hasValidEmail } from '../../utils/emailValidation';
import { formatMobileNumber } from '../../utils/format.util';
import HTMLConstants from '../../utils/html.constants';
import './OrderNotificationPreferences.scss';

interface IOrderNotificationPreferencesProps {
  setIsEmailEntered: (e: boolean) => void;
  setIsNotificationsChecked: (e: boolean) => void;
  setIsReceiptsChecked: (e: boolean) => void;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  setNotificationPreferences: any;
  msisdns: string[];
}

const OrderNotificationPreferences: FunctionComponent<IOrderNotificationPreferencesProps> = ({
  setIsEmailEntered,
  setIsNotificationsChecked,
  setIsReceiptsChecked,
  setNotificationPreferences,
  msisdns,
}) => {
  const [emailInput, setEmailInput] = useState('');
  const [emailNotification, setEmailNotification] = useState(true);
  const [sms, setSMS] = useState(false);
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const [options, setOptions] = useState<any>([]);
  const [selectedLanguage, setSelectedLanguage] = useState();
  const [selectedMsisdn, setSelectedMsisdn] = useState(null);
  const [selectedReceipt, setSelectedReceipt] = useState(HTMLConstants.EMAIL_ADDRESS);
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const [isEmailValid, setIsEmailValid] = useState<any>(false);
  const [languageUpdateModal, setLanguageUpdateModal] = useState(false);
  const [languageUpdateConfirmation, setLanguageUpdateConfirmation] = useState(undefined);

  const languages = [HTMLConstants.ENGLISH, HTMLConstants.SPANISH];
  const receipts = [HTMLConstants.EMAIL_ADDRESS, HTMLConstants.BILLING_ADDRESS];

  const handleEmailInput = (email: string) => {
    setEmailInput(email);
    setIsEmailValid(hasValidEmail(email) && email !== '');
  };

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const handleLanguagePreferenceChange = (event: any) => {
    setLanguageUpdateModal(true);
    setLanguageUpdateConfirmation(event.value);
  };

  const handleLanguageConfirmation = () => {
    setLanguageUpdateModal(false);
    setSelectedLanguage(languageUpdateConfirmation);
  };

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const handleChangeMsisdn = (event: any) => {
    if (event?.label) {
      setSelectedMsisdn(event?.value);
    }
  };

  useEffect(() => {
    const options = msisdns.map((option) => {
      return {
        label: formatMobileNumber(option),
        value: option,
      };
    });
    setOptions(options);
  }, [msisdns]);

  useEffect(() => {
    const generateNotificationDetails = (): void => {
      const notificationDetailRequest: INotificationDetailRequest = {
        languagePreference: selectedLanguage === HTMLConstants.ENGLISH ? HTMLConstants.EN : HTMLConstants.ES,
        notificationDetails: [
          {
            notificationMode:
              selectedReceipt === HTMLConstants.EMAIL_ADDRESS
                ? HTMLConstants.NOTIFICATION_MODE_EMAIL
                : HTMLConstants.NOTIFICATION_MODE_HARDCOPY,
            value: selectedReceipt === HTMLConstants.EMAIL_ADDRESS ? emailInput : undefined,
            notificationType: HTMLConstants.NOTIFICATION_TYPE_RECEIPT,
          },
        ],
      };

      if (sms) {
        notificationDetailRequest.notificationDetails.push({
          notificationMode: HTMLConstants.NOTIFICATION_MODE_SMS,
          value: selectedMsisdn ? selectedMsisdn : '',
          notificationType: HTMLConstants.NOTIFICATION_TYPE_NOTIFICATION,
        });
      }

      if (emailNotification) {
        notificationDetailRequest.notificationDetails.push({
          notificationMode: HTMLConstants.NOTIFICATION_MODE_EMAIL,
          value: emailInput,
          notificationType: HTMLConstants.NOTIFICATION_TYPE_NOTIFICATION,
        });
      }

      setNotificationPreferences(notificationDetailRequest);
    };
    setIsEmailEntered(isEmailValid);
    setIsNotificationsChecked(emailNotification || sms);
    setIsReceiptsChecked(selectedReceipt !== undefined);
    generateNotificationDetails();
  }, [
    isEmailValid,
    emailInput,
    selectedLanguage,
    setNotificationPreferences,
    emailNotification,
    sms,
    selectedMsisdn,
    selectedReceipt,
    setIsEmailEntered,
    setIsNotificationsChecked,
    setIsReceiptsChecked,
  ]);

  return (
    <div className='notification-preferences-container' data-testid='order-notification'>
      <hr className='mb-6 line-divider' />
      <div className='order-notification-header'>{HTMLConstants.ORDER_NOTIFICATION_PREFERENCES}</div>
      {languages.map((language, index) => {
        return (
          <div key={index} data-testid={language} className='my-3'>
            <RadioButton
              inputId={language}
              name='language-preference'
              value={language}
              onChange={(event) => handleLanguagePreferenceChange(event)}
              checked={selectedLanguage === language}
            />
            <label className='ml-3' htmlFor={language}>
              {language}
            </label>
          </div>
        );
      })}
      <Dialog showHeader={false} visible={languageUpdateModal} onHide={() => setLanguageUpdateModal(false)}>
        <div className='language-update-container pt-5 px-4'>
          <img className='tmo-icon' src={TmoIcon} alt='t-mobile icon' />
          <i role='button' tabIndex={0} className='pi pi-times' onClick={() => setLanguageUpdateModal(false)}></i>
          <h3 className='pt-3'>{HTMLConstants.LANGUAGE_UPDATE}</h3>
          <p className='pt-3'>{HTMLConstants.MODIFY_LANGUAGE_TEXT}</p>
          <div className='col-12 buttons-container button-spacing'>
            <div className='button-box'>
              <button className='btn btn-secondary' type='button' onClick={() => setLanguageUpdateModal(false)}>
                Cancel
              </button>
            </div>
            <div className='button-box'>
              <button
                className='btn btn-primary'
                type='button'
                onClick={() => {
                  handleLanguageConfirmation();
                }}
              >
                Continue
              </button>
            </div>
          </div>
        </div>
      </Dialog>
      <div className='preference-title'>{HTMLConstants.DELIVERY_OPTIONS}</div>
      <span className='p-float-label p-input-icon-right' data-testid='email-box'>
        <img
          role='presentation'
          src={ClearIcon}
          onClick={() => handleEmailInput('')}
          className='clear-icon'
          alt='clear'
        />
        <InputText
          data-testid='email-input'
          className={`${!hasValidEmail(emailInput) && 'p-invalid block'} email-input`}
          value={emailInput}
          onChange={(event) => handleEmailInput(event.target.value)}
        />
        <label className={`${!hasValidEmail(emailInput) && 'p-error'} `} htmlFor='emailAddress'>
          {HTMLConstants.EMAIL_ADDRESS}
        </label>
        {!hasValidEmail(emailInput) && (
          <div>
            <img src={ErrorIcon} alt='error-icon' />
            <small className='p-error'>{HTMLConstants.INVALID_ADDRESS}</small>
          </div>
        )}
      </span>
      <div className='p-grid my-4 ml-1'>
        <div className='notifications-container mr-8' data-testid='notifications'>
          <div className='preference-title'>{HTMLConstants.NOTIFICATIONS}</div>
          <div className='my-3' data-testid='email-notification'>
            <Checkbox
              inputId='emailNotification'
              onChange={(event) => {
                setEmailNotification(event.checked);
              }}
              checked={emailNotification}
            ></Checkbox>
            <label htmlFor='emailNotification' className={`order-prefrences-line ml-3`}>
              {HTMLConstants.EMAIL_ADDRESS}
            </label>
          </div>
          <div className='my-3' data-testid='sms-notification'>
            <Checkbox
              inputId='sms'
              onChange={(event) => {
                setSMS(event.checked);
              }}
              checked={sms}
            ></Checkbox>

            <label htmlFor='sms' className={`order-prefrences-line ml-3`}>
              {HTMLConstants.SMS_MESSAGE}
            </label>
            <div>
              <Select className='mySelect' onChange={(event: any) => handleChangeMsisdn(event)} options={options} />
            </div>
          </div>
        </div>
        <div className='receipts-container mr-8' data-testid='receipts'>
          <div className='preference-title'>{HTMLConstants.RECEIPTS}</div>
          {receipts.map((receipt, index) => {
            return (
              <div key={index} className='my-3'>
                <RadioButton
                  inputId={receipt}
                  name='receipt'
                  value={receipt}
                  onChange={(event) => setSelectedReceipt(event.value)}
                  checked={selectedReceipt === receipt}
                />
                <label className={`order-prefrences-line ml-3`} htmlFor={receipt}>
                  {receipt}
                </label>
              </div>
            );
          })}
        </div>
        <div className='documents-container mr-5' data-testid='documents'>
          <div className='preference-title'>
            {HTMLConstants.DOCUMENTS}
            <Tooltip enable displayText={HTMLConstants.DOCUMENTS_TOOLTIP_TEXT}>
              <span className='tooltip-circle'>
                <img src={TooltipIcon} className='tooltip-img' alt='tooltip' />
              </span>
            </Tooltip>
          </div>
          <div className='my-3'>
            <Checkbox inputId='emailDocuments' checked></Checkbox>
            <label htmlFor='emailDocuments' className={`order-prefrences-line ml-3`}>
              {HTMLConstants.EMAIL_ADDRESS}
            </label>
          </div>
        </div>
      </div>
      <hr className='mb-6 line-divider' />
    </div>
  );
};

export default OrderNotificationPreferences;
