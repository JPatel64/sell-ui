import { GlobalNavUtil } from '@tmobile/atlas-util';
import React, { FunctionComponent } from 'react';
import { useLocation } from 'react-router-dom';
import HTMLConstants from '../../utils/html.constants';
import './OrderConfirmation.scss';

const OrderConfirmation: FunctionComponent = () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const location: any = useLocation();
  const orderNumber = location.state?.orderNumber;
  const totalCharges = location.state?.totalCharges;

  const navigateToShopNow = () => {
    GlobalNavUtil.launchMicroApp('shop-now');
  };

  const navigateToOrderLookUp = () => {
    GlobalNavUtil.launchMicroApp('order-lookup');
  };

  return (
    <div className='confirmation-preferences-container' data-testid='order-confirmation'>
      <div className='pl-3 pt-3'>
        <h1 data-testid='order-confirmation-header'>{HTMLConstants.ORDER_CONFIRMATION}</h1>
      </div>
      <hr className='line-divider-header' />
      <div className='pl-3'>
        <span className='p-float-label my-4' data-testid='transaction-detail'>
          <div className='order-confirmation-header'>{HTMLConstants.TRANSACTION_DETAILS}</div>
        </span>
        <div className='p-float-label'>
          <div className='mr-6' data-testid='confirmations-notificaiton-box'>
            <div className='preference-font' data-testid='confirmations-notificaiton'>
              <i className='pi pi-check mr-2'></i>
              {HTMLConstants.CONFIRMATION_NOTICE}
            </div>
          </div>
          <div className='p-grid my-4' data-testid='confirmations-info-container'>
            <div className='p-col-2 mr-6'>
              <div className='preference-title' data-testid='confirmations-info-amount-title'>
                {HTMLConstants.AMOUNT_CHARGE}
              </div>
              <div className='preference-font' data-testid='confirmations-info-amount'>
                {totalCharges}
              </div>
            </div>
            <div className='p-col-6 mr-6'>
              <div className='preference-title' data-testid='confirmations-info-order-title'>
                {HTMLConstants.ORDER_NUMBER}
              </div>
              <div className='preference-font' data-testid='confirmations-info-order'>
                {orderNumber}
              </div>
            </div>
          </div>
        </div>
      </div>

      <hr className=' line-divider' />
      <div className='container my-4' data-testid='confirmations-info-container'>
        <div className='row'>
          <div className='col-12 buttons-container button-spacing'>
            <div className='button-box'>
              <button
                data-testid='order-lookup'
                className='btn btn-secondary'
                type='button'
                onClick={() => navigateToOrderLookUp()}
              >
                Order lookup
              </button>
            </div>
            <div className='button-box'>
              <button
                data-testid='shop-now'
                className='btn btn-primary'
                type='button'
                onClick={() => navigateToShopNow()}
              >
                Shop now
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default OrderConfirmation;
