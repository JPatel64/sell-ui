import { fireEvent, render, screen } from '@testing-library/react';
import React from 'react';
import OrderConfirmation from './OrderConfirmation';

jest.mock('@tmobile/atlas-util');
const mockedNavigate = jest.fn();
jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
  useLocation: () => ({
    pathname: 'payment',
  }),
  useNavigate: () => mockedNavigate,
}));

describe('OrderConfirmation component', () => {
  it('should render without crashing', () => {
    render(<OrderConfirmation />);

    expect(screen.getByTestId('order-confirmation-header')).toHaveTextContent('Order confirmation');

    expect(screen.getByTestId('transaction-detail')).toHaveTextContent('Transaction details');
    expect(screen.getByTestId('confirmations-notificaiton')).toHaveTextContent('Your order has been placed.');

    expect(screen.getByTestId('confirmations-info-amount-title')).toHaveTextContent('Amount charge today');

    expect(screen.getByTestId('confirmations-info-order-title')).toHaveTextContent('Order number');

    fireEvent.click(screen.getByTestId('order-lookup'));
    fireEvent.click(screen.getByTestId('shop-now'));
  });
});
