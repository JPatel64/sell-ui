import { fireEvent, render, screen } from '@testing-library/react';
import React from 'react';
import HTMLConstants from '../../utils/html.constants';
import ShippingAddress from './ShippingAddress';

describe('Shipping Address component', () => {
  it('should render without crashing', () => {
    const mockShippingInfo = {
      firstName: 'John',
      middleInitial: undefined,
      lastName: 'Watson',
      address1: '40631 Peachtree Dr NE',
      address2: undefined,
      city: 'Marysville',
      state: 'WA',
      zipCode: '98259',
    };
    render(<ShippingAddress shippingAddress={mockShippingInfo} />);

    expect(screen.getByTestId('shipping-address-header')).toHaveTextContent('Shipping address');
    expect(screen.getByTestId('shipping-address-header')).toHaveTextContent('Edit');

    expect(screen.getByTestId('display-shipping-address')).toHaveTextContent('John');
    expect(screen.getByTestId('display-shipping-address')).toHaveTextContent('Watson');
    expect(screen.getByTestId('display-shipping-address')).toHaveTextContent('40631 Peachtree Dr NE');

    fireEvent.click(screen.getByText(HTMLConstants.EDIT));
  });
});
