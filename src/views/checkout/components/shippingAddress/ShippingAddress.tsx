import React, { FunctionComponent, useState } from 'react';
import { Address } from '../../interfaces';
import HTMLConstants from '../../utils/html.constants';
import EditShippingAddress from '../editShippingAddress/EditShippingAddress';
import './ShippingAddress.scss';

interface IShippingAddressProps {
  shippingAddress: Address;
}

const ShippingAddress: FunctionComponent<IShippingAddressProps> = ({ shippingAddress }) => {
  const [displayEditAddress, setDisplayEditAddress] = useState(false);
  const [selectedShippingAddress, setSelectedShippingAddress] = useState<Address>(shippingAddress);
  const handleEditClick = (event: React.MouseEvent) => {
    event.preventDefault();
    setDisplayEditAddress(true);
  };

  return (
    <div className='shipping-address-container'>
      <div data-testid='shipping-address-header' className='shipping-address-header'>
        {HTMLConstants.SHIPPING_ADDRESS}
        {!displayEditAddress && (
          <a href='/' className='edit-link ml-2' onClick={handleEditClick}>
            {HTMLConstants.EDIT}
          </a>
        )}
      </div>
      {displayEditAddress ? (
        <EditShippingAddress
          address={selectedShippingAddress}
          handleCancelBtn={setDisplayEditAddress}
          setSelectedShippingAddress={setSelectedShippingAddress}
        />
      ) : (
        <div data-testid='display-shipping-address' className='display-shipping-address mt-3 mb-4'>
          <div>{`${selectedShippingAddress.firstName ?? ''} ${selectedShippingAddress.middleName ?? ''} ${
            selectedShippingAddress.lastName ?? ''
          }`}</div>
          {selectedShippingAddress.address3 && <div>{selectedShippingAddress.address3}</div>}
          {selectedShippingAddress.address1 && <div>{selectedShippingAddress.address1}</div>}
          {selectedShippingAddress.address2 && <div>{selectedShippingAddress.address2}</div>}
          <div>
            {`${selectedShippingAddress.city ?? ''}, ${selectedShippingAddress.state ?? ''} ${
              selectedShippingAddress.zipCode ?? ''
            }`}{' '}
          </div>
        </div>
      )}
    </div>
  );
};

export default ShippingAddress;
