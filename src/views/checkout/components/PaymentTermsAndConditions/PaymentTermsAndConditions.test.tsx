import { fireEvent, render, screen } from '@testing-library/react';
import React from 'react';
import HTMLConstants from '../../utils/html.constants';
import PaymentTermsAndConditions from './PaymentTermsAndConditions';

describe('OrderNotificationPreferences component', () => {
  const terms = {
    english: 'Terms and Conditions English',
    spanish: 'Terms and Conditions Spanish',
  };
  it('should render without crashing', async () => {
    render(<PaymentTermsAndConditions terms={terms} totalCharges={''} setIsAcceptedTC={() => true} />);
    expect(screen.getByText(HTMLConstants.CUSTOMER_ADVISEMENT)).toBeInTheDocument();
    expect(screen.getByTestId('English-radioBtn')).toHaveTextContent('English');
    expect(screen.getByTestId('Spanish-radioBtn')).toHaveTextContent('Spanish');
    expect(screen.getByText('Terms and Conditions English')).toBeInTheDocument();
    expect(screen.getByText(HTMLConstants.CUSTOMER_ACCEPTS_TC)).toBeInTheDocument();

    const radioButtons = screen.findAllByRole('radio');
    (await radioButtons).forEach((button) => {
      fireEvent.click(button, { target: { checked: true } });
    });

    const checkbox = screen.getByTestId('checkbox-tc').firstChild;
    expect(checkbox).not.toBeChecked();
    if (checkbox) fireEvent.click(checkbox, { target: { checked: true } });
  });
});
