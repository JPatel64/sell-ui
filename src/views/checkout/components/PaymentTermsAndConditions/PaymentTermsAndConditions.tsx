import { Checkbox } from 'primereact/checkbox';
import { RadioButton } from 'primereact/radiobutton';
import { ScrollPanel } from 'primereact/scrollpanel';
import React, { FunctionComponent, useState } from 'react';
import HTMLConstants from '../../utils/html.constants';
import './PaymentTermsAndConditions.scss';

interface IPaymentTermsAndConditionsProps {
  terms: { english: string; spanish: string };
  totalCharges: string;
  setIsAcceptedTC: React.Dispatch<React.SetStateAction<boolean>>;
}

const PaymentTermsAndConditions: FunctionComponent<IPaymentTermsAndConditionsProps> = ({
  terms,
  totalCharges,
  setIsAcceptedTC,
}) => {
  const [selectedLanguage, setSelectedLanguage] = useState(HTMLConstants.ENGLISH);
  const [isAccepted, setIsAccepted] = useState(false);
  const languages = [HTMLConstants.ENGLISH, HTMLConstants.SPANISH];
  const replaceTotalCharges = (tc: string) => {
    return tc.replace('{totalCharge}', totalCharges);
  };
  return (
    <div className='tc-container' data-testid='terms-conditions'>
      <div className='tc-title'>{HTMLConstants.CUSTOMER_ADVISEMENT}</div>
      <div className='language-container'>
        {languages.map((language: string, index: number) => {
          return (
            <div key={index} data-testid={`${language}-radioBtn`} className='my-3'>
              <RadioButton
                inputId={language}
                name='language-preference'
                value={language}
                onChange={(event) => setSelectedLanguage(event.value)}
                checked={selectedLanguage === language}
              />
              <label className='ml-3' htmlFor={language}>
                {language}
              </label>
            </div>
          );
        })}
        <div className='tc-content-heading'>{HTMLConstants.TERM_CONDITION_PAYMENT}</div>
        <ScrollPanel className='tc-scroll'>
          <div
            //Allows for string to be set with HTML elements
            dangerouslySetInnerHTML={{
              __html:
                selectedLanguage === HTMLConstants.ENGLISH
                  ? replaceTotalCharges(terms.english)
                  : replaceTotalCharges(terms.spanish),
            }}
          ></div>
        </ScrollPanel>
        <div className='my-4' data-testid='checkbox-tc'>
          <Checkbox
            inputId='checkbox-tc'
            onChange={(event) => {
              setIsAccepted(event.checked);
              setIsAcceptedTC(event.checked);
            }}
            checked={isAccepted}
          ></Checkbox>
          <label htmlFor='checkbox-tc' className='ml-3'>
            {HTMLConstants.CUSTOMER_ACCEPTS_TC}
          </label>
        </div>
      </div>
    </div>
  );
};

export default PaymentTermsAndConditions;
