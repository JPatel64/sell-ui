import '@testing-library/jest-dom/extend-expect';
import { render, screen } from '@testing-library/react';
import React from 'react';
import mockQuoteData from '../../../../../mocks/responses/tms.quote.response.json';
import { Cart } from '../../interfaces/cart';
import { formatCurrency } from '../../utils/format.util';
import OrderSummary from './OrderSummary';
describe('Order summary Component', () => {
  it('should render without crashing', () => {
    const mockProps: Cart = mockQuoteData as unknown as Cart;
    const cost = 12;

    render(<OrderSummary cartData={mockProps} shippingCost={cost} />);

    screen.findByDisplayValue('Order summary');
    expect(screen.getByTestId('subtotal-label')).toHaveTextContent('Subtotal (pre-tax)');
    expect(screen.getByTestId('subtotal-value')).toHaveTextContent(`${formatCurrency(mockProps.subTotal)}`);
    screen.findByDisplayValue('Estimated due today');
    expect(screen.getByTestId('subtotal-label')).toHaveTextContent('Subtotal (pre-tax)');
    expect(screen.getByTestId('subtotal-value')).toHaveTextContent(`${formatCurrency(mockProps.subTotal)}`);

    expect(screen.getByTestId('discounts-label')).toHaveTextContent('Discounts and credit');
    expect(screen.getByTestId('discounts-value')).toHaveTextContent(
      `(${formatCurrency(mockProps.credit.oneTimeCredit)})`,
    );

    expect(screen.getByTestId('tax-label')).toHaveTextContent('Tax');
    expect(screen.getByTestId('tax-value')).toHaveTextContent(`${formatCurrency(mockProps.tax?.total)}`);

    expect(screen.getByTestId('shipping-label')).toHaveTextContent('Shipping');
    expect(screen.getByTestId('shipping-value')).toHaveTextContent(`${formatCurrency(cost)}`);

    expect(screen.getByTestId('total-label')).toHaveTextContent('Total due today (estimated)');
    expect(screen.getByTestId('total-value')).toHaveTextContent(`${formatCurrency(mockProps.total + cost)}`);
  });
});
