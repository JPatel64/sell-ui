import React, { FC, useEffect, useState } from 'react';
import { Cart } from '../../interfaces/cart';
import { formatCurrency } from '../../utils/format.util';
import HTMLConstants from '../../utils/html.constants';
import './OrderSummary.scss';

interface OrderSummaryProps {
  cartData: Cart | undefined;
  shippingCost: number | 0;
}

const OrderSummary: FC<OrderSummaryProps> = ({ cartData, shippingCost }) => {
  const [totalCharges, setTotalCharges] = useState(0);

  useEffect(() => {
    let total = 0;
    cartData?.lines?.forEach((line) =>
      line.charges?.forEach((charge) => {
        total += charge.amount;
      }),
    );

    setTotalCharges(total);
  }, [cartData]);

  return (
    <>
      {cartData && (
        <div className='orderSummary' data-testid='orderSummary'>
          <hr className='mb-6 line-divider' />
          <div className='order-summary-header'>{HTMLConstants.ORDER_SUMMARY}</div>
          <div className='row'>
            <div className='col-2 items' data-testid='subtotal-label'>
              {HTMLConstants.SUBTOTAL_PRETAX}
            </div>
            <div className='col-10 items' data-testid='subtotal-value'>
              {formatCurrency(cartData?.subTotal)}
            </div>
          </div>
          <div className='row'>
            <div className='col-2 items' data-testid='discounts-label'>
              {HTMLConstants.DISCOUNTS_CREDITS}
            </div>
            <div className='col-10 items' data-testid='discounts-value'>
              ({formatCurrency(cartData?.credit.oneTimeCredit)})
            </div>
          </div>
          <div className='row'>
            <div className='col-2 items' data-testid='deposits-label'>
              {HTMLConstants.DEPOSITS_CHARGES}
            </div>
            <div className='col-10 items' data-testid='deposits-value'>
              {formatCurrency(totalCharges)}
            </div>
          </div>
          <div className='row'>
            <div className='col-2 items' data-testid='tax-label'>
              {HTMLConstants.TAX}
            </div>
            <div className='col-10 items' data-testid='tax-value'>
              {formatCurrency(cartData?.tax?.total)}
            </div>
          </div>
          <div className='row'>
            <div className='col-2 items' data-testid='shipping-label'>
              {HTMLConstants.SHIPPING}
            </div>
            <div className='col-10 items' data-testid='shipping-value'>
              {formatCurrency(shippingCost)}
            </div>
          </div>
          <div className='row'>
            <div className='col-2 bold' data-testid='total-label'>
              <div className='preference-title'>{HTMLConstants.TOTAL_DUE_TODAY}</div>
            </div>
            <div className='col-10 bold' data-testid='total-value'>
              {formatCurrency(cartData?.total + shippingCost)}
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default OrderSummary;
