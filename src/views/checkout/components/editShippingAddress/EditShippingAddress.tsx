import { http } from '@tmobile/atlas-util';
import { Dialog } from 'primereact/dialog';
import { Dropdown } from 'primereact/dropdown';
import { InputText } from 'primereact/inputtext';
import React, { FunctionComponent, useEffect, useState } from 'react';
import ClearIcon from '../../../../assets/clearIcon.svg';
import TmoIcon from '../../../../assets/tmoIcon.png';
import { Address } from '../../interfaces';
import { IVerifiedAddress } from '../../interfaces/shipping/IVerifiedAddress';
import { IVerifyAddressRequest } from '../../interfaces/shipping/IVerifyAddressRequest';
import { IVerifyAddressResponse } from '../../interfaces/shipping/IVerifyAddressResponse';
import { PROFILE_BASE_URL, VALIDATE_ZIPCODE_URL, VERIFY_ADDRESS_URL } from '../../utils/app.constants';
import { getGatewayUrl } from '../../utils/auth.util';
import HTMLConstants from '../../utils/html.constants';
import './EditShippingAddress.scss';

interface IEditShippingAddressProps {
  address: Address;
  handleCancelBtn: (e: boolean) => void;
  setSelectedShippingAddress: (e: Address) => void;
}

const EditShippingAddress: FunctionComponent<IEditShippingAddressProps> = ({
  address,
  handleCancelBtn,
  setSelectedShippingAddress,
}) => {
  const [addressFields, setAddressFields] = useState({
    firstName: address.firstName ?? '',
    middleName: address.middleName ?? '',
    lastName: address.lastName ?? '',
    address1: address.address1 ?? '',
    address2: address.address2 ?? '',
    address3: address.address3 ?? '',
    zipCode: address.zipCode ?? '',
    city: address.city ?? '',
    state: address.state ?? '',
  });
  const [showUrbanizationField, setShowUrbanizationField] = useState<boolean>(false);
  const [disableValidate, setDisableValidate] = useState<boolean>(true);
  const [showValidateAddressModal, setShowValidateAddressModal] = useState<boolean>(false);
  const [verifiedAddress, setVerifiedAddress] = useState<IVerifiedAddress>();
  const [selectedAddressOption, setSelectedAddressOption] = useState<string>('');

  useEffect(() => {
    //If required fields are empty then disable validate button
    if (
      addressFields.address1 === '' ||
      addressFields.city === '' ||
      addressFields.state === '' ||
      addressFields.zipCode === ''
    ) {
      setDisableValidate(true);
    }
    //Show Urbanization Field for Puerto Rico
    if (addressFields.state === 'PR') setShowUrbanizationField(true);
  }, [addressFields.address1, addressFields.city, addressFields.state, addressFields.zipCode]);

  const handleEnableValidate = () => {
    //Only Enable Validate Button if required fields are not empty
    if (
      addressFields.address1 !== '' &&
      addressFields.city !== '' &&
      addressFields.state !== '' &&
      addressFields.zipCode !== ''
    ) {
      setDisableValidate(false);
    }
  };

  const handleValidateBtn = async () => {
    const addressToVerifyModel: IVerifyAddressRequest = {
      addressToVerify: {
        international: false,
        address1: addressFields.address1,
        address2: addressFields.address2,
        city: addressFields.city,
        state: addressFields.state,
        zipCode: addressFields.zipCode,
      },
    };
    try {
      const { data } = await http.post<IVerifyAddressResponse>(
        `${getGatewayUrl()}${VERIFY_ADDRESS_URL}`,
        addressToVerifyModel,
      );
      if (data.isChangeInAddress) {
        setVerifiedAddress(data.verifiedAddress);
        setShowValidateAddressModal(true);
      }
    } catch (e) {
      console.error(e);
      throw e;
    }
  };

  const stateOptions = [
    'AL',
    'AK',
    'AS',
    'AZ',
    'AR',
    'CA',
    'CO',
    'CT',
    'DE',
    'DC',
    'FM',
    'FL',
    'GA',
    'GU',
    'HI',
    'ID',
    'IL',
    'IN',
    'IA',
    'KS',
    'KY',
    'LA',
    'ME',
    'MH',
    'MD',
    'MA',
    'MI',
    'MN',
    'MS',
    'MO',
    'MT',
    'NE',
    'NV',
    'NH',
    'NJ',
    'NM',
    'NY',
    'NC',
    'ND',
    'MP',
    'OH',
    'OK',
    'OR',
    'PW',
    'PA',
    'PR',
    'RI',
    'SC',
    'SD',
    'TN',
    'TX',
    'UT',
    'VT',
    'VI',
    'VA',
    'WA',
    'WV',
    'WI',
    'WY',
  ].map((v) => ({ name: v, code: v }));

  const validateZipcode = async (zipCode: string) => {
    if (zipCode.length >= 5) {
      try {
        await http.get(`${getGatewayUrl()}${PROFILE_BASE_URL}${VALIDATE_ZIPCODE_URL}${zipCode}`);
      } catch (e) {
        console.error(e);
        throw e;
      }
    }
  };

  const addressSelectionChanged = () => {
    const addr: Address = {
      firstName: addressFields.firstName,
      middleName: addressFields.middleName,
      lastName: addressFields.lastName,
      address1: selectedAddressOption === 'verifiedAddress' ? verifiedAddress?.address1 : addressFields.address1,
      address2: selectedAddressOption === 'verifiedAddress' ? verifiedAddress?.address2 : addressFields.address2,
      address3: selectedAddressOption === 'verifiedAddress' ? verifiedAddress?.address3 : addressFields.address3,
      city: selectedAddressOption === 'verifiedAddress' ? verifiedAddress?.city : addressFields.city,
      state: selectedAddressOption === 'verifiedAddress' ? verifiedAddress?.state : addressFields.state,
      zipCode: selectedAddressOption === 'verifiedAddress' ? verifiedAddress?.zipCode : addressFields.zipCode,
    };
    setSelectedShippingAddress(addr);
    setShowValidateAddressModal(false);
    handleCancelBtn(false);
  };

  return (
    <div className='edit-shipping-container'>
      <div className='my-2'>
        <span className='p-float-label p-input-icon-right' data-testid='first-name'>
          <img
            role='presentation'
            src={ClearIcon}
            onClick={() =>
              setAddressFields((prevState) => ({
                ...prevState,
                firstName: '',
              }))
            }
            className='clear-icon'
            alt='clear'
          />
          <InputText
            data-testid='first-name-input'
            className={`firstName-edit-input`}
            value={addressFields.firstName}
            onChange={(event) =>
              setAddressFields((prevState) => ({
                ...prevState,
                firstName: event.target.value,
              }))
            }
          />
          <label htmlFor='firstName'>{HTMLConstants.FIRST_NAME}</label>
        </span>
        <span className='p-float-label p-input-icon-right' data-testid='middle-name'>
          <InputText
            data-testid='middle-name-input'
            className={`mi-edit-input`}
            value={addressFields.middleName}
            onChange={(event) =>
              setAddressFields((prevState) => ({
                ...prevState,
                middleName: event.target.value.length > 1 ? event.target.value.substring(0, 1) : event.target.value,
              }))
            }
          />
          <label htmlFor='middleName'>{HTMLConstants.MIDDLE_INITIAL}</label>
        </span>
        <span className='p-float-label p-input-icon-right' data-testid='last-name'>
          <img
            role='presentation'
            src={ClearIcon}
            onClick={() =>
              setAddressFields((prevState) => ({
                ...prevState,
                lastName: '',
              }))
            }
            className='clear-icon'
            alt='clear'
          />
          <InputText
            data-testid='last-name-input'
            className={`edit-input`}
            value={addressFields.lastName}
            onChange={(event) =>
              setAddressFields((prevState) => ({
                ...prevState,
                lastName: event.target.value,
              }))
            }
          />
          <label htmlFor='lastName'>{HTMLConstants.LAST_NAME}</label>
        </span>
      </div>
      <div className='my-2'>
        <span className='p-float-label p-input-icon-right' data-testid='address-1'>
          <img
            role='presentation'
            src={ClearIcon}
            onClick={() =>
              setAddressFields((prevState) => ({
                ...prevState,
                address1: '',
              }))
            }
            className='clear-icon'
            alt='clear'
          />
          <InputText
            data-testid='address-1-input'
            className={`edit-input`}
            value={addressFields.address1}
            onChange={(event) => {
              setAddressFields((prevState) => ({
                ...prevState,
                address1: event.target.value,
              }));
              handleEnableValidate();
            }}
          />
          <label htmlFor='address1'>{HTMLConstants.ADDRESS_1}</label>
        </span>
        <span className='p-float-label p-input-icon-right' data-testid='address-2'>
          <InputText
            data-testid='address-2-input'
            className={`edit-input`}
            value={addressFields.address2}
            onChange={(event) =>
              setAddressFields((prevState) => ({
                ...prevState,
                address2: event.target.value,
              }))
            }
          />
          <label htmlFor='address2'>{HTMLConstants.ADDRESS_2}</label>
        </span>
      </div>
      <div className='my-2'>
        <span className='p-float-label p-input-icon-right' data-testid='zip'>
          <img
            role='presentation'
            src={ClearIcon}
            onClick={() =>
              setAddressFields((prevState) => ({
                ...prevState,
                zipCode: '',
              }))
            }
            className='clear-icon'
            alt='clear'
          />
          <InputText
            data-testid='zip-input'
            className={`edit-input`}
            value={addressFields.zipCode}
            onChange={(event) => {
              setAddressFields((prevState) => ({
                ...prevState,
                zipCode: event.target.value,
              }));
              validateZipcode(event.target.value);
              handleEnableValidate();
            }}
          />
          <label htmlFor='zipCode'>{HTMLConstants.ZIP_CODE}</label>
        </span>

        <span className='p-float-label p-input-icon-right' data-testid='city'>
          <img
            role='presentation'
            src={ClearIcon}
            onClick={() =>
              setAddressFields((prevState) => ({
                ...prevState,
                city: '',
              }))
            }
            className='clear-icon'
            alt='clear'
          />
          <InputText
            data-testid='city-input'
            className={`edit-input`}
            value={addressFields.city}
            onChange={(event) => {
              setAddressFields((prevState) => ({
                ...prevState,
                city: event.target.value,
              }));
              handleEnableValidate();
            }}
          />
          <label htmlFor='city'>{HTMLConstants.CITY}</label>
        </span>
      </div>
      <Dropdown
        value={addressFields.state}
        options={stateOptions}
        onChange={(event) => {
          setAddressFields((prevState) => ({
            ...prevState,
            state: event.target.value.name,
          }));
          handleEnableValidate();
          if (event.target.value.name === 'PR') {
            setShowUrbanizationField(true);
          }
        }}
        optionLabel='name'
        placeholder={addressFields.state ?? ''}
      />
      {showUrbanizationField && (
        <span className='p-float-label p-input-icon-right ml-4' data-testid='city'>
          <img
            role='presentation'
            src={ClearIcon}
            onClick={() =>
              setAddressFields((prevState) => ({
                ...prevState,
                address3: '',
              }))
            }
            className='clear-icon'
            alt='clear'
          />
          <InputText
            data-testid='urbanization-code'
            className={`edit-input`}
            value={addressFields.address3}
            onChange={(event) => {
              setAddressFields((prevState) => ({
                ...prevState,
                address3: event.target.value,
              }));
            }}
          />
          <label htmlFor='urbanization-code'>{HTMLConstants.URBANIZATION_CODE}</label>
        </span>
      )}

      <div className='validate-button-group my-4' style={{ textAlign: 'center' }}>
        <button
          className='btn cancel-btn p-col mx-4 btn btn-secondary'
          data-testid='cancel-button'
          onClick={() => handleCancelBtn(false)}
        >
          Cancel
        </button>
        <button
          data-testid='validate-button'
          disabled={disableValidate}
          className={`btn btn-primary p-col mx-4 ${disableValidate && 'continue-btn-disabled'}`}
          onClick={() => handleValidateBtn()}
        >
          Validate
        </button>
      </div>
      <Dialog
        showHeader={false}
        visible={showValidateAddressModal}
        onHide={() => setShowValidateAddressModal(false)}
        style={{ width: '40%' }}
      >
        <div className='verify-address-container pt-5 px-4'>
          <img className='tmo-icon' src={TmoIcon} alt='t-mobile icon' />
          <i role='button' tabIndex={0} className='pi pi-times' onClick={() => setShowValidateAddressModal(false)}></i>
          <h3 className='pt-3'>{HTMLConstants.ADDRESS_TO_USE_QUESTION}</h3>
          <div className='address-header'>
            <input
              data-testid='verified-Address'
              type='radio'
              name='AddressToUse'
              value='verifiedAddress'
              defaultChecked
              onChange={() => setSelectedAddressOption('verifiedAddress')}
            />
            <div data-testid='validated-text' className='parent-div address-type'>
              {HTMLConstants.VALIDATED_ADDRESS}
            </div>
          </div>

          <div data-testid='display-verified-address' className='display-address'>
            <div>{`${addressFields.firstName ?? ''} ${addressFields.middleName ?? ''} ${
              addressFields.lastName ?? ''
            }`}</div>
            {verifiedAddress?.address3 && <div>{verifiedAddress?.address3}</div>}
            {verifiedAddress?.address1 && <div>{verifiedAddress?.address1}</div>}
            {verifiedAddress?.address2 && <div>{verifiedAddress?.address2}</div>}
            <div>
              {`${verifiedAddress?.city ?? ''}, ${verifiedAddress?.state ?? ''} ${verifiedAddress?.zipCode ?? ''}`}{' '}
            </div>
          </div>
          <div className='address-header'>
            <input
              data-testid='entered-Address'
              type='radio'
              name='AddressToUse'
              value='enteredAddress'
              onChange={() => setSelectedAddressOption('enteredAddress')}
            />
            <div data-testid='entered-text' className='parent-div address-type'>
              {HTMLConstants.ENTERED_ADDRESS}
            </div>
          </div>

          <div data-testid='display-entered-address' className='display-address'>
            <div>{`${addressFields.firstName ?? ''} ${addressFields.middleName ?? ''} ${
              addressFields.lastName ?? ''
            }`}</div>
            {<div>{addressFields.address3}</div>}
            {<div>{addressFields.address1}</div>}
            {<div>{addressFields.address2}</div>}
            <div>{`${addressFields.city}, ${addressFields.state} ${addressFields.zipCode}`} </div>
          </div>
          <div className='col-12 buttons-container button-spacing'>
            <div className='button-box'>
              <button className='btn btn-secondary' type='button' onClick={() => setShowValidateAddressModal(false)}>
                Cancel
              </button>
            </div>
            <div className='button-box'>
              <button className='btn btn-primary' type='button' onClick={() => addressSelectionChanged()}>
                Continue
              </button>
            </div>
          </div>
        </div>
      </Dialog>
    </div>
  );
};

export default EditShippingAddress;
