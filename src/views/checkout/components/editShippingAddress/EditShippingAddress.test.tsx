import { fireEvent, render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import React from 'react';
import EditShippingAddress from './EditShippingAddress';

describe('EditShippingAddress component', () => {
  const mockShippingInfo = {
    firstName: 'John',
    middleInitial: undefined,
    lastName: 'Watson',
    address1: '40631 Peachtree Dr NE',
    address2: undefined,
    city: 'Marysville',
    state: 'WA',
    zipCode: '98259',
  };

  const mockEmptyShippingInfo = {
    firstName: 'John',
    middleInitial: undefined,
    lastName: 'Watson',
    address1: '',
    address2: undefined,
    city: '',
    state: 'PR',
    zipCode: '',
  };

  it('should render without crashing', () => {
    render(
      <EditShippingAddress
        address={mockShippingInfo}
        handleCancelBtn={() => {
          return undefined;
        }}
        setSelectedShippingAddress={() => {
          return undefined;
        }}
      />,
    );

    expect(screen.getByTestId('first-name')).toHaveTextContent('First name');
    expect(screen.getByTestId('middle-name')).toHaveTextContent('MI');
    expect(screen.getByTestId('last-name')).toHaveTextContent('Last name');

    expect(screen.getByTestId('address-1')).toHaveTextContent('Address 1');

    expect(screen.getByTestId('address-2')).toHaveTextContent('Address 2 (Optional)');
    expect(screen.getByTestId('zip')).toHaveTextContent('ZIP code');
    expect(screen.getByTestId('city')).toHaveTextContent('City');
  });

  it('should render validate address modal', async () => {
    render(
      <EditShippingAddress
        address={mockShippingInfo}
        handleCancelBtn={() => {
          return undefined;
        }}
        setSelectedShippingAddress={() => {
          return undefined;
        }}
      />,
    );
    fireEvent.click(screen.getByTestId('validate-button'));
    fireEvent.click(screen.getByTestId('cancel-button'));

    const imgListItems = screen.getAllByRole('presentation');
    expect(imgListItems).toHaveLength(5);

    imgListItems.forEach((item) => {
      // You can import wihthin from @testing-library/react
      fireEvent.click(item);
    });

    userEvent.type(screen.getByTestId('city-input'), 'Washington');
    userEvent.type(screen.getByTestId('address-2-input'), '40631 Peachtree Dr NE');
    const validateButton = await screen.findByRole('button', { name: /Validate/i });
    fireEvent.click(validateButton);
    const cancelButton = await screen.findByRole('button', { name: /Cancel/i });
    fireEvent.click(cancelButton);

    // const verifiedAddress = await screen.findByTestId('verified-Address');
    // userEvent.type(verifiedAddress, 'verifiedAddress');
    // const enteredAddress = await screen.findByTestId('entered-Address');
    // userEvent.type(enteredAddress, 'enteredAddress');
  });

  it('should render with empty address modal with State PR', async () => {
    render(
      <EditShippingAddress
        address={mockEmptyShippingInfo}
        handleCancelBtn={() => {
          return undefined;
        }}
        setSelectedShippingAddress={() => {
          return undefined;
        }}
      />,
    );
    // fireEvent.click(screen.getByRole('presentation'));

    userEvent.type(screen.getByTestId('address-1-input'), '40631 Peachtree Dr NE');
    userEvent.type(screen.getByTestId('first-name-input'), 'Bob');
    userEvent.type(screen.getByTestId('last-name-input'), 'Shaw');
    userEvent.type(screen.getByTestId('zip-input'), '982593');
    userEvent.type(screen.getByTestId('city-input'), 'Washington');
    userEvent.type(screen.getByTestId('urbanization-code'), 'Washington');
    // userEvent.type(screen.getByTestId('validate-button'), { target: { disabled: false } }),
    //   fireEvent.click(screen.getByTestId('validate-button'));
  });
});
