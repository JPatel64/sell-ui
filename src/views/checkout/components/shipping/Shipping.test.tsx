import { fireEvent, render, screen } from '@testing-library/react';
import { GlobalNavUtil, storageUtil } from '@tmobile/atlas-util';
import React from 'react';
import mockResponse from '../../../../../mocks/responses/shippingOptionsResponse.json';
import OrderNotificationPreferences from '../orderNotificationPreferences/OrderNotificationPreferences';
import ShippingOptions from '../shippingOptions/ShippingOptions';
import Shipping from './Shipping';
const mockedNavigate = jest.fn();

jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
  useNavigate: () => mockedNavigate,
}));

it('renders without crashing', async () => {
  jest.spyOn(storageUtil, 'getCartId').mockReturnValue('cartid123');
  jest.spyOn(GlobalNavUtil, 'launchMicroApp').mockReturnValue();
  const mockShippingOptions = mockResponse.shippingOptions;

  render(
    <OrderNotificationPreferences
      setIsEmailEntered={() => true}
      setIsNotificationsChecked={() => true}
      setIsReceiptsChecked={() => true}
      setNotificationPreferences={() => undefined}
      msisdns={['']}
    />,
  );

  render(
    <ShippingOptions
      setShippingAdditionalInfo={() => {
        return '';
      }}
      shippingAdditionalInfo={''}
      shippingOptions={mockShippingOptions}
      selectedOption={undefined}
      // eslint-disable-next-line @typescript-eslint/no-empty-function
      setSelectedOption={() => {
        return undefined;
      }}
      isShippingWaived={false}
      isShippingAdditionalInfo={false}
      // eslint-disable-next-line @typescript-eslint/no-empty-function
      setIsShippingWaived={() => {
        return undefined;
      }}
      setIsShippingMethodSelected={() => {
        return true;
      }}
    />,
  );

  render(<Shipping />);
  await screen.findAllByTestId('shipping-method');
  await screen.findAllByTestId('shipping-options');
  const buttons = screen.getAllByRole('button');
  buttons.forEach((button) => {
    fireEvent.click(button);
  });
});
