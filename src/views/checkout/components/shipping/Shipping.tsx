import { GlobalNavUtil, http, storageUtil } from '@tmobile/atlas-util';
import { AxiosResponse } from 'axios';
import React, { FunctionComponent, useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { Cart } from '../../interfaces/cart';
import { Address } from '../../interfaces/index';
import {
  IPostShippingOptionsRequest,
  IPostShippingOptionsResponse,
  IProfileAddressResponse,
  IShippingOptionsResponse,
  ShippingOption,
} from '../../interfaces/shipping/shipping.interface';
import {
  CART_BASE_URL,
  NOTIFICATIONS_URL,
  PROFILE_ADDRESS_URL,
  PROFILE_BASE_URL,
  QUOTES_URL,
  SHIPPING_BASE_URL,
  SHIPPING_OPTIONS_URL,
} from '../../utils/app.constants';
import { getGatewayUrl } from '../../utils/auth.util';
import { formatCurrency } from '../../utils/format.util';
import OrderNotificationPreferences from '../orderNotificationPreferences/OrderNotificationPreferences';
import OrderSummary from '../orderSummary/OrderSummary';
import ShippingAddress from '../shippingAddress/ShippingAddress';
import ShippingOptions from '../shippingOptions/ShippingOptions';
import './Shipping.scss';

const Shipping: FunctionComponent = () => {
  const [cartData, setCartData] = useState<Cart | undefined>(undefined);
  const [shippingOptions, setShippingOptions] = useState<ShippingOption[]>();
  const [selectedOption, setSelectedOption] = useState<ShippingOption | undefined>();
  const [isShippingWaived, setIsShippingWaived] = useState(false);
  const [isShippingAdditionalInfo, setIsShippingAdditionalInfo] = useState(false);
  const [shippingAdditionalInfo, setShippingAdditionalInfo] = useState('');
  const [notificationsPreferences, setNotificationPreferences] = useState(undefined);

  const [isEmailEntered, setIsEmailEntered] = useState(false);
  const [isShippingMethodSelected, setIsShippingMethodSelected] = useState(false);
  const [isNotificationsChecked, setIsNotificationsChecked] = useState(false);
  const [isReceiptsChecked, setIsReceiptsChecked] = useState(false);
  const [profileShippingAddress, setProfileShippingAddress] = useState<Address | undefined>();
  const navigate = useNavigate();

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const msisdns = GlobalNavUtil.getSelectedLines().map((lines: any) => lines.lineId);
  const CART_ID = storageUtil.getCartId();

  const sendSaveAndCompleteEvent = async () => {
    try {
      await postNotificationPreferences();
      await postShippingOptions();
      const shippingAmount = selectedOption?.cost.amount ? selectedOption.cost.amount : 0;
      const amountDue = formatCurrency((cartData?.total ? cartData?.total : 0) + shippingAmount);
      navigate('/checkout/payment', { state: { price: amountDue } });
    } catch (e) {
      console.error('Failed to post selected shipping options');
    }
  };

  const navigateToShopNow = () => {
    navigate('/');
  };

  const postShippingOptions = async () => {
    const model: IPostShippingOptionsRequest = {
      shipmentDetailsId: selectedOption?.shipmentDetailsId !== undefined ? selectedOption.shipmentDetailsId : '',
      optionsId: selectedOption?.optionsId !== undefined ? selectedOption.optionsId : '',
      extCartId: CART_ID ? CART_ID : '',
      storeId: '',
      notes: shippingAdditionalInfo,
    };

    try {
      const { data } = await http.post<IPostShippingOptionsResponse>(
        `${getGatewayUrl()}${SHIPPING_BASE_URL}${SHIPPING_OPTIONS_URL}${CART_ID}`,
        model,
      );
      // eslint-disable-next-line no-console
      console.log(data);
    } catch (e) {
      console.error(e);
      throw e;
    }
  };

  const postNotificationPreferences = async () => {
    try {
      await http.post(`${getGatewayUrl()}${CART_BASE_URL}/${CART_ID}${NOTIFICATIONS_URL}`, notificationsPreferences);
    } catch (e) {
      console.error(e);
      throw e;
    }
  };

  useEffect(() => {
    http
      .get<Cart>(`${getGatewayUrl()}${CART_BASE_URL}/${CART_ID}${QUOTES_URL}`, {})
      .then((res: AxiosResponse) => {
        setCartData(res.data);
      })
      .catch((e) => console.error(e.message));
  }, []);

  useEffect(() => {
    http
      .get<IShippingOptionsResponse>(`${getGatewayUrl()}${SHIPPING_BASE_URL}${SHIPPING_OPTIONS_URL}${CART_ID}`)
      .then((res) => {
        setShippingOptions(res.data?.shippingOptions);
      })
      .catch((e) => console.error(e.message));
  }, []);

  useEffect(() => {
    const profileId = storageUtil.getProfileId();
    http
      .get<IProfileAddressResponse>(`${getGatewayUrl()}${PROFILE_BASE_URL}${PROFILE_ADDRESS_URL}${profileId}`)
      .then((res) => {
        const shippingAddress = res.data?.addresses.find((address) => {
          if (!address.addressTypes?.some((adt) => adt.type === 'SHIPPING')) {
            return undefined;
          }
          return address;
        });

        // default billing address if shipping address not found
        const address = shippingAddress === undefined ? res.data?.addresses[0] : shippingAddress;

        setProfileShippingAddress(address);
        setIsShippingAdditionalInfo(address?.state == 'PR');
      })
      .catch((e) => console.error(e.message));
  }, []);

  const disableContinueBtn = (): boolean => {
    return !isEmailEntered || !isNotificationsChecked || !isReceiptsChecked || !isShippingMethodSelected;
  };

  return (
    <div className='shipping-page p-3'>
      <div className='p-grid'>
        <div className='p-col'>
          {profileShippingAddress && <ShippingAddress shippingAddress={profileShippingAddress} />}
          <ShippingOptions
            shippingOptions={shippingOptions}
            selectedOption={selectedOption}
            setSelectedOption={setSelectedOption}
            isShippingWaived={isShippingWaived}
            isShippingAdditionalInfo={isShippingAdditionalInfo}
            setShippingAdditionalInfo={setShippingAdditionalInfo}
            shippingAdditionalInfo={shippingAdditionalInfo}
            setIsShippingWaived={setIsShippingWaived}
            setIsShippingMethodSelected={setIsShippingMethodSelected}
          />
        </div>
      </div>
      <OrderSummary
        cartData={cartData}
        shippingCost={selectedOption?.cost.amount !== undefined ? selectedOption.cost.amount : 0}
      />
      <OrderNotificationPreferences
        setIsEmailEntered={setIsEmailEntered}
        setIsNotificationsChecked={setIsNotificationsChecked}
        setIsReceiptsChecked={setIsReceiptsChecked}
        setNotificationPreferences={setNotificationPreferences}
        msisdns={msisdns}
      />
      <div className='validate-button-group my-4' style={{ textAlign: 'center' }}>
        <button className='btn cancel-btn p-col mx-4 btn btn-secondary' onClick={() => navigateToShopNow()}>
          Cancel
        </button>
        <button
          disabled={disableContinueBtn()}
          className={`btn btn-primary p-col mx-4 ${disableContinueBtn() && 'continue-btn-disabled'}`}
          onClick={() => sendSaveAndCompleteEvent()}
        >
          Continue
        </button>
      </div>
    </div>
  );
};

export default Shipping;
