import React, { FunctionComponent } from 'react';
import { IPaymentDetails } from '../../interfaces/payment';
import { CARD_TYPE_MAPPING } from '../../utils/app.constants';

interface IPaymentDetailsProps {
  paymentDetails: IPaymentDetails;
}

const PaymentDetails: FunctionComponent<IPaymentDetailsProps> = ({ paymentDetails }) => {
  const truncatedCardNumber =
    '****' + paymentDetails.paymentMethodAlias.substring(paymentDetails.paymentMethodAlias.length - 4);

  const cardType = CARD_TYPE_MAPPING[paymentDetails.cardType];

  return (
    <>
      <div data-testid='payment-method'>
        <h4>Payment method</h4>
        <div className='mb-3 mt-2'>
          <i className={`t-icon t-icon-${cardType} mr-2`} data-testid='card-type'></i>
          <span>ending in {truncatedCardNumber}</span>
        </div>
      </div>
      <div data-testid='payment-address' className='mb-4'>
        <p className='font-bold mb-2'>Payment address</p>
        <p>{paymentDetails.address1}</p>
        <p>{paymentDetails.address2}</p>
        <p>
          {paymentDetails.city}, {paymentDetails.state} {paymentDetails.zipCode}
        </p>
      </div>
    </>
  );
};

export default PaymentDetails;
