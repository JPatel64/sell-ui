import { render, screen } from '@testing-library/react';
import React from 'react';
import PaymentDetails from './PaymentDetails';

describe('Payment details', () => {
  it('should display payment method and address', async () => {
    const mockPaymentDetails = {
      paymentMethodAlias: '5406421311115454',
      cardType: 'MASTERCARD',
      fullName: 'random name',
      address1: '40631 Peachtree Rd SE',
      address2: '',
      city: 'Marysville',
      state: 'WA',
      zipCode: '98259',
      cardExpiryMonthYear: '0223',
      paymentId: '14491105',
    };
    render(<PaymentDetails paymentDetails={mockPaymentDetails} />);
    expect(screen.getByTestId('payment-method')).toHaveTextContent('Payment method');
    expect(screen.getByTestId('card-type')).toHaveClass('t-icon-card-master'); // card type mapping
    expect(screen.getByTestId('payment-method')).toHaveTextContent('ending in ****5454');
    expect(screen.getByTestId('payment-address')).toHaveTextContent(`Payment address`);
    expect(screen.getByTestId('payment-address')).toHaveTextContent(`${mockPaymentDetails.address1}`);
    expect(screen.getByTestId('payment-address')).toHaveTextContent(
      `${mockPaymentDetails.city}, ${mockPaymentDetails.state} ${mockPaymentDetails.zipCode}`,
    );
  });
});
