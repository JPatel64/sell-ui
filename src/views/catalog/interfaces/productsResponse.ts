export interface IProductResponse {
  products: Product[];
}

export interface Product {
  familyCode: string;
  familyName: string;
  description?: string | null;
  shortDescription?: string | null;
  longDescription?: string | null;
  manufacturer: string;
  type: string;
  subtype: string;
  productClass?: string;
  productSubClass?: string | null;
  productCategory?: string;
  productSubCategory?: string | null;
  sskRequired?: string | null;
  ratingsCount?: string | null;
  averageStarRating?: string | null;
  reviewStarImagePath?: string | null;
  billingType?: string | null;
  categories?: string[] | null;
  colorOptions: ColorOption[];
  memoryOptions: MemoryOption[];
  formattedMemoryOptions?: FormattedMemoryOption[] | null;
  operatingSystem?: string;
  features?: Feature[] | null;
  badgesAndPromotions?: BadgesAndPromotion[] | null;
  specifications?: Specification[] | null;
  shapes?: string[];
  skus: Sku[];
  networkSpeeds?: string[];
}

export interface Sku {
  id: string;
  skuCode: string;
  upcCode: string;
  ratingSku: string;
  availability: Availability;
  isDefaultSku: boolean;
  skuPrices: SkuPricingOption[];
  images: Image[];
  specifications?: Specification[];
  boxContents?: BoxContent[];
  condition: string;
  simType?: string[];
  color?: string | null;
  memory?: string | null;
  memoryUom?: string | null;
}

export interface Feature {
  type: string;
  name: string;
  description: string;
}

export interface BadgesAndPromotion {
  label: string;
  value: string;
}

export interface Specification {
  label: string;
  value: string[];
}

export interface BoxContent {
  name: string;
}

export interface Image {
  imageType?: string;
  imageCode?: string;
  url?: string;
}

export interface Availability {
  availabilityStatus: string;
  estimatedShippingFromDateTime?: string | null;
  estimatedShippingToDateTime?: string | null;
  preorderStartDateTime?: string | null;
  preorderEndDateTime?: string | null;
  backOrderAction?: string;
}

export interface SkuPricingOption {
  pricingOptionName: string;
  skuPrice: Price;
}

export interface Price {
  priceSummary: PriceSummary;
}

export interface PriceSummary {
  payNowPrice: OneTimeListSalePrice;
  monthlyPrice?: ListSalePrice;
}

export interface ListSalePrice {
  listPrice: SimpleMonetaryValue;
  salePrice: SimpleMonetaryValue;
  contractTerm: number;
  contractFrequency: string;
}

export interface OneTimeListSalePrice {
  listPrice: SimpleMonetaryValue;
  salePrice: SimpleMonetaryValue;
}

export interface SimpleMonetaryValue {
  amount: number;
  display: string;
}

export interface ColorOption {
  name: string;
  imagePath: string;
}
export interface MemoryOption {
  name: string;
}
export interface FormattedMemoryOption {
  capacity: string;
  unit: string;
}
