export interface IAddAccessoryRequest {
  sku: string;
  offerId?: string;
  quantity: number;
  pricingOption: string;
}
