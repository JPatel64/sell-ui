import { Availability, SkuPricingOption } from './productsResponse';

export interface IAccessoriesResponse {
  accessories: IAccessory[];
}

export interface IAccessory {
  familyName: string;
  manufacturer: string;
  subtype: string;
  skuCode: string;
  thumbnail: string;
  color: string;
  availability: Availability;
  skuPrices: SkuPricingOption[];
  boxContents: BoxContent[] | null;
  specifications: Specification[] | null;
}

export interface Specification {
  label: string;
  value: string[];
}

export interface BoxContent {
  name: string;
}
