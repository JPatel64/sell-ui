export interface IAddDeviceRequest {
  sku: string;
  pricingOption: string;
}
