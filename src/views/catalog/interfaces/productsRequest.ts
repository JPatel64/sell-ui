export interface IProductRequest {
  transactionType: string;
  productTypes: string[];
  productSubtypes: string[];
  pageSize: number;
}
