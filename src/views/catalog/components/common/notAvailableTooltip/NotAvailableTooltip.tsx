import React, { useState } from 'react';

interface Iprops {
  children: JSX.Element;
  enable: boolean;
}

const NotAvailableTooltip = ({ children, enable = false }: Iprops) => {
  const [show, setShow] = useState(false);

  return (
    <>
      {enable ? (
        <div data-testid='not-available' className='tooltip-container'>
          <div className={`arrow-box ${show && 'visible'}`}>Not available in this combination</div>
          <div onMouseEnter={() => setShow(true)} onMouseLeave={() => setShow(false)}>
            {children}
          </div>
        </div>
      ) : (
        children
      )}
    </>
  );
};

export default NotAvailableTooltip;
