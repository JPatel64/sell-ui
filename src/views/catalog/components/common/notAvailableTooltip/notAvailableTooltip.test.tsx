import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import React from 'react';
import NotAvailableTooltip from './NotAvailableTooltip';

describe('NotAvailableTooltip JSX Component', () => {
  it('should display children and render NotAvailableTooltip on hover when enabled', () => {
    render(
      <NotAvailableTooltip enable={true}>
        <div>some content</div>
      </NotAvailableTooltip>,
    );
    userEvent.hover(screen.getByTestId('not-available'));
    expect(screen.getByTestId('not-available')).toHaveTextContent('Not available in this combination');
    expect(screen.getByTestId('not-available')).toHaveTextContent('some content');
  });
});
