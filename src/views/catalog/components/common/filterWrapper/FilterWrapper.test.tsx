import { render, screen } from '@testing-library/react';
import React from 'react';
import { FilterWrapper } from './FilterWrapper';

describe('FilterWrapper Component', () => {
  it('should render with given header and content', () => {
    render(
      <FilterWrapper
        isOpen={false}
        toggleFilter={() => {
          undefined;
        }}
      >
        <div>some content</div>
      </FilterWrapper>,
    );
    expect(screen.getByText('some content')).toBeTruthy();
  });
});
