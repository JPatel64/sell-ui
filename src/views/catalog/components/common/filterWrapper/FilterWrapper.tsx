import React, { FunctionComponent } from 'react';

export const FilterWrapper: FunctionComponent<{
  children: React.ReactNode;
  isOpen: boolean;
  toggleFilter: () => void;
}> = ({ children, isOpen, toggleFilter }) => {
  return (
    <div id='filter'>
      <div data-testid='filter-open' className={`container ${isOpen ? 'filter-open' : undefined}`}>
        <div className='filter-sort-container transparent-border'>
          <div className='row tertiary-buttons task' style={{ marginRight: '0' }}>
            <div className='button-box small' style={{ paddingTop: '15px' }}>
              <a data-testid='filter-toggle' onClick={() => toggleFilter()} aria-label='Filter'>
                <span id='title' className='center'>
                  <i className='fa fa-filter'></i>
                  {isOpen ? 'Close filter' : 'Filter'}
                </span>
              </a>
            </div>
          </div>
        </div>
        <div id='filter-panel' className='filter-items' aria-expanded={isOpen}>
          {children}
        </div>
      </div>
    </div>
  );
};
