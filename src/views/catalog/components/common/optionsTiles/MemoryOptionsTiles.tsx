import React, { FunctionComponent } from 'react';
import { FormattedMemoryOption } from '../../../interfaces/productsResponse.js';
import NotAvailableTooltip from '../notAvailableTooltip/NotAvailableTooltip';
const { useState } = React;

interface IOptionsTilesProps {
  options: FormattedMemoryOption[] | null | undefined;
  onChange: (e: string) => void;
  availableOptions: string[];
  defaultOption: string;
}

export const MemoryOptionsTiles: FunctionComponent<IOptionsTilesProps> = ({
  options,
  onChange,
  availableOptions,
  defaultOption,
}) => {
  const [option, setOption] = useState('');

  const isAvailable = (selectedOption: string) => {
    return availableOptions.includes(selectedOption);
  };

  const handleChange = (selectedOption: string) => {
    setOption(selectedOption);
    onChange(selectedOption);
  };

  return (
    <React.Fragment>
      {options?.map((memoryOption, index) => {
        const formattedMemoryOption = `${memoryOption.capacity}${memoryOption.unit}`;
        const notAvailable = !isAvailable(formattedMemoryOption);
        return (
          <NotAvailableTooltip key={index} enable={notAvailable}>
            <div>
              <label>
                {notAvailable && (
                  <svg style={{ width: '50px', height: '50px' }} className='memory-line'>
                    <line
                      strokeDasharray='5, 5'
                      x1={0}
                      y1={0}
                      x2='100%'
                      y2='100%'
                      style={{ stroke: '#bebebe', strokeWidth: 2 }}
                    />
                  </svg>
                )}
                <input
                  type='radio'
                  value={formattedMemoryOption}
                  checked={option ? option === formattedMemoryOption : formattedMemoryOption === defaultOption}
                  onChange={(e) => {
                    handleChange(e.target.value);
                  }}
                  disabled={notAvailable}
                />
                <div
                  style={{ width: '50px', height: '50px' }}
                  className={notAvailable ? 'not-available-memory' : 'memory-option'}
                >{`${memoryOption.capacity}\n${memoryOption.unit}`}</div>
              </label>
            </div>
          </NotAvailableTooltip>
        );
      })}
    </React.Fragment>
  );
};

export default MemoryOptionsTiles;
