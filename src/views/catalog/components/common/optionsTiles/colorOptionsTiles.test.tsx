import { render, screen } from '@testing-library/react';
import React from 'react';
import ColorOptionsTiles from './ColorOptionsTiles';

describe('OptionTiles TSX Component', () => {
  const colorOption = 'Pacific Blue';
  const availableColorOptions = ['Silver', 'Pacific Blue', 'Graphite', 'Gold'];
  const colorOptions = [
    {
      name: 'Silver',
      imagePath: '/images/products/Apple-iPhone-12-Pro-Max-Silver/Apple-iPhone-12-Pro-Max-Silver.gif',
    },
    {
      name: 'Gold',
      imagePath: '/images/products/Apple-iPhone-12-Pro-Max-Gold/Apple-iPhone-12-Pro-Max-Gold.gif',
    },
    {
      name: 'Graphite',
      imagePath: '/images/products/Apple-iPhone-12-Pro-Max-Graphite/Apple-iPhone-12-Pro-Max-Graphite.gif',
    },
    {
      name: 'Pacific Blue',
      imagePath: '/images/products/Apple-iPhone-12-Pro-Max-Pacific-Blue/Apple-iPhone-12-Pro-Max-Pacific-Blue.gif',
    },
  ];

  it('should render OptionTiles Component for color', () => {
    render(
      <ColorOptionsTiles
        options={colorOptions}
        onChange={() => null}
        baseUrl={''}
        availableOptions={availableColorOptions}
        defaultOption={colorOption}
      />,
    );

    expect(screen.getByAltText('Silver')).toBeInTheDocument();
    expect(screen.getByAltText('Pacific Blue')).toBeInTheDocument();
    expect(screen.getByAltText('Graphite')).toBeInTheDocument();
    expect(screen.getByAltText('Gold')).toBeInTheDocument();
  });
});
