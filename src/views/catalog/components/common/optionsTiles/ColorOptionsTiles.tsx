import React, { FunctionComponent } from 'react';
import { ColorOption } from '../../../interfaces/productsResponse.js';
import NotAvailableTooltip from '../notAvailableTooltip/NotAvailableTooltip';
const { useState } = React;

interface IOptionsTilesProps {
  options: ColorOption[] | undefined;
  onChange: (e: string) => void;
  availableOptions: string[];
  defaultOption: string;
  baseUrl: string;
}

export const ColorOptionsTiles: FunctionComponent<IOptionsTilesProps> = ({
  options,
  onChange,
  availableOptions,
  defaultOption,
  baseUrl,
}) => {
  const [option, setOption] = useState('');

  const isAvailable = (selectedOption: string) => {
    return availableOptions?.includes(selectedOption);
  };

  const handleChange = (selectedOption: string) => {
    setOption(selectedOption);
    onChange(selectedOption);
  };

  return (
    <React.Fragment>
      {options?.map((colorOption, index) => {
        const notAvailable = !isAvailable(colorOption.name);
        return (
          <React.Fragment key={index}>
            <NotAvailableTooltip enable={notAvailable}>
              <div>
                <label>
                  {notAvailable && (
                    <svg style={{ width: '50px', height: '50px' }} className='color-line'>
                      <line
                        strokeDasharray='5, 5'
                        x1={0}
                        y1={0}
                        x2='100%'
                        y2='100%'
                        style={{ stroke: '#bebebe', strokeWidth: 2 }}
                      />
                    </svg>
                  )}
                  <input
                    type='radio'
                    value={colorOption.name}
                    checked={option ? option === colorOption.name : colorOption.name === defaultOption}
                    onChange={(e) => {
                      handleChange(e.target.value);
                    }}
                    disabled={notAvailable}
                  />
                  <img
                    src={baseUrl + colorOption.imagePath}
                    alt={colorOption.name}
                    className={notAvailable ? 'not-available' : undefined}
                  />
                </label>
              </div>
            </NotAvailableTooltip>
          </React.Fragment>
        );
      })}
    </React.Fragment>
  );
};

export default ColorOptionsTiles;
