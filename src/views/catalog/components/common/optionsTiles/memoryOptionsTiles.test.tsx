import { render, screen } from '@testing-library/react';
import React from 'react';
import MemoryOptionsTiles from './MemoryOptionsTiles';

describe('OptionTiles TSX Component', () => {
  const memoryOption = '64GB';
  const availableMemoryOptions = ['256GB', '64GB'];
  const memoryOptions = [
    {
      capacity: '256',
      unit: 'GB',
    },
    {
      capacity: '64',
      unit: 'GB',
    },
    {
      capacity: '32',
      unit: 'GB',
    },
  ];

  it('should render OptionTiles Component for memory', () => {
    render(
      <MemoryOptionsTiles
        options={memoryOptions}
        onChange={() => null}
        availableOptions={availableMemoryOptions}
        defaultOption={memoryOption}
      />,
    );

    expect(screen.getByDisplayValue('32GB')).toBeInTheDocument();
    expect(screen.getByDisplayValue('64GB')).toBeInTheDocument();
    expect(screen.getByDisplayValue('256GB')).toBeInTheDocument();
  });

  it('should only render available options for memory', () => {
    render(
      <MemoryOptionsTiles
        options={memoryOptions}
        onChange={() => null}
        availableOptions={availableMemoryOptions}
        defaultOption={memoryOption}
      />,
    );

    expect(screen.getByDisplayValue('32GB')).toBeDisabled();
    expect(screen.getByDisplayValue('64GB')).toBeEnabled();
    expect(screen.getByDisplayValue('256GB')).toBeEnabled();
  });
});
