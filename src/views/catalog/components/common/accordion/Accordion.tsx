import React, { FC, useEffect, useState } from 'react';

interface IProps {
  header: string;
  defaultOpen?: boolean;
  children: JSX.Element;
}

export const Accordion: FC<IProps> = (props) => {
  const [isOpen, setIsOpen] = useState(false);

  useEffect(() => {
    if (props.defaultOpen) {
      setIsOpen(true);
    }
  }, []);

  function toggleAccordion() {
    setIsOpen(!isOpen);
  }

  return (
    <div className='accordion-tab'>
      <div data-testid='header' onClick={toggleAccordion} className='p-header'>
        <span className='caret'>
          <i className={`fa icon-24 ${isOpen ? 'fa-angle-up' : 'fa-angle-down'}`}></i>
        </span>
        <h5 className='title' style={{ color: isOpen ? '#e20074' : 'black' }}>
          {props.header}{' '}
        </h5>
      </div>
      <div data-testid='content' className='accordion-content' aria-expanded={isOpen}>
        {props.children}
      </div>
    </div>
  );
};
