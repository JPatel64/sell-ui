import { render, screen, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import React from 'react';
import { Accordion } from './Accordion';

describe('Accordion Component', () => {
  it('should render with given header and content', () => {
    render(
      <Accordion header='some header'>
        <div>some content</div>
      </Accordion>,
    );
    expect(screen.getByText('some header')).toBeTruthy();
    expect(screen.getByText('some content')).toBeTruthy();
  });

  it('should toggle content when clicking accordion header', async () => {
    render(
      <Accordion header='some header'>
        <div>some content</div>
      </Accordion>,
    );

    expect(screen.getByTestId('content')).toHaveAttribute('aria-expanded', 'false');

    userEvent.click(screen.getByTestId('header'));
    await waitFor(() => {
      expect(screen.getByTestId('content')).toHaveAttribute('aria-expanded', 'true');
    });

    userEvent.click(screen.getByTestId('header'));
    await waitFor(() => {
      expect(screen.getByTestId('content')).toHaveAttribute('aria-expanded', 'false');
    });
  });
});
