import React from 'react';
import { doSearch } from './search.util';

const Search = () => {
  return (
    <>
      <div className='search-box search'>
        <fieldset className='form-group'>
          <input
            data-testid='search'
            className='form-control'
            style={{ paddingTop: '0' }}
            id='search'
            type='search'
            autoComplete='off'
            onKeyUp={() => doSearch()}
            placeholder='Search'
          />
        </fieldset>
        <div className='search-icon' style={{ fontSize: '16px' }}>
          <i className='fa fa-search def'></i>
        </div>
      </div>
    </>
  );
};

export default Search;
