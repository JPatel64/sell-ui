export const doSearch = () => {
  const searchElement = document.getElementById('search') as HTMLInputElement;

  if (searchElement) {
    const term = searchElement.value
      .toLowerCase()
      .split(' ')
      .filter((e: string) => String(e).trim());

    const event = new CustomEvent('filter_results', { detail: { filterType: 'search', value: term } });
    document.dispatchEvent(event);
  }
};

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const search = (array: any, terms: string[]) => {
  const regexList = terms.map((t) => new RegExp(t, 'gi'));

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const check = (field: any) => {
    if (field !== null && typeof field === 'object') {
      return Object.values(field).some(check);
    }

    if (Array.isArray(field)) {
      return field.some(check);
    }

    return typeof field === 'string' && regexList.some((rx) => rx.test(field));
  };

  return array.filter(check);
};
