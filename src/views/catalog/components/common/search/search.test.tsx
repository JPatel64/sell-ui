import { render, screen } from '@testing-library/react';
import React from 'react';
import devices from '../../../../../../mocks/responses/devices.json';
import { Product, Sku } from '../../../interfaces/productsResponse';
import Search from './Search';
import { search } from './search.util';

describe('search', () => {
  const array: Product[] = devices.products;

  it('should filter array based on name', () => {
    const results = search(array, ['galaxy']);
    expect(results.length).toBe(32);
    results.forEach((result: Product) => {
      expect(result.familyName).toEqual(expect.stringContaining('Galaxy'));
    });
  });

  it('should filter array based on manufacturer', () => {
    const results = search(array, ['oneplus']);
    expect(results.length).toBe(6);
    results.forEach((result: Product) => {
      expect(result.manufacturer).toEqual(expect.stringContaining('OnePlus'));
    });
  });

  it('should filter array based on memory options', () => {
    const results = search(array, ['256gb']);
    expect(results.length).toBe(14);
    results.forEach((result: Product) => {
      expect(result.memoryOptions).toEqual(
        expect.arrayContaining([
          {
            name: '256GB',
          },
        ]),
      );
    });
  });

  it('should filter array based on SKU', () => {
    const results = search(array, ['610214661180']);
    expect(results.length).toBe(1);
    results.forEach((result: Product) => {
      expect(result.skus.find((sku: Sku) => sku.skuCode === '610214661180')).toBeTruthy();
    });
  });

  it('should filter array regardless of Case', () => {
    const results = search(array, ['GaLaXy']);
    results.forEach((result: Product) => {
      expect(result.familyName).toEqual(expect.stringContaining('Galaxy'));
    });
  });
});

describe('Search TSX Component', () => {
  it('should display search input', () => {
    render(<Search />);
    screen.getByRole('searchbox');
  });
});
