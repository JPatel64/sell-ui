import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import React from 'react';
import OptionsDropdown from './OptionsDropdown';

describe('OptionsDropdown JSX Component', () => {
  const defaultOption = 'Space Gray AL Black Band';
  const availableOptions = [
    'Gold Stainless Stone Band',
    'SS Space Black Black Band',
    'Gold AL Pink Sand Band',
    'Space Gray AL Black Band',
    'Silver AL White Band',
  ];
  const colorOptions = [
    {
      name: 'Stainless White Band',
      imagePath:
        '/images/products/Apple-Watch-Series-5-40mm-Stainless-White-Band/Apple-Watch-Series-5-40mm-Stainless-White-Band.gif',
    },
    {
      name: 'Gold Stainless Stone Band',
      imagePath:
        '/images/products/Apple-Watch-Series-5-40mm-Gold-Stainless-Stone-Band/Apple-Watch-Series-5-40mm-Gold-Stainless-Stone-Band.gif',
    },
    {
      name: 'SS Space Black Black Band',
      imagePath:
        '/images/products/Apple-Watch-Series-5-40mm-Space-Black-Black-Band/Apple-Watch-Series-5-40mm-Space-Black-Black-Band.gif',
    },
    {
      name: 'Space Gray AL Black Band',
      imagePath:
        '/images/products/Apple-Watch-Series-5-40mm-Space-Gray-Black-Band/Apple-Watch-Series-5-40mm-Space-Gray-Black-Band.gif',
    },
    {
      name: 'Silver AL White Band',
      imagePath:
        '/images/products/Apple-Watch-Series-5-40mm-Silver-White-Band/Apple-Watch-Series-5-40mm-Silver-White-Band.gif',
    },
    {
      name: 'Gold AL Pink Sand Band',
      imagePath:
        '/images/products/Apple-Watch-Series-5-40mm-Gold-Pink-Sand-Band/Apple-Watch-Series-5-40mm-Gold-Pink-Sand-Band.gif',
    },
  ];
  it('should render OptionsDropdown Component and display options', () => {
    render(
      <OptionsDropdown
        options={colorOptions}
        type='color'
        availableOptions={availableOptions}
        defaultOption={defaultOption}
        onChange={() => null}
      />,
    );

    userEvent.click(screen.getByRole('combobox'));
    expect(screen.getByRole('combobox')).toHaveTextContent('Stainless White Band');
    expect(screen.getByRole('combobox')).toHaveTextContent('Gold Stainless Stone Band');
    expect(screen.getByRole('combobox')).toHaveTextContent('SS Space Black Black Band');
    expect(screen.getByRole('combobox')).toHaveTextContent('Gold AL Pink Sand Band');
    expect(screen.getByRole('combobox')).toHaveTextContent('Space Gray AL Black Band');
    expect(screen.getByRole('combobox')).toHaveTextContent('Silver AL White Band');
  });

  it('should only render available options', () => {
    render(
      <OptionsDropdown
        options={colorOptions}
        type='color'
        availableOptions={availableOptions}
        defaultOption={defaultOption}
        onChange={() => null}
      />,
    );

    userEvent.click(screen.getByRole('combobox'));

    expect(screen.getByTestId('Stainless White Band')).toBeDisabled();

    expect(screen.getByTestId('Gold Stainless Stone Band')).toBeEnabled();
    expect(screen.getByTestId('SS Space Black Black Band')).toBeEnabled();
    expect(screen.getByTestId('Gold AL Pink Sand Band')).toBeEnabled();
    expect(screen.getByTestId('Space Gray AL Black Band')).toBeEnabled();
    expect(screen.getByTestId('Silver AL White Band')).toBeEnabled();
  });
});
