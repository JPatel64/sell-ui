import React, { FunctionComponent } from 'react';

interface IOptionsDropdownProps {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  options: any | undefined;
  type: string;
  onChange: (e: string) => void;
  availableOptions: string[];
  defaultOption: string;
}

export const OptionsDropdown: FunctionComponent<IOptionsDropdownProps> = ({
  options,
  type,
  onChange,
  availableOptions,
  defaultOption,
}) => {
  return (
    <React.Fragment>
      <select
        name={`${type} select`}
        aria-label={`${type} select`}
        className='form-control'
        onChange={(e) => {
          onChange(e.target.value);
        }}
      >
        {options.map((option: { name: string; capacity: string; unit: string }, index: React.Key) => {
          const label = type === 'color' ? option.name : `${option.capacity}${option.unit}`;
          const notAvailable = !availableOptions.includes(label);
          return (
            <option
              data-testid={label}
              key={index}
              className='dropdown-item'
              value={label}
              aria-label={label}
              defaultValue={defaultOption}
              disabled={notAvailable}
            >
              {label}
            </option>
          );
        })}
      </select>
    </React.Fragment>
  );
};

export default OptionsDropdown;
