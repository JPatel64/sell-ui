import React, { FunctionComponent, useEffect } from 'react';
import { SkuPricingOption } from '../../../interfaces/productsResponse.js';
import { convertPricingOptionName } from '../../../util/convert-pricing-option';
import { formatPrice } from './payment-options.util';

interface IPaymentOptionsProps {
  disabled: boolean;
  skuPricingOptions: SkuPricingOption[] | undefined;
  skuCode?: string;
  selectedPaymentOption?: string;
  onSelectedPaymentOption: (e: string) => void;
}

const PaymentOptions: FunctionComponent<IPaymentOptionsProps> = ({
  disabled,
  skuPricingOptions,
  skuCode,
  selectedPaymentOption,
  onSelectedPaymentOption,
}) => {
  const formatter = new Intl.NumberFormat('en-US', { style: 'currency', currency: 'USD' });

  // reset option selection when skuCode changes
  useEffect(() => {
    onSelectedPaymentOption('');
  }, [skuCode]);

  return (
    <div className='mx-3 my-4 container'>
      {skuPricingOptions?.map((priceOption, index) => {
        priceOption.pricingOptionName = convertPricingOptionName(priceOption.pricingOptionName);
        const { label, price, downPayment } = formatPrice(priceOption);

        return (
          <div data-testid='skuPrices' key={index} className={`row ${disabled ? 'disabled' : ''}`}>
            <>
              <label className='form-label col-md-8 px-0'>
                <span className='radio'>
                  <input
                    data-testid={priceOption.pricingOptionName + skuCode}
                    disabled={disabled}
                    type='radio'
                    name='payment-option'
                    aria-label={priceOption.pricingOptionName}
                    value={priceOption.pricingOptionName + '--' + skuCode}
                    checked={selectedPaymentOption === priceOption.pricingOptionName + '--' + skuCode}
                    onChange={(e) => onSelectedPaymentOption(e.target.value)}
                  />
                  <span className='radio-mask'></span>
                </span>
                <p className='content'>
                  {label} <br />
                  {downPayment && 'Down payment'}
                </p>
              </label>
              <p className='content col-md-4 px-0 text-right align-self-end'>
                {formatter.format(price ? +price : 0)} <br />
                {downPayment && formatter.format(downPayment ? +downPayment : 0)}
              </p>
            </>
          </div>
        );
      })}
    </div>
  );
};

export default PaymentOptions;
