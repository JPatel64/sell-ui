import { render, screen } from '@testing-library/react';
import React from 'react';
import { formatPrice } from './payment-options.util';
import PaymentOptions from './PaymentOptions';

const skuPrices = [
  {
    pricingOptionName: 'FULL',
    skuPrice: {
      priceSummary: {
        payNowPrice: {
          listPrice: {
            amount: 1099.99,
            display: '1099.99',
          },
          salePrice: {
            amount: 1099.99,
            display: '1099.99',
          },
        },
      },
    },
  },
  {
    pricingOptionName: 'EIP',
    skuPrice: {
      priceSummary: {
        monthlyPrice: {
          listPrice: {
            amount: 15.0,
            display: '15.00',
          },
          salePrice: {
            amount: 15.0,
            display: '15.00',
          },
          contractTerm: 24,
          contractFrequency: 'MONTHLY',
        },
        payNowPrice: {
          listPrice: {
            amount: 0.0,
            display: '0.00',
          },
          salePrice: {
            amount: 0.0,
            display: '0.00',
          },
        },
      },
    },
  },
  {
    pricingOptionName: 'JOD',
    skuPrice: {
      priceSummary: {
        monthlyPrice: {
          listPrice: {
            amount: 30.0,
            display: '30.00',
          },
          salePrice: {
            amount: 30.0,
            display: '30.00',
          },
          contractTerm: 24,
          contractFrequency: 'MONTHLY',
        },
        payNowPrice: {
          listPrice: {
            amount: 0.0,
            display: '0.00',
          },
          salePrice: {
            amount: 0.0,
            display: '0.00',
          },
        },
      },
    },
  },
];

describe('Payment options', () => {
  it('should format price', () => {
    const results = skuPrices.map((s) => formatPrice(s));

    expect(results[0].label).toEqual('Full retail price');
    expect(results[0].price).toEqual('1099.99');
    expect(results[0]).not.toHaveProperty('downPayment');

    expect(results[1].label).toEqual('EIP/mo, 24 mos');
    expect(results[1].price).toEqual('15.00');
    expect(results[1].downPayment).toEqual('0.00');

    expect(results[2].label).toEqual('JOD/mo, 24 mos');
    expect(results[2].price).toEqual('30.00');
    expect(results[2].downPayment).toEqual('0.00');
  });
});

describe('PaymentOptions TSX Component', () => {
  it('should render Payment Options Component', () => {
    render(
      <PaymentOptions
        disabled={false}
        skuPricingOptions={skuPrices}
        onSelectedPaymentOption={() => null}
        skuCode='610214666598'
      />,
    );

    expect(screen.getByTestId('FULL610214666598')).toBeInTheDocument();
    expect(screen.getByTestId('EIP610214666598')).toBeInTheDocument();
    expect(screen.getByTestId('JOD610214666598')).toBeInTheDocument();

    expect(screen.getByTestId('FULL610214666598')).toBeEnabled();
    expect(screen.getByTestId('EIP610214666598')).toBeEnabled();
    expect(screen.getByTestId('JOD610214666598')).toBeEnabled();
  });

  it('should disable Payment Options', () => {
    render(
      <PaymentOptions
        disabled={true}
        skuPricingOptions={skuPrices}
        onSelectedPaymentOption={() => null}
        skuCode='610214666598'
      />,
    );

    expect(screen.getByTestId('FULL610214666598')).toBeDisabled();
    expect(screen.getByTestId('EIP610214666598')).toBeDisabled();
    expect(screen.getByTestId('JOD610214666598')).toBeDisabled();
  });
});
