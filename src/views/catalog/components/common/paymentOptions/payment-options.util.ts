import { SkuPricingOption } from '../../../interfaces/productsResponse';

export const formatPrice = (priceOption: SkuPricingOption) => {
  const pricingOptionName = priceOption?.pricingOptionName;
  const priceType = pricingOptionName === 'FULL' ? 'payNowPrice' : 'monthlyPrice';
  const priceSummary = priceOption.skuPrice?.priceSummary;

  const price =
    priceType === 'payNowPrice'
      ? priceSummary.payNowPrice?.salePrice?.display
      : priceSummary.monthlyPrice?.salePrice?.display;
  const contractTerm = priceType === 'payNowPrice' ? 0 : priceSummary.monthlyPrice?.contractTerm;

  if (pricingOptionName === 'FULL') {
    return { label: 'Full retail price', price };
  } else {
    return {
      label: `${pricingOptionName}/mo, ${contractTerm} mos`,
      price,
      downPayment: priceSummary.payNowPrice?.salePrice?.display,
    };
  }
};
