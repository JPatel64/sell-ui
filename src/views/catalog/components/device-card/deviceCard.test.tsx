import { render, screen, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import React from 'react';
import devices from '../../../../../mocks/responses/devices.json';
import { Product, Sku } from '../../interfaces/productsResponse';
import { createColorMemoryMapping, formatAvailability } from './device.util';
import DeviceCard from './DeviceCard';

describe('Device', () => {
  const skus: Sku[] = devices.products[0].skus;
  it('should create color mapping', () => {
    const expected = {
      colors: {
        'Flamingo Pink': ['128GB'],
        'Prism Black': ['128GB'],
        'Prism Blue': ['256GB', '128GB'],
        'Prism White': ['128GB'],
      },
      memories: {
        '128GB': ['Prism Blue', 'Prism White', 'Flamingo Pink', 'Prism Black'],
        '256GB': ['Prism Blue'],
      },
    };

    const result = createColorMemoryMapping(skus);
    expect(result).toEqual(expected);
  });

  it('should format availability', () => {
    expect(formatAvailability('AVAILABLE')).toEqual('In stock');
    expect(formatAvailability('AVAILABLE_FOR_BACK_ORDER')).toEqual('Backorder');
    expect(formatAvailability('NOT_AVAILABLE')).toEqual('Out of stock');
    expect(formatAvailability('')).toEqual('Out of stock');
  });
});

describe('Device Component', () => {
  const deviceProps: Product = devices.products[0];
  it('should render device Component', () => {
    render(<DeviceCard device={deviceProps} onSelectedPaymentOption={() => null} />);

    expect(screen.getByTestId('mainHeading')).toHaveTextContent('Galaxy S10e');
    expect(screen.getByTestId('availability')).toHaveTextContent('In stock');
    expect(screen.getByTestId('shipDate')).toHaveTextContent('Estimated ship date');
    expect(screen.getByRole('link')).toHaveTextContent('View details');
    expect(screen.getByRole('button')).toHaveTextContent('Add to Cart');
    expect(screen.getByRole('button')).toBeDisabled();
  });

  it('should update selected color & memory option text if available', async () => {
    render(<DeviceCard device={deviceProps} onSelectedPaymentOption={() => null} />);

    //Default Selection
    expect(screen.getByTestId('colorMemoryText')).toHaveTextContent('Prism Black 128GB');

    //Updated Selection
    userEvent.click(screen.getByAltText('Prism Blue'));
    await waitFor(() => {
      expect(screen.getByDisplayValue('256GB')).toBeEnabled();
    });
    userEvent.click(screen.getByDisplayValue('256GB'));

    await waitFor(() => {
      expect(screen.getByTestId('colorMemoryText')).toHaveTextContent('Prism Blue 256GB');
    });
  });

  it('should update availability based on color & memory option selected', async () => {
    render(<DeviceCard device={deviceProps} onSelectedPaymentOption={() => null} />);

    //Default Selection
    expect(screen.getByTestId('availability')).toHaveTextContent('In stock');

    //Updated Selection
    userEvent.click(screen.getByAltText('Prism Blue'));
    await waitFor(() => {
      expect(screen.getByDisplayValue('256GB')).toBeEnabled();
    });
    userEvent.click(screen.getByDisplayValue('256GB'));

    await waitFor(() => {
      expect(screen.getByTestId('availability')).toHaveTextContent('In stock');
    });
  });

  it('should update shipping dates based on color & memory option selected', async () => {
    render(<DeviceCard device={deviceProps} onSelectedPaymentOption={() => null} />);

    //Default Selection
    expect(screen.getByTestId('shipDate')).toHaveTextContent('Estimated ship date 12/04/2021 - 12/06/2021');

    //Updated Selection
    userEvent.click(screen.getByAltText('Prism Blue'));
    await waitFor(() => {
      expect(screen.getByDisplayValue('256GB')).toBeEnabled();
    });
    userEvent.click(screen.getByDisplayValue('256GB'));

    await waitFor(() => {
      expect(screen.getByTestId('shipDate')).toHaveTextContent('Estimated ship date 12/04/2021 - 12/06/2021');
    });
  });
});
