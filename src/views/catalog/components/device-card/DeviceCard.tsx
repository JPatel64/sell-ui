import { http, storageUtil } from '@tmobile/atlas-util';
import { Dialog } from 'primereact/dialog';
import React, { FunctionComponent, useEffect, useState } from 'react';
import TmoIcon from '../../../../assets/tmoIcon.png';
import { UPDATE_IS_DEVICE_ADDED, useAddDeviceContext } from '../../context/AddDeviceContext';
import { IAddDeviceRequest } from '../../interfaces/addDeviceRequest';
import { Product, Sku, Specification } from '../../interfaces/productsResponse';
import {
  CART_URL,
  DEVICES_URL,
  KEY_FEATURES,
  OTHER_FEATURES,
  PRODUCT_DETAILS_HEADER,
  SORT_DIRECTION_DESCENDING,
} from '../../util/app.constants';
import { getGatewayUrl } from '../../util/auth.util';
import { convertPricingOptionName } from '../../util/convert-pricing-option';
import { findThumbNail, sortAndSplitMemoryOptions } from '../../util/format.util';
import OptionsDropdown from '../common/optionsDropdown/OptionsDropdown';
import { ColorOptionsTiles } from '../common/optionsTiles/ColorOptionsTiles';
import MemoryOptionsTiles from '../common/optionsTiles/MemoryOptionsTiles';
import PaymentOptions from '../common/paymentOptions/PaymentOptions';
import { CDN_BASE_URL, createColorMemoryMapping, formatAvailability } from './device.util';
import './DeviceCard.scss';

interface IDeviceProps {
  device: Product;
  selectedPaymentOption?: string;
  onSelectedPaymentOption: (e: string) => void;
}

const DeviceCard: FunctionComponent<IDeviceProps> = ({ device, selectedPaymentOption, onSelectedPaymentOption }) => {
  const [selectedSku, setSelectedSku] = useState<Sku | undefined>(device.skus.find((sku) => sku.isDefaultSku));
  const [keyFeatures, setKeyFeatures] = useState<string[]>([]);
  const [otherFeatures, setotherFeatures] = useState<Specification[]>([]);

  const [isEligibleJOD, setIsEligibleJOD] = useState(false);

  const [showDetailsModal, setShowDetailsModal] = useState<boolean>(false);

  // const [isFreeShipping, setIsFreeShipping] = React.useState(false); // replace this after we get sku.availabileCartPromotions[]
  const isFreeShipping = false;

  const formattedAvailability = formatAvailability(selectedSku?.availability?.availabilityStatus);

  const defaultSku = device?.skus.find((sku) => sku.isDefaultSku);
  const defaultColor = defaultSku?.color ? defaultSku.color : '';
  const defaultMemory = defaultSku?.memory ? defaultSku.memory : '';

  const [selectedColor, setSelectedColor] = useState(defaultColor);
  const [selectedMemory, setSelectedMemory] = useState(defaultMemory);

  const updateColor = (color: string) => {
    setSelectedColor(color);
  };

  const updateMemory = (memory: string) => {
    setSelectedMemory(memory);
  };

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const handleDetailsClick = (event: any) => {
    event.preventDefault();
    setShowDetailsModal(true);
  };
  device.formattedMemoryOptions = sortAndSplitMemoryOptions(device.memoryOptions, SORT_DIRECTION_DESCENDING);
  const defaultAvailableColors = device.colorOptions.map((co) => co.name);
  const defaultAvailableMemory = device.formattedMemoryOptions.map((mo) => `${mo.capacity}${mo.unit}`);
  const [availableColorOptions, setAvailableColorOptions] = useState(defaultAvailableColors);
  const [availableMemoryOptions, setAvailableMemoryOptions] = useState(defaultAvailableMemory);

  const { addDeviceState, addDeviceDispatch } = useAddDeviceContext();

  const findSku = () => {
    const foundSku = device.skus.find((sku) => sku.color === selectedColor && sku.memory === selectedMemory);
    if (foundSku) {
      setSelectedSku(foundSku);
      setIsEligibleJOD(
        foundSku.skuPrices?.some((skuPrice) => convertPricingOptionName(skuPrice.pricingOptionName) === 'JOD'),
      );
      // setisFreeShipping(sku.availabileCartPromotions.filter((p) => p.promoSubtypes.includes('SHIPPING'))); TODO: get clarification on sku.availabileCartPromotions[]
    }
  };

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const addToCart = async (sku: any, paymentOption: any) => {
    const request: IAddDeviceRequest = {
      sku: sku?.skuCode,
      pricingOption: paymentOption.split('--')[0],
    };
    const cartId = storageUtil.getCartId();
    const lineId = storageUtil.getLineId();
    storageUtil.setSku(sku?.skuCode);

    const btn = document.getElementById(`btn-${sku.skuCode}`) as HTMLButtonElement;
    btn.disabled = true;
    btn.classList.add('spin');
    try {
      await http.post(`${getGatewayUrl()}${CART_URL}${DEVICES_URL}?cartId=${cartId}&lineId=${lineId}`, request);
      addDeviceDispatch({ type: UPDATE_IS_DEVICE_ADDED, payload: true });
      btn.classList.remove('spin');
    } catch (e) {
      console.error(e);
      btn.classList.remove('spin');
      throw e;
    }
  };
  const colorMemoryOptionsMap = createColorMemoryMapping(device.skus);

  useEffect(() => {
    const options = colorMemoryOptionsMap.colors[selectedColor] ?? [];
    setAvailableMemoryOptions(options);
    findSku();
  }, [selectedColor]);

  useEffect(() => {
    const options = colorMemoryOptionsMap.memories[selectedMemory] ?? [];
    setAvailableColorOptions(options);
    findSku();
  }, [selectedMemory]);

  useEffect(() => {
    if (device.features) {
      const features = [];
      for (const val of device.features) {
        features.push(val.name);
      }
      setKeyFeatures(features);
    }
  }, [device]);

  useEffect(() => {
    if (device.specifications) {
      const features = [...otherFeatures];
      for (const val of device.specifications) {
        features.push(val);
      }
      setotherFeatures(features);
    }
  }, [device]);

  return (
    <div>
      <div className='col-md-* mb-5'>
        <div className='card fuse-card'>
          <div data-testid='catalog-card' className='catalog-card card-body card-content px-4'>
            <h6 data-testid='mainHeading' className='main-heading'>
              {device.familyName}
            </h6>
            <div className='row d-flex align-items-center mb-2'>
              <div className='col-md-4'>
                <img src={`${CDN_BASE_URL}${findThumbNail(selectedSku)?.url}`} alt={device.familyName} width={50} />
              </div>
              <div className='col-md-8 align-self-start px-0'>
                <div>
                  <p data-testid='availability' className='font-weight-bold'>
                    {formattedAvailability}
                  </p>
                  {isFreeShipping && (
                    <p className='font-weight-bold'>
                      Free shipping <span className='rounded-circle deal-icon'>Deal!</span>
                    </p>
                  )}
                  <div data-testid='shipDate' className={isFreeShipping ? 'mt-4' : undefined}>
                    Estimated ship date <br />
                    {selectedSku?.availability?.estimatedShippingFromDateTime
                      ? new Date(selectedSku?.availability?.estimatedShippingFromDateTime).toLocaleString('default', {
                          month: 'numeric',
                          day: '2-digit',
                          year: 'numeric',
                        })
                      : ''}
                    &nbsp;-&nbsp;
                    {selectedSku?.availability?.estimatedShippingToDateTime
                      ? new Date(selectedSku?.availability?.estimatedShippingToDateTime).toLocaleString('default', {
                          month: 'numeric',
                          day: '2-digit',
                          year: 'numeric',
                        })
                      : ''}
                  </div>
                </div>
              </div>
              <a className='details-link ml-3' href='#' onClick={handleDetailsClick}>
                <u>View details</u>
              </a>
              <div style={{ width: '15.438rem', height: '1.688rem' }}>
                {isEligibleJOD && (
                  <p className='font-weight-bold mt-2 ml-3' style={{ width: 'max-content' }}>
                    Eligible for JUMP! On Demand
                  </p>
                )}
              </div>
              <div style={{ height: '12.875rem', width: 'calc(100% - 30px)' }}>
                <PaymentOptions
                  skuCode={selectedSku?.skuCode}
                  disabled={formattedAvailability === 'Out of stock' || addDeviceState?.isDeviceAdded}
                  skuPricingOptions={selectedSku?.skuPrices}
                  selectedPaymentOption={selectedPaymentOption}
                  onSelectedPaymentOption={(option: string) => onSelectedPaymentOption(option)}
                />
              </div>

              <div className='pt-3'>
                <div data-testid='colorMemoryText' className='font-weight-bold pl-3' style={{ maxWidth: '285px' }}>
                  {selectedColor} {selectedMemory}
                </div>

                {device.colorOptions.length > 4 ? (
                  <div className='mt-3 mb-3 ml-3' style={{ paddingTop: '5px', paddingBottom: '5px' }}>
                    <OptionsDropdown
                      options={device.colorOptions}
                      type='color'
                      onChange={updateColor}
                      availableOptions={addDeviceState?.isDeviceAdded ? [] : availableColorOptions}
                      defaultOption={defaultColor}
                    />
                  </div>
                ) : (
                  <div className='d-flex flex-row mr-3 mb-3 ml-3 mt-2' style={{ width: '15rem' }}>
                    <ColorOptionsTiles
                      options={device.colorOptions}
                      baseUrl={CDN_BASE_URL}
                      onChange={updateColor}
                      availableOptions={addDeviceState?.isDeviceAdded ? [] : availableColorOptions}
                      defaultOption={defaultColor}
                    />
                  </div>
                )}

                {device.formattedMemoryOptions.length > 4 ? (
                  <div className='mt-2 mb-3'>
                    <OptionsDropdown
                      options={device.formattedMemoryOptions}
                      type='memory'
                      onChange={updateMemory}
                      availableOptions={addDeviceState?.isDeviceAdded ? [] : availableMemoryOptions}
                      defaultOption={defaultMemory}
                    />
                  </div>
                ) : (
                  <div className='d-flex flex-row m-3' style={{ width: '15rem' }}>
                    <MemoryOptionsTiles
                      options={device.formattedMemoryOptions}
                      onChange={updateMemory}
                      availableOptions={addDeviceState?.isDeviceAdded ? [] : availableMemoryOptions}
                      defaultOption={defaultMemory}
                    />
                  </div>
                )}
              </div>
              <div className='row mx-auto justify-content-center align-items-center' style={{ fontSize: '.8rem' }}>
                <button
                  data-testid={'addCart' + selectedSku?.skuCode}
                  className='btn btn-primary'
                  disabled={
                    !selectedSku ||
                    selectedPaymentOption?.split('--')[1] !== selectedSku?.skuCode ||
                    addDeviceState?.isDeviceAdded
                  }
                  type='button'
                  onClick={() => addToCart(selectedSku, selectedPaymentOption)}
                  id={`btn-${selectedSku?.skuCode}`}
                >
                  Add to Cart
                  <span className='spinner' />
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Dialog
        showHeader={false}
        visible={showDetailsModal}
        onHide={() => setShowDetailsModal(false)}
        style={{ width: '40%' }}
      >
        <div className='language-Update-Container pt-5 px-4'>
          <img className='tmo-icon' src={TmoIcon} alt='t-mobile icon' />
          <i role='button' tabIndex={0} className='pi pi-times' onClick={() => setShowDetailsModal(false)}></i>
          <h3 className='pt-3'>{PRODUCT_DETAILS_HEADER}</h3>
          <div>
            <h5 className='pt-3'>{KEY_FEATURES}</h5>

            {
              <div>
                {keyFeatures.map((value, index) => {
                  return (
                    <p key={index} className='pt-2'>
                      {value}
                    </p>
                  );
                })}
              </div>
            }
          </div>

          <div>
            <h5 className='pt-3'>{OTHER_FEATURES}</h5>
            {
              <div>
                {otherFeatures.map((spec, index) => {
                  return (
                    <p key={index} className='pt-2'>
                      {spec.label}:{spec.value}
                    </p>
                  );
                })}
              </div>
            }
          </div>
          <div className='col-12 buttons-container button-spacing'>
            <div className='button-box'>
              <button className='btn btn-primary' type='button' onClick={() => setShowDetailsModal(false)}>
                Close
              </button>
            </div>
          </div>
        </div>
      </Dialog>
    </div>
  );
};

export default DeviceCard;
