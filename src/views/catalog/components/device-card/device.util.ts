import { Sku } from '../../interfaces/productsResponse';

export const IN_STOCK = 'In stock';
export const AVAILABLE_FOR_BACKORDER = 'Backorder';
export const AVAILABLE_FOR_PREORDER = 'Pre-order';
export const OUT_OF_STOCK = 'Out of stock';
export const CDN_BASE_URL = 'https://cdn.tmobile.com';

export const formatAvailability = (availability: string | undefined) => {
  switch (availability?.toUpperCase().trim()) {
    case 'AVAILABLE':
      return IN_STOCK;
    case 'AVAILABLE_FOR_BACK_ORDER':
      return AVAILABLE_FOR_BACKORDER;
    default:
      return OUT_OF_STOCK;
  }
};

export const createColorMemoryMapping = (skus: Sku[]) => {
  return skus.reduce<{ colors: { [key: string]: string[] }; memories: { [key: string]: string[] } }>(
    (acc, curr) => {
      if (curr.color && curr.memory) {
        addColorMemoryMapping(curr.color, curr.memory, acc.colors);
        addColorMemoryMapping(curr.memory, curr.color, acc.memories);
      }
      return acc;
    },
    { colors: {}, memories: {} },
  );
};

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const addColorMemoryMapping = (key: string, value: string, accumulator: any) => {
  if (!accumulator[key]) {
    accumulator[key] = [value];
  } else {
    if (!accumulator[key].includes(value)) {
      accumulator[key].push(value);
    }
  }
};
