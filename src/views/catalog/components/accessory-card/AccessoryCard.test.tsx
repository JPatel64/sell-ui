import { render, screen, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import React from 'react';
import accessoriesResponse from '../../../../../mocks/responses/iphone13-accessories.json';
import AccessoryCard from './AccessoryCard';

describe('Accessory card component', () => {
  const accessory = accessoriesResponse.accessories[0];
  beforeEach(() => {
    render(<AccessoryCard accessory={accessory} />);
  });

  it('should display accessory details', () => {
    screen.getByTestId('catalog-card');
    expect(screen.getByText(accessory.familyName)).toBeInTheDocument();
    expect(screen.getByText(/Estimated ship date/i)).toBeInTheDocument();
    expect(screen.getByText(/View details/i)).toBeInTheDocument();
    expect(screen.getAllByTestId('skuPrices').length).toBe(accessory.skuPrices.length);
    expect(screen.getByText(/Quantity/i)).toBeInTheDocument();
  });

  it('should update quantity when clicking +/- button', async () => {
    userEvent.click(screen.getByTestId('FULL194252192375')); // select pricing option to enable add button
    await waitFor(() => expect(screen.getByTestId('plus-button')).not.toBeDisabled());

    userEvent.click(screen.getByTestId('plus-button'));
    await waitFor(() => expect((screen.getByTestId('quantity') as HTMLInputElement).value).toBe('1'));

    userEvent.click(screen.getByTestId('minus-button'));
    await waitFor(() => expect((screen.getByTestId('quantity') as HTMLInputElement).value).toBe('0'));
  });
});
