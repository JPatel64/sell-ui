import { http, storageUtil } from '@tmobile/atlas-util';
import { Dialog } from 'primereact/dialog';
import React, { FunctionComponent, SyntheticEvent, useState } from 'react';
import { useAsyncCallback } from 'react-async-hook';
import TmoIcon from '../../../../assets/tmoIcon.png';
import { IAccessory } from '../../interfaces/accessoriesResponse';
import {
  ACCESSORIES_URL,
  BOX_CONTENTS,
  CART_URL,
  CDN_BASE_URL,
  OTHER_FEATURES,
  PRODUCT_DETAILS_HEADER,
} from '../../util/app.constants';
import { getGatewayUrl } from '../../util/auth.util';
import { formatDate } from '../../util/format.util';
import PaymentOptions from '../common/paymentOptions/PaymentOptions';
import { formatAvailability } from '../device-card/device.util';

interface IAccessoryCardProps {
  accessory: IAccessory;
}

const addAccessoryToCart = async (sku: string, pricingOption: string, cartId: string, lineId: string) => {
  const addAccessoryRequest = {
    sku,
    quantity: 1,
    pricingOption: pricingOption.split('--')[0],
  };
  const { data } = await http.post(
    `${getGatewayUrl()}${CART_URL}${ACCESSORIES_URL}?cartId=${cartId}&lineId=${lineId}`,
    addAccessoryRequest,
  );
  return data.accessoryId;
};

const removeAccessoryFromCart = async (accessoryId: string | undefined, cartId: string, lineId: string) => {
  await http.delete(`${getGatewayUrl()}${CART_URL}${ACCESSORIES_URL}/${accessoryId}?cartId=${cartId}&lineId=${lineId}`);
};

const AccessoryCard: FunctionComponent<IAccessoryCardProps> = ({ accessory }) => {
  const formattedAvailability = formatAvailability(accessory?.availability?.availabilityStatus);
  const hasFreeShipping = false; // replace this after we get availabileCartPromotions[]

  const [selectedPaymentOption, setSelectedPaymentOption] = useState(''); // default value should be synced what's in the cart in future iteration
  const [quantity, setQuantity] = useState(0); // default value should be synced what's in the cart in future iteration
  const [accessoriesAdded, setAccessoriesAdded] = useState<string[]>([]); // track list of accessory ids added to cart
  const [showDetailsModal, setShowDetailsModal] = useState<boolean>(false);

  const addAccessory = async () => {
    const cartId = storageUtil.getCartId();
    const lineId = storageUtil.getLineId();
    await addAccessoryToCart(accessory.skuCode, selectedPaymentOption, cartId, lineId).then((accessoryId) => {
      setAccessoriesAdded([...accessoriesAdded, accessoryId]);
      setQuantity((prev) => prev + 1);
    });
  };

  const removeAccessory = async () => {
    const accessoryId = accessoriesAdded[accessoriesAdded.length - 1];
    const cartId = storageUtil.getCartId();
    const lineId = storageUtil.getLineId();
    await removeAccessoryFromCart(accessoryId, cartId, lineId).then(() => {
      setAccessoriesAdded(accessoriesAdded.slice(0, -1));
      setQuantity((prev) => prev - 1);
    });
  };

  const asyncOnAdd = useAsyncCallback(addAccessory);
  const asyncOnRemove = useAsyncCallback(removeAccessory);

  const isAddDisabled = () => {
    return asyncOnAdd.loading || asyncOnRemove.loading || !selectedPaymentOption;
  };

  const isRemoveDisabled = () => {
    return asyncOnAdd.loading || asyncOnRemove.loading || quantity === 0;
  };

  const isPaymentOptionDisabled = () => {
    return formattedAvailability === 'Out of stock' || quantity > 0;
  };

  const handleDetailsClick = (event: SyntheticEvent<EventTarget>) => {
    event.preventDefault();
    setShowDetailsModal(true);
  };

  return (
    <>
      <div>
        <div className='col-md-* mb-5'>
          <div className='card fuse-card'>
            <div data-testid='catalog-card' className='catalog-card card-body card-content px-4'>
              <h6 className='main-heading'>{accessory.familyName}</h6>
              <div className='row d-flex align-items-center'>
                <div className='col-md-4'>
                  <img src={`${CDN_BASE_URL}/${accessory.thumbnail}`} alt={accessory.familyName} width={50} />
                </div>
                <div className='col-md-8 align-self-start px-0'>
                  <div>
                    <p data-testid='availability' className='font-weight-bold'>
                      {formatAvailability(accessory?.availability?.availabilityStatus)}
                    </p>
                    {hasFreeShipping && (
                      <p className='font-weight-bold'>
                        Free shipping <span className='rounded-circle deal-icon'>Deal!</span>
                      </p>
                    )}
                    <div data-testid='shipDate' className={hasFreeShipping ? 'mt-4' : undefined}>
                      Estimated ship date <br />
                      {formatDate(accessory?.availability?.estimatedShippingFromDateTime)}
                      &nbsp;-&nbsp;
                      {formatDate(accessory?.availability?.estimatedShippingToDateTime)}
                    </div>
                  </div>
                </div>
                <a className='ml-3 details-link mt-2' onClick={handleDetailsClick}>
                  <u>View details</u>
                </a>
                <PaymentOptions
                  disabled={isPaymentOptionDisabled()}
                  skuPricingOptions={accessory.skuPrices}
                  skuCode={accessory.skuCode}
                  selectedPaymentOption={selectedPaymentOption}
                  onSelectedPaymentOption={(option: string) => setSelectedPaymentOption(option)}
                />
                <div className='col'>
                  <p className='mb-2'>Quantity</p>
                  <div className='row'>
                    <div className='col-12 accessories-quantity-wrapper'>
                      <button
                        data-testid='minus-button'
                        className='button-decrease'
                        type='button'
                        disabled={isRemoveDisabled()}
                        onClick={asyncOnRemove.execute}
                      >
                        <i className='fa fa-minus'></i>
                      </button>
                      <input
                        data-testid='quantity'
                        type='number'
                        className='quantity-amount'
                        readOnly
                        value={quantity}
                      />
                      <button
                        data-testid='plus-button'
                        className='button-increase'
                        type='button'
                        disabled={isAddDisabled()}
                        onClick={asyncOnAdd.execute}
                      >
                        <i className='fa fa-plus'></i>
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <Dialog
          showHeader={false}
          visible={showDetailsModal}
          onHide={() => setShowDetailsModal(false)}
          style={{ width: '40%' }}
        >
          <div className='language-Update-Container pt-5 px-4'>
            <img className='tmo-icon' src={TmoIcon} alt='t-mobile icon' />
            <i role='button' tabIndex={0} className='pi pi-times' onClick={() => setShowDetailsModal(false)}></i>
            <h3 className='pt-3'>{PRODUCT_DETAILS_HEADER}</h3>

            <div>
              <h5 className='pt-3'>{OTHER_FEATURES}</h5>
              {accessory.specifications && (
                <div>
                  {accessory.specifications.map((spec, index) => {
                    return (
                      <p key={index} className='pt-2'>
                        {spec.label}:{spec.value}
                      </p>
                    );
                  })}
                </div>
              )}
            </div>
            <div>
              <h5 className='pt-3'>{BOX_CONTENTS}</h5>

              {accessory.boxContents && (
                <div>
                  {accessory.boxContents.map((value, index) => {
                    return (
                      <p key={index} className='pt-2'>
                        {value.name}
                      </p>
                    );
                  })}
                </div>
              )}
            </div>

            <div className='col-12 buttons-container button-spacing'>
              <div className='button-box'>
                <button className='btn btn-secondary' type='button' onClick={() => setShowDetailsModal(false)}>
                  Close
                </button>
              </div>
            </div>
          </div>
        </Dialog>
      </div>
    </>
  );
};

export default AccessoryCard;
