import React, { FunctionComponent } from 'react';
import { UseFilterAccessories } from '../../hooks/UseFilterAccessories';
import { IAccessory } from '../../interfaces/accessoriesResponse';
import AccessoryCard from '../accessory-card/AccessoryCard';

interface IAccessoryListProps {
  accessories: IAccessory[];
}

export const AccessoryCardList: FunctionComponent<IAccessoryListProps> = ({ accessories }) => {
  const { items } = UseFilterAccessories(accessories);

  return (
    <div className='container product-list'>
      <div className='row'>
        <div className='card-deck' id='results' role='main'>
          {items.map((accessory: IAccessory, index: number) => {
            return <AccessoryCard key={index} accessory={accessory} />;
          })}
        </div>
      </div>
    </div>
  );
};
