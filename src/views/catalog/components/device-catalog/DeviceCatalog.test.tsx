import { render, screen, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import React from 'react';
import DeviceCatalog from './DeviceCatalog';

describe('Device Catalog component', () => {
  it('should render device catalog', async () => {
    render(<DeviceCatalog />);
    await screen.findByTestId('eipJodHeader');
    expect(screen.getByTestId('eipJodHeader')).toHaveTextContent('EIP and JOD may not be processed in the same order.');
    await screen.findAllByTestId('catalog-card');
  });

  it('should enable Add to Cart Button until price option is clicked', async () => {
    render(<DeviceCatalog />);
    await screen.findByTestId('FULL610214662804');
    userEvent.click(screen.getByTestId('FULL610214662804'));
    await waitFor(() => {
      expect(screen.getByTestId('addCart610214662804')).toBeEnabled();
    });
  });
});
