import { dispatchComplete, GlobalNavUtil, http } from '@tmobile/atlas-util';
import React from 'react';
import { useAsync } from 'react-async-hook';
import { AddDeviceProvider, useAddDeviceContext } from '../../context/AddDeviceContext';
import { IProductRequest } from '../../interfaces/productsRequest';
import { IProductResponse } from '../../interfaces/productsResponse';
import {
  DEVICES_URL,
  EIP_JOD,
  PAGE_SIZE,
  PRODUCT_SUBTYPE_HANDSET,
  PRODUCT_TYPE_DEVICE,
  PRODUCT_URL,
  TRANSACTION_TYPE,
} from '../../util/app.constants';
import { getGatewayUrl } from '../../util/auth.util';
import Search from '../common/search/Search';
import { DeviceCardList } from '../device-card-list/DeviceCardList';
import { DeviceFilterContainer } from '../filter-container/DeviceFilterContainer';
import './DeviceCatalog.scss';

// const getSecureToken = async () => {
//   return await secureTokenService.getToken(getAuthConfig());
// };

const getDevices = async () => {
  //await getSecureToken();

  const model: IProductRequest = {
    transactionType: TRANSACTION_TYPE,
    productTypes: [PRODUCT_TYPE_DEVICE],
    productSubtypes: [PRODUCT_SUBTYPE_HANDSET],
    pageSize: PAGE_SIZE,
  };
  const { data } = await http.post<IProductResponse>(`${getGatewayUrl()}${PRODUCT_URL}${DEVICES_URL}`, model);
  return data.products;
};

const DeviceCatalog = () => {
  const { result: devices } = useAsync(getDevices, []);

  const navigateToNextStep = () => {
    dispatchComplete();
  };

  const navigateToShopNow = () => {
    GlobalNavUtil.launchMicroApp('shop-now');
  };

  const Buttons = () => {
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const { addDeviceState, addDeviceDispatch } = useAddDeviceContext();
    return (
      <div className='buttonsDiv'>
        <button
          data-testid='cancel-button'
          className='btn btn-primary buttons'
          type='button'
          onClick={() => navigateToShopNow()}
        >
          Cancel
        </button>
        <button
          data-testid='continue-button'
          className='btn btn-primary buttons'
          type='button'
          disabled={!addDeviceState.isDeviceAdded}
          onClick={() => navigateToNextStep()}
        >
          Continue
        </button>
      </div>
    );
  };

  return (
    <AddDeviceProvider>
      {devices && (
        <>
          <div className='bg-light catalog-container'>
            <div className='container' id='header'>
              <div>
                <div className='row'>
                  <div data-testid='eipJodHeader' className='col-sm-7 col-md-7 mt-4' style={{ fontWeight: 'bold' }}>
                    <p>{EIP_JOD}</p>
                  </div>
                  <div className='col-sm-3 col-md-3'>
                    <Search />
                  </div>
                  <div className='col-sm-2 col-md-2'>
                    <DeviceFilterContainer products={devices} />
                  </div>
                </div>
              </div>
            </div>
            <div className='album pl-5 pr-5 bg-light'>{devices && <DeviceCardList devices={devices} />}</div>
          </div>
          <div className='catalog-container'>
            <Buttons />
          </div>
        </>
      )}
    </AddDeviceProvider>
  );
};

export default DeviceCatalog;
