import { render, screen, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import React from 'react';
import { DeviceFilterContainer } from './DeviceFilterContainer';

describe('Filter container component', () => {
  it('should toggle Filter', async () => {
    render(<DeviceFilterContainer products={[]} />);

    userEvent.click(screen.getByTestId('filter-toggle'));
    await waitFor(() => {
      expect(screen.getByTestId('filter-open')).toHaveClass('filter-open');
    });
  });
});
