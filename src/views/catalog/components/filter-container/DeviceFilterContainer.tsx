import React, { FunctionComponent, useState } from 'react';
import { Product } from '../../interfaces/productsResponse';
import { getConditions } from '../../util/deviceFilters/condition';
import { getManufacturers } from '../../util/deviceFilters/manufacturer';
import { getNetworkSpeeds } from '../../util/deviceFilters/network-speed';
import { getOperatingSystems } from '../../util/deviceFilters/operating-system';
import { getPrices } from '../../util/deviceFilters/price';
import { getPricingOptions } from '../../util/deviceFilters/pricing-option';
import { getSimTypes } from '../../util/deviceFilters/sim-type';
import { FilterWrapper } from '../common/filterWrapper/FilterWrapper';
import { Filter } from '../filter/Filter';

interface IProps {
  products: Product[];
}

export const DeviceFilterContainer: FunctionComponent<IProps> = ({ products }) => {
  const [isOpen, setIsOpen] = useState(false);
  function toggleFilter() {
    setIsOpen(!isOpen);
  }
  const manufacturers = getManufacturers(products);
  const networkSpeeds = getNetworkSpeeds(products);
  const operatingSystems = getOperatingSystems();
  const conditions = getConditions();
  const prices = getPrices();
  const simTypes = getSimTypes();
  const pricingOptions = getPricingOptions();

  const orgnizedPrices = (
    p: {
      label: string;
      value: {
        min: number;
        max: number;
      };
    }[],
  ) => {
    const temp: { label: string; value: string }[] = [];
    p.forEach((a) => temp.push({ label: a.label, value: `${a.value.min}-${a.value.max}` }));
    return temp;
  };

  return (
    <FilterWrapper isOpen={isOpen} toggleFilter={toggleFilter}>
      <Filter title='Manufacturer' filterType='manufacturer' defaultOpen={true} options={manufacturers}></Filter>
      <Filter title='Operating systems' filterType='operating-system' options={operatingSystems}></Filter>
      <Filter title='Price' filterType='price' options={orgnizedPrices(prices)}></Filter>
      <Filter title='Network speeds' filterType='network-speed' options={networkSpeeds}></Filter>
      <Filter title='Condition' filterType='condition' options={conditions}></Filter>
      <Filter title='SIM type' filterType='sim-type' options={simTypes}></Filter>
      <Filter title='Pricing option' filterType='pricing-option' options={pricingOptions}></Filter>
    </FilterWrapper>
  );
};
