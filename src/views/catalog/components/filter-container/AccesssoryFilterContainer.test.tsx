import { render, screen, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import React from 'react';
import { AccessoryFilterContainer } from './AccessoryFilterContainer';

describe('Filter container component', () => {
  it('should toggle Filter', async () => {
    render(<AccessoryFilterContainer accessories={[]} />);

    userEvent.click(screen.getByTestId('filter-toggle'));
    await waitFor(() => {
      expect(screen.getByTestId('filter-open')).toHaveClass('filter-open');
    });
  });
});
