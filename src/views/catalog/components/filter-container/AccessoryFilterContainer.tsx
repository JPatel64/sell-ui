import React, { FunctionComponent, useState } from 'react';
import { IAccessory } from '../../interfaces/accessoriesResponse';
import { getAccessoryTypes } from '../../util/accessoryFilters/accessory-type';
import { getManufacturers } from '../../util/accessoryFilters/manufacturer';
import { getPrices } from '../../util/accessoryFilters/price';
import { FilterWrapper } from '../common/filterWrapper/FilterWrapper';
import { Filter } from '../filter/Filter';

interface IProps {
  accessories: IAccessory[];
}

export const AccessoryFilterContainer: FunctionComponent<IProps> = ({ accessories }) => {
  const [isOpen, setIsOpen] = useState(false);
  function toggleFilter() {
    setIsOpen(!isOpen);
  }
  const accessoryTypes = getAccessoryTypes(accessories);
  const manufacturers = getManufacturers(accessories);
  const prices = getPrices();

  const orgnizedPrices = (
    p: {
      label: string;
      value: {
        min: number;
        max: number;
      };
    }[],
  ) => {
    const temp: { label: string; value: string }[] = [];
    p.forEach((a) => temp.push({ label: a.label, value: `${a.value.min}-${a.value.max}` }));
    return temp;
  };

  return (
    <FilterWrapper isOpen={isOpen} toggleFilter={toggleFilter}>
      <Filter title='Accessory type' filterType='accessory-type' options={accessoryTypes} defaultOpen={true}></Filter>
      <Filter title='Manufacturer' filterType='manufacturer' options={manufacturers}></Filter>
      <Filter title='Price' filterType='price' options={orgnizedPrices(prices)}></Filter>
    </FilterWrapper>
  );
};
