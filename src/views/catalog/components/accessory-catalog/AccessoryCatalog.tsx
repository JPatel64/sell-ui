import { dispatchComplete, http, storageUtil } from '@tmobile/atlas-util';
import React from 'react';
import { useAsync } from 'react-async-hook';
import { useNavigate } from 'react-router';
import useStore from '../../../../store';
import { IAccessoriesResponse } from '../../interfaces/accessoriesResponse';
import { ACCESSORIES_URL, EIP_MIN_ELIGIBLE, PRODUCT_URL } from '../../util/app.constants';
import { getGatewayUrl } from '../../util/auth.util';
import { AccessoryCardList } from '../accessory-card-list/AccessoryCardList';
import Search from '../common/search/Search';
import { AccessoryFilterContainer } from '../filter-container/AccessoryFilterContainer';
import './AccessoryCatalog.scss';
const getAccessories = async (deviceSku: string, transactionType: string) => {
  const { data } = await http.get<IAccessoriesResponse>(
    `${getGatewayUrl()}${PRODUCT_URL}${ACCESSORIES_URL}?compatibleSkus=${deviceSku}&transactionType=${transactionType}`,
  );
  return data.accessories;
};

const AccessoryCatalog = () => {
  const navigate = useNavigate();
  const deviceSku = storageUtil.getSku();
  const { flowType, selectedSku } = useStore();

  // TODO: figure out why using flowType and selectedSku messes up the types
  const transactionType = 'UPGRADES';
  const { result: accessories } = useAsync(getAccessories, [deviceSku, transactionType]);

  const navigateToNextStep = () => {
    dispatchComplete();
  };

  const navigateToShopNow = () => {
    navigate('/');
  };

  const Buttons = () => {
    return (
      <div className='buttonsDiv'>
        <button
          data-testid='cancel-button'
          className='btn btn-primary buttons'
          type='button'
          onClick={() => navigateToShopNow()}
        >
          Cancel
        </button>
        <button
          data-testid='continue-button'
          className='btn btn-primary buttons'
          type='button'
          onClick={() => navigateToNextStep()}
        >
          Continue
        </button>
      </div>
    );
  };
  return (
    <>
      {accessories && (
        <>
          <div className='bg-light catalog-container'>
            <div className='container' id='header'>
              <div>
                <div className='row'>
                  <div data-testid='eipJodHeader' className='col-sm-7 col-md-7 mt-4' style={{ fontWeight: 'bold' }}>
                    <p>{EIP_MIN_ELIGIBLE}</p>
                  </div>
                  <div className='col-sm-3 col-md-3'>
                    <Search />
                  </div>
                  <div className='col-sm-2 col-md-2'>
                    <AccessoryFilterContainer accessories={accessories} />
                  </div>
                </div>
              </div>
            </div>
            <div className='album pl-5 pr-5 bg-light'>
              {accessories && <AccessoryCardList accessories={accessories} />}
            </div>
          </div>
          <div className='catalog-container'>
            <Buttons />
          </div>
        </>
      )}
    </>
  );
};

export default AccessoryCatalog;
