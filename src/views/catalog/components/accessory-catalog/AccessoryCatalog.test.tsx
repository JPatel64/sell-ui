import { render, screen } from '@testing-library/react';
import React from 'react';
import { EIP_MIN_ELIGIBLE } from '../../util/app.constants';
import AccessoryCatalog from './AccessoryCatalog';

describe('Accessory Catalog component', () => {
  it('should display catalog cards', async () => {
    render(<AccessoryCatalog />);
    await screen.findAllByTestId('catalog-card');

    expect(screen.getByTestId('eipJodHeader')).toHaveTextContent(EIP_MIN_ELIGIBLE);
  });
});
