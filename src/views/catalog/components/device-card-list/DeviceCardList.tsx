import React, { FunctionComponent, useState } from 'react';
import { UseFilterDevices } from '../../hooks/UseFilterDevices';
import { Product } from '../../interfaces/productsResponse';
import DeviceCard from '../device-card/DeviceCard';

interface IDeviceListProps {
  devices: Product[];
}

export const DeviceCardList: FunctionComponent<IDeviceListProps> = ({ devices }) => {
  const { items } = UseFilterDevices(devices);
  const [selectedPaymentOption, setSelectedPaymentOption] = useState('');
  return (
    <div className='container product-list'>
      <div className='row'>
        <div className='card-deck' id='results' role='main'>
          {items.map((i: Product) => {
            return (
              <DeviceCard
                key={i.skus[0].skuCode}
                device={i}
                selectedPaymentOption={selectedPaymentOption}
                onSelectedPaymentOption={(option) => setSelectedPaymentOption(option)}
              />
            );
          })}
        </div>
      </div>
    </div>
  );
};
