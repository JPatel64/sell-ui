import React, { useState } from 'react';
import { Accordion } from '../common/accordion/Accordion';

interface IProps {
  title: string;
  filterType: string;
  options: { label: string; value: string }[];
  defaultOpen?: boolean;
}

export const Filter = (props: IProps) => {
  const options = props.options.map((option) => {
    return {
      ...option,
    };
  });

  const [selectedOptions, setSelectedOptions] = useState<string[]>([]);

  const updateSelectedOptions = (
    event: React.ChangeEvent<HTMLInputElement>,
    option: { label: string; value: string },
  ) => {
    let optionsCopy = selectedOptions;

    if (event.target.checked) {
      optionsCopy.push(option.value);
    } else {
      optionsCopy = optionsCopy.filter((a) => a !== option.value);
    }

    setSelectedOptions(optionsCopy);
    const customEvent = new CustomEvent('filter_results', {
      detail: { filterType: props.filterType, value: optionsCopy },
    });
    document.dispatchEvent(customEvent);
  };

  return (
    <Accordion header={props.title} defaultOpen={props.defaultOpen}>
      <div className='p-content checkbox-group'>
        {options.map((option, index) => {
          return (
            <label key={option.label} className='form-label'>
              <span className='tmo-checkbox'>
                <input
                  className={`filter-${props.filterType}`}
                  type='checkbox'
                  value={index}
                  onChange={(event) => updateSelectedOptions(event, option)}
                />
                <span className='checkbox'>
                  <i className='fa fa-check' />
                </span>
              </span>
              <p className='content'>{option.label}</p>
            </label>
          );
        })}
      </div>
    </Accordion>
  );
};
