import { render, screen, waitFor } from '@testing-library/react';
import React from 'react';
import { getOperatingSystems } from '../../util/deviceFilters/operating-system';
import { Filter } from './Filter';

describe('Filter Component', () => {
  it('should display Filter Header', () => {
    render(<Filter title='Operating Systems' filterType='operating-system' options={getOperatingSystems()}></Filter>);

    expect(screen.getByRole('heading')).toHaveTextContent('Operating Systems');
  });

  it('should display Filter Options', () => {
    render(<Filter title='Operating Systems' filterType='operating-system' options={getOperatingSystems()}></Filter>);

    waitFor(() => {
      expect(screen.getByText('Android')).toBeInTheDocument();
      expect(screen.getByText('iOS')).toBeInTheDocument();
      expect(screen.getByText('Other')).toBeInTheDocument();
    });
  });
});
