export const convertPricingOptionName = (option: string) => {
  const eipRegex = /^CRP\d{1,2}$/;
  const jodRegex = /^CRP\d{3}$/;

  if (option === 'FRP') {
    return 'FULL';
  } else if (eipRegex.test(option)) {
    return 'EIP';
  } else if (jodRegex.test(option)) {
    return 'JOD';
  }
  return option;
};

// converting to values that are expected when adding a sku to the cart
// from DCP: EIP is CRPx/CRPxx, JOD is CRPxxx
