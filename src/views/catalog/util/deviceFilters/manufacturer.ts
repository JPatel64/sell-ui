import { Product } from '../../interfaces/productsResponse';

export const getManufacturers = (products: Product[]) => {
  return Array.from(new Set(products.map((p) => p.manufacturer)))
    .sort((a, b) => a.localeCompare(b))
    .map((m) => {
      return {
        label: m,
        value: m,
      };
    });
};

export const filter = (items: Product[], selectedManufacturers: string[]) => {
  return items.filter(
    (item) => selectedManufacturers.length === 0 || selectedManufacturers.includes(item.manufacturer),
  );
};
