import { Product } from '../../interfaces/productsResponse';
export const PRICING_OPTION_FRP = 'FULL';
export const PRICING_OPTION_JOD = 'JOD';
export const PRICING_OPTION_EIP = 'EIP';
// moved these from constants.ts due to issues importing stuff from ts

export const getPricingOptions = () => {
  return [
    {
      label: 'Equipment Installment Plan',
      value: PRICING_OPTION_EIP,
    },
    {
      label: 'Full Retail Price',
      value: PRICING_OPTION_FRP,
    },
    {
      label: 'Jump! on Demand',
      value: PRICING_OPTION_JOD,
    },
  ];
};

export const filter = (items: Product[], selectedPricingOptions: string[]) => {
  return items.filter(
    (item) =>
      selectedPricingOptions.length === 0 ||
      item.skus.some((sku) =>
        sku.skuPrices.some((skuPrice) => selectedPricingOptions.includes(skuPrice.pricingOptionName)),
      ),
  );
};
