import devices from '../../../../../mocks/responses/devices.json';
import { Product, Sku } from '../../interfaces/productsResponse';
import { filter, getSimTypes } from './sim-type';

describe('SIM type Filter', () => {
  const products: Product[] = devices.products;

  it('should return SIM and eSIM as options', () => {
    const result = getSimTypes();
    expect(result.length).toBe(2);
    expect(result[0]).toEqual({ label: 'eSIM', value: 'eSIM' });
    expect(result[1]).toEqual({ label: 'SIM', value: 'pSIM' });
  });

  it('should filter list of products by selected sim type', () => {
    const result = filter(products, ['eSIM']);
    expect(result.length).toBe(12);
    expect(result.every((p) => p.skus.some((sku: Sku) => sku.simType?.some((type) => type === 'eSIM')))).toBeTruthy();
  });

  it('should return list of products if no options selected', () => {
    const result = filter(products, []);
    expect(result.length).toEqual(products.length);
  });
});
