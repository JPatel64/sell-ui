import { Product } from '../../interfaces/productsResponse';
export const getSimTypes = () => {
  return [
    {
      label: 'eSIM',
      value: 'eSIM',
    },
    {
      label: 'SIM',
      value: 'pSIM',
    },
  ];
};

export const filter = (items: Product[], selectedSimTypes: string[]) => {
  return items.filter(
    (item) =>
      selectedSimTypes.length === 0 ||
      item.skus.some((sku) => sku.simType?.some((type) => selectedSimTypes.includes(type))),
  );
};
