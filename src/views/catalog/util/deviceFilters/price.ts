import { Product } from '../../interfaces/productsResponse';
import { PRICING_OPTION_FRP } from './pricing-option';

export const getPrices = () => {
  return [
    {
      min: 100,
      max: 400,
    },
    {
      min: 400,
      max: 700,
    },
    {
      min: 700,
      max: 1000,
    },
    {
      min: 1000,
      max: 3000,
    },
  ].map((price) => {
    return {
      label: `$${price.min} to $${price.max}`,
      value: price,
    };
  });
};

export const filter = (items: Product[], selectedPriceRanges: string[]) => {
  const priceRangeObjs: { min: number; max: number }[] = [];
  selectedPriceRanges.forEach((a) => {
    priceRangeObjs.push({ min: +a.split('-')[0], max: +a.split('-')[1] });
  });

  return items.filter(
    (item) =>
      priceRangeObjs.length === 0 ||
      item.skus.some((sku) =>
        sku.skuPrices?.some(
          (skuPrice) =>
            skuPrice.pricingOptionName === PRICING_OPTION_FRP &&
            isInSelectedPriceRanges(skuPrice?.skuPrice?.priceSummary?.payNowPrice?.salePrice?.amount, priceRangeObjs),
        ),
      ),
  );
};

const isInSelectedPriceRanges = (price: number, selectedPriceRanges: { min: number; max: number }[]) => {
  return selectedPriceRanges.some((priceRange: { min: number; max: number }) => {
    return price >= priceRange.min && price <= priceRange.max;
  });
};
