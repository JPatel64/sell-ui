import devices from '../../../../../mocks/responses/devices.json';
import { Product } from '../../interfaces/productsResponse';
import { filter, getPrices } from './price';

describe('Price Filter', () => {
  const products: Product[] = devices.products;

  it('should return price ranges as options', () => {
    const result = getPrices();
    expect(result.length).toBe(4);
    expect(result[0]).toEqual({ label: '$100 to $400', value: { min: 100, max: 400 } });
    expect(result[1]).toEqual({ label: '$400 to $700', value: { min: 400, max: 700 } });
    expect(result[2]).toEqual({ label: '$700 to $1000', value: { min: 700, max: 1000 } });
    expect(result[3]).toEqual({ label: '$1000 to $3000', value: { min: 1000, max: 3000 } });
  });

  it('should filter list of products by selected price range', () => {
    const strArr: string[] = ['400-700'];
    products[0].skus[0].skuPrices[0].pricingOptionName = 'FULL';
    const result = filter(products, strArr);
    expect(result.length).toBe(1);
  });

  it('should return list of products if no options selected', () => {
    const result = filter(products, []);
    expect(result.length).toEqual(products.length);
  });
});
