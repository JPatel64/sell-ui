import { Product } from '../../interfaces/productsResponse';
export const getConditions = () => {
  return [
    {
      label: 'New',
      value: 'A-Stock',
    },
    {
      label: 'Certified pre-owned',
      value: 'Pre-owned',
    },
  ];
};

export const filter = (items: Product[], selectedConditions: string[]) => {
  return items.filter(
    (item) => selectedConditions.length === 0 || item.skus.some((s) => selectedConditions.includes(s.condition)),
  );
};
