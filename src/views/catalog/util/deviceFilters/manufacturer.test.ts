import devices from '../../../../../mocks/responses/devices.json';
import { Product } from '../../interfaces/productsResponse';
import { filter, getManufacturers } from './manufacturer';

describe('Manufacturer Filter', () => {
  const products: Product[] = devices.products;

  it('should return manufacturer options based given list of products', () => {
    const result = getManufacturers(products);
    expect(result.length).toBe(4);
  });

  it('should filter list of products by selected manufacturer', () => {
    const result = filter(products, ['Samsung']);
    expect(result.length).toBe(32);
    expect(result.every((p) => p.manufacturer === 'Samsung')).toBeTruthy();
  });

  it('should return list of products if no options selected', () => {
    const result = filter(products, []);
    expect(result.length).toEqual(products.length);
  });
});
