import devices from '../../../../../mocks/responses/devices.json';
import { filter, getConditions } from './condition';

describe('Condition Filter', () => {
  const products = devices.products;

  it('should return New and Pre-owned as options', () => {
    const result = getConditions();
    expect(result.length).toBe(2);
    expect(result[0]).toEqual({ label: 'New', value: 'A-Stock' });
    expect(result[1]).toEqual({ label: 'Certified pre-owned', value: 'Pre-owned' });
  });

  it('should filter list of products by selected condition', () => {
    const result = filter(products, ['A-Stock']);
    expect(result.length).toBe(40);
    expect(result.every((p) => p.skus.some((sku) => sku.condition === 'A-Stock'))).toBeTruthy();
  });

  it('should return list of products if no options selected', () => {
    const result = filter(products, []);
    expect(result.length).toEqual(products.length);
  });
});
