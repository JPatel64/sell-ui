import devices from '../../../../../mocks/responses/devices.json';
import { Product } from '../../interfaces/productsResponse';
import { filter, getOperatingSystems } from './operating-system';

describe('Operating Systems Filter', () => {
  const products: Product[] = devices.products;

  it('should return android, ios, and other as options', () => {
    const result = getOperatingSystems();
    expect(result.length).toBe(3);
    expect(result[0]).toEqual({ label: 'Android', value: 'Android' });
    expect(result[1]).toEqual({ label: 'iOS', value: 'iOS' });
    expect(result[2]).toEqual({ label: 'Other', value: 'Other' });
  });

  it('should filter list of products by selected operating system', () => {
    const result = filter(products, ['Android']);
    expect(result.length).toBe(36);
    expect(result.every((p) => p.operatingSystem === 'Android')).toBeTruthy();
  });

  it('should return list of products if no options selected', () => {
    const result = filter(products, []);
    expect(result.length).toEqual(products.length);
  });
});
