import { Product } from '../../interfaces/productsResponse';

const ANDROID = 'Android';
const IOS = 'iOS';
const OTHER = 'Other';

export const getOperatingSystems = () => {
  return [
    {
      label: ANDROID,
      value: ANDROID,
    },
    {
      label: IOS,
      value: IOS,
    },
    {
      label: OTHER,
      value: OTHER,
    },
  ];
};

export const filter = (items: Product[], selectedOperatingSystems: string[]) => {
  return items.filter(
    (item) =>
      selectedOperatingSystems.length === 0 ||
      (item.operatingSystem && selectedOperatingSystems.includes(item.operatingSystem)) ||
      (selectedOperatingSystems.includes(OTHER) && item.operatingSystem !== ANDROID && item.operatingSystem !== IOS),
  );
};
