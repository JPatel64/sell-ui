import devices from '../../../../../mocks/responses/devices.json';
import { Product } from '../../interfaces/productsResponse';
import { filter, getPricingOptions } from './pricing-option';

describe('Pricing Options Filter', () => {
  const products: Product[] = devices.products;

  it('should return EIP, JOD, FRP as options', () => {
    const result = getPricingOptions();
    expect(result.length).toBe(3);
    expect(result[0]).toEqual({ label: 'Equipment Installment Plan', value: 'EIP' });
    expect(result[1]).toEqual({ label: 'Full Retail Price', value: 'FULL' });
    expect(result[2]).toEqual({ label: 'Jump! on Demand', value: 'JOD' });
  });

  it('should filter list of products by selected pricing option', () => {
    const result = filter(products, ['FRP']);
    expect(result.length).toBe(40);
    expect(
      result.every((p) => p.skus.some((sku) => sku.skuPrices.some((s) => s.pricingOptionName === 'FRP'))),
    ).toBeTruthy();
  });

  it('should return list of products if no options selected', () => {
    const result = filter(products, []);
    expect(result.length).toEqual(products.length);
  });
});
