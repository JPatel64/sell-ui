import devices from '../../../../../mocks/responses/devices.json';
import { Product } from '../../interfaces/productsResponse';
import { filter, getNetworkSpeeds } from './network-speed';

describe('Network Speeds Filter', () => {
  const products: Product[] = devices.products;

  it('should return network speed options based given list of products', () => {
    const result = getNetworkSpeeds(products);
    expect(result.length).toBe(4);
  });

  it('should filter list of products by selected network speeds', () => {
    const result = filter(products, ['5G']);
    expect(result.length).toBe(26);
    expect(result.every((p) => p.networkSpeeds?.includes('5G'))).toBeTruthy();
  });

  it('should return list of products if no options selected', () => {
    const result = filter(products, []);
    expect(result.length).toEqual(products.length);
  });
});
