import { Product } from '../../interfaces/productsResponse';
import { SPECIFICATION_NETWORKSPEED } from '../app.constants';

export const getNetworkSpeeds = (products: Product[]) => {
  let networkSpeeds: string[] = [];
  products.forEach(
    (p) =>
      (p.networkSpeeds = p.specifications?.find(
        (specification) => specification.label === SPECIFICATION_NETWORKSPEED,
      )?.value),
  );
  products.forEach((p) => (networkSpeeds = networkSpeeds.concat(p.networkSpeeds ? p.networkSpeeds : '')));
  return Array.from(new Set(networkSpeeds))
    .sort((a, b) => a.localeCompare(b))
    .map((ns) => {
      return {
        label: ns,
        value: ns,
      };
    });
};

export const filter = (items: Product[], selectedNetworkSpeeds: string[]) => {
  return items.filter(
    (item) => selectedNetworkSpeeds.length === 0 || item.networkSpeeds?.some((s) => selectedNetworkSpeeds.includes(s)),
  );
};
