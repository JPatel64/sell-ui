import { AtlasAuthConfig, GlobalNavUtil } from '@tmobile/atlas-util';
import { API_GATEWAY_LOCAL, MEG_GATEWAY_URL_EXTN } from './app.constants';

export function getGatewayUrl() {
  return process.env.NODE_ENV === 'production'
    ? GlobalNavUtil.getAtlasGatewayUrl('catalog-atlas') + MEG_GATEWAY_URL_EXTN
    : API_GATEWAY_LOCAL;
}

export function getAuthConfig(): AtlasAuthConfig {
  return {
    applicationId: 'catalog-service',
  };
}
