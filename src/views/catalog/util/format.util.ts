/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { FormattedMemoryOption, Image, MemoryOption, Sku } from '../interfaces/productsResponse';

export const formatCurrency = (amount = 0) => {
  const formatter = Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD',
  });
  return formatter.format(amount);
};

export const sortAndSplitMemoryOptions = (memoryOptions: MemoryOption[], sortDirection: string) => {
  const splitMemoryOptions = memoryOptions.map((mo) => ({
    capacity: mo.name.match(/\d+/g)![0],
    unit: mo.name.match(/[a-zA-Z]+/g)![0],
  }));

  const sortByUnit = (formattedMemoryOptions: FormattedMemoryOption[], unit: string, direction: string) => {
    const filteredOptions = formattedMemoryOptions.filter((mo) => mo.unit === unit);

    if (direction === 'descending') {
      return filteredOptions.sort((a, b) => Number(b.capacity) - Number(a.capacity));
    } else {
      return filteredOptions.sort((a, b) => Number(a.capacity) - Number(b.capacity));
    }
  };

  const sortedMB = sortByUnit(splitMemoryOptions, 'MB', sortDirection);
  const sortedGB = sortByUnit(splitMemoryOptions, 'GB', sortDirection);
  const sortedTB = sortByUnit(splitMemoryOptions, 'TB', sortDirection);

  if (sortDirection === 'descending') {
    return [...sortedTB, ...sortedGB, ...sortedMB];
  } else {
    return [...sortedMB, ...sortedGB, ...sortedTB];
  }
};

export const findThumbNail = (sku: Sku | undefined) => {
  const thumbnail = sku?.images?.find((image) => image.imageType === 'ThumbNail');
  if (thumbnail) {
    return thumbnail;
  } else {
    return sku?.images ? sku.images[0] : ({} as Image);
  }
};

export const formatDate = (date: string | null | undefined) => {
  return date
    ? new Date(date).toLocaleString('default', {
        month: 'numeric',
        day: '2-digit',
        year: 'numeric',
      })
    : '';
};
