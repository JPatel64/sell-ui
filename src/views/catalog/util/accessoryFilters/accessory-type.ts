import { IAccessory } from './../../interfaces/accessoriesResponse';

export const getAccessoryTypes = (accessories: IAccessory[]) => {
  return Array.from(new Set(accessories.map((a) => a.subtype)))
    .sort((a, b) => a.localeCompare(b))
    .map((t) => {
      return {
        label: t,
        value: t,
      };
    });
};

export const filter = (items: IAccessory[], selectedAccessoryTypes: string[]) => {
  return items.filter((item) => selectedAccessoryTypes.length === 0 || selectedAccessoryTypes.includes(item.subtype));
};
