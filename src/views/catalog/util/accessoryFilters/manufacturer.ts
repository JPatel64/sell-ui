import { IAccessory } from '../../interfaces/accessoriesResponse';

export const getManufacturers = (accessories: IAccessory[]) => {
  return Array.from(new Set(accessories.map((p) => p.manufacturer)))
    .sort((a, b) => a.localeCompare(b))
    .map((m) => {
      return {
        label: m,
        value: m,
      };
    });
};

export const filter = (items: IAccessory[], selectedManufacturers: string[]) => {
  return items.filter(
    (item) => selectedManufacturers.length === 0 || selectedManufacturers.includes(item.manufacturer),
  );
};
