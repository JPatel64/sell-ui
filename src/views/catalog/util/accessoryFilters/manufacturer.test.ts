import accessoriesData from '../../../../../mocks/responses/iphone13-accessories.json';
import { IAccessory } from './../../interfaces/accessoriesResponse';
import { filter, getManufacturers } from './manufacturer';

describe('Manufacturer Filter', () => {
  const accessories: IAccessory[] = accessoriesData.accessories;

  it('should return manufacturer options based given list of accessories', () => {
    const result = getManufacturers(accessories);
    expect(result.length).toBe(24);
  });

  it('should filter list of accessories by selected manufacturer', () => {
    const result = filter(accessories, ['Apple']);
    expect(result.length).toBe(19);
    expect(result.every((p) => p.manufacturer === 'Apple')).toBeTruthy();
  });

  it('should return list of accessories if no options selected', () => {
    const result = filter(accessories, []);
    expect(result.length).toEqual(accessories.length);
  });
});
