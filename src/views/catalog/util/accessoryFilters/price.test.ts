import accessoriesData from '../../../../../mocks/responses/iphone13-accessories.json';
import { IAccessory } from '../../interfaces/accessoriesResponse';
import { filter, getPrices } from './price';

describe('Price Filter', () => {
  const accessories: IAccessory[] = accessoriesData.accessories;
  it('should return price ranges as options', () => {
    const result = getPrices();
    expect(result.length).toBe(4);
    expect(result[0]).toEqual({ label: '$0 to $40', value: { min: 0, max: 40 } });
    expect(result[1]).toEqual({ label: '$40 to $100', value: { min: 40, max: 100 } });
    expect(result[2]).toEqual({ label: '$100 to $300', value: { min: 100, max: 300 } });
    expect(result[3]).toEqual({ label: '$300 & above', value: { min: 300, max: 9999 } });
  });

  it('should filter list of accessories by selected price range', () => {
    const strArr: string[] = ['0-40'];
    accessories[0].skuPrices[0].pricingOptionName = 'FULL';
    const result = filter(accessories, strArr);
    expect(result.length).toBe(1);
  });

  it('should return list of accessories if no options selected', () => {
    const result = filter(accessories, []);
    expect(result.length).toEqual(accessories.length);
  });
});
