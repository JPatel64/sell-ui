import acc from '../../../../../mocks/responses/iphone13-accessories.json';
import { IAccessory } from './../../interfaces/accessoriesResponse';
import { filter, getAccessoryTypes } from './accessory-type';

describe('Accessory type Filter', () => {
  const accessories: IAccessory[] = acc.accessories;
  it('should return accessory type options based given list of accessories', () => {
    const result = getAccessoryTypes(accessories);
    expect(result.length).toBe(8);
  });

  it('should filter list of accessories by selected accessory type', () => {
    const result = filter(accessories, ['Chargers & adapters']);
    expect(result.length).toBe(24);
    expect(result.every((p) => p.subtype === 'Chargers & adapters')).toBeTruthy();
  });

  it('should return list of accessories if no accessory type option selected', () => {
    const result = filter(accessories, []);
    expect(result.length).toEqual(accessories.length);
  });
});
