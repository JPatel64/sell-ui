import { IAccessory } from '../../interfaces/accessoriesResponse';

export const getPrices = () => {
  return [
    {
      min: 0,
      max: 40,
    },
    {
      min: 40,
      max: 100,
    },
    {
      min: 100,
      max: 300,
    },
    {
      min: 300,
      max: 9999,
    },
  ].map((price) => {
    return {
      label: price.max === 9999 ? `$${price.min} & above` : `$${price.min} to $${price.max}`,
      value: price,
    };
  });
};

export const filter = (items: IAccessory[], selectedPriceRanges: string[]) => {
  const priceRangeObjs: { min: number; max: number }[] = [];
  selectedPriceRanges.forEach((a) => {
    priceRangeObjs.push({ min: +a.split('-')[0], max: +a.split('-')[1] });
  });

  return items.filter(
    (item) =>
      priceRangeObjs.length === 0 ||
      item.skuPrices?.some(
        (skuPrice) =>
          skuPrice.pricingOptionName === 'FULL' &&
          isInSelectedPriceRanges(skuPrice?.skuPrice?.priceSummary?.payNowPrice?.salePrice?.amount, priceRangeObjs),
      ),
  );
};

const isInSelectedPriceRanges = (price: number, selectedPriceRanges: { min: number; max: number }[]) => {
  return selectedPriceRanges.some((priceRange: { min: number; max: number }) => {
    return price >= priceRange.min && price <= priceRange.max;
  });
};
