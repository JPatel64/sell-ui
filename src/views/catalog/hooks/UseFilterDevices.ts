import { useEffect, useState } from 'react';
import { search } from '../components/common/search/search.util';
import { Product } from '../interfaces/productsResponse';

interface IFilterOptions {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [filterType: string]: any;
}

const UseFilterDevices = (products: Product[]) => {
  const [items, setItems] = useState([...products]);
  let searchTerm: string[];
  let result: Product[] = [];
  const filterOptions: IFilterOptions = {};
  useEffect(() => {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const filterItems = async (event: any) => {
      if (event.detail.filterType !== 'search') {
        filterOptions[event.detail.filterType] = event.detail.value;
      } else {
        searchTerm = event.detail.value;
      }

      result = await applyFilters(products, filterOptions);

      if (searchTerm?.length > 0) {
        result = search(result, searchTerm);
      }
      setItems(result);
    };
    document.addEventListener('filter_results', filterItems);
    return () => document.removeEventListener('filter_results', filterItems);
  }, [products]);
  return { items, setItems };
};

const applyFilters = async (items: Product[], filterOptions: IFilterOptions) => {
  let result = [...items];
  for (const [key, option] of Object.entries(filterOptions)) {
    const filter = await import(`../util/deviceFilters/${key}.ts`);
    result = filter.filter(result, option);
  }
  return result;
};

export { UseFilterDevices };
