import { useEffect, useState } from 'react';
import { search } from '../components/common/search/search.util';
import { IAccessory } from '../interfaces/accessoriesResponse';

interface IFilterOptions {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [filterType: string]: any;
}

const UseFilterAccessories = (accessories: IAccessory[]) => {
  const [items, setItems] = useState([...accessories]);
  let result: IAccessory[] = [];
  let searchTerm: string[];
  const filterOptions: IFilterOptions = {};
  useEffect(() => {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const filterItems = async (event: any) => {
      if (event.detail.filterType !== 'search') {
        filterOptions[event.detail.filterType] = event.detail.value;
      } else {
        searchTerm = event.detail.value;
      }
      result = await applyFilters(accessories, filterOptions);

      if (searchTerm?.length > 0) {
        result = search(result, searchTerm);
      }

      setItems(result);
    };
    document.addEventListener('filter_results', filterItems);
    return () => document.removeEventListener('filter_results', filterItems);
  }, [accessories]);
  return { items, setItems };
};

const applyFilters = async (items: IAccessory[], filterOptions: IFilterOptions) => {
  let result = [...items];
  for (const [key, option] of Object.entries(filterOptions)) {
    const filter = await import(`../util/accessoryFilters/${key}.ts`);
    result = filter.filter(result, option);
  }
  return result;
};

export { UseFilterAccessories };
