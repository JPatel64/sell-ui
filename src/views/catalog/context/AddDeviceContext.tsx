/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { createContext, FC, useContext, useReducer } from 'react';
export const UPDATE_IS_DEVICE_ADDED = 'UPDATE_IS_DEVICE_ADDED';

interface IInitialState {
  isDeviceAdded: boolean;
}

const AddDeviceContext = createContext<any>({});
const initialState: IInitialState = {
  isDeviceAdded: false,
};

const init = () => {
  return initialState;
};

const addDeviceReducer = (state: IInitialState, action: any) => {
  switch (action.type) {
    case UPDATE_IS_DEVICE_ADDED:
      return { ...state, isDeviceAdded: action.payload };
    default:
      break;
  }
};

interface IProps {
  children: any;
}

const AddDeviceProvider: FC<IProps> = ({ children }) => {
  const [state, dispatch] = useReducer(addDeviceReducer, initialState as any, init as any);
  const value = { addDeviceState: state, addDeviceDispatch: dispatch };
  return <AddDeviceContext.Provider value={value}>{children}</AddDeviceContext.Provider>;
};

const useAddDeviceContext = () => {
  const context = useContext(AddDeviceContext);
  if (context === undefined) {
    throw new Error('useAddDeviceContext must be used within a AddDeviceProvider');
  }

  return context;
};

export { AddDeviceProvider, useAddDeviceContext, AddDeviceContext };
