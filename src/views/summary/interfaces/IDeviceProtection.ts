export interface IDeviceProtection {
  notifications?: null;
  services: Service[];
}

export interface Service {
  category: string;
  offerId: string;
  offerName: string;
  description: string;
  shortDescription: string;
  offerType: string;
  offerSubtype: string;
  socIndicator: null;
  capabilityCode: null;
  tier: string;
  currentSelection: boolean;
  existingSelection: boolean;
  cartConflict: boolean;
  availability: string;
  serviceGroup: string;
  taxInclusive: boolean;
  price: Price;
  conflictSkus: string[];
}

export interface Price {
  priceSummary: PriceSummary;
}

export interface PriceSummary {
  monthlyPrice: MonthlyPrice;
  payNowPrice: PayNowPrice;
}

export interface MonthlyPrice {
  listPrice: ListPriceClass;
  salePrice: ListPriceClass;
  contractTerm: number;
  contractFrequency: string;
}

export interface ListPriceClass {
  amount: number;
  display: string;
}

export interface PayNowPrice {
  listPrice: ListPriceClass;
  salePrice: ListPriceClass;
}

export interface IAddDeviceProtection {
  addServices?: Services;
  removeServices?: Services;
  removeExistingServices?: Services;
}

export interface Services {
  offerIds: string[];
}
