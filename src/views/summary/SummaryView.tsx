import React from 'react';
import Summary from './components/summary/Summary';

function SummaryView() {
  return <Summary />;
}

export default SummaryView;
