import { faExternalLink, faFlag } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { FunctionComponent, useState } from 'react';
import { DECLINE_PROTECTION_VALUE, DEVICE_PROTECTION_C2_LINK } from '../../../../util/app.constants';
import HTMLConstants from '../../../../util/html.constants';
import { IDeviceProtection, Service } from '../../interfaces/IDeviceProtection';
import styles from './DeviceProtection.module.scss';

interface IDeviceProtectionProps {
  protectionOptions: IDeviceProtection | undefined;
  deviceSelection: string;
  setDeviceSelection: (e: string) => void;
  setIsShowValidationMessage: (e: boolean) => void;
  isShowValidationMessage: boolean;
}

const DeviceProtection: FunctionComponent<IDeviceProtectionProps> = ({
  protectionOptions,
  deviceSelection,
  setDeviceSelection,
  setIsShowValidationMessage,
  isShowValidationMessage,
}) => {
  const [isShowConflictMessage, setisShowConflictMessage] = useState(
    protectionOptions?.services.find((s) => s.cartConflict) !== undefined,
  );
  const selectionChanged = (selection: string) => {
    setIsShowValidationMessage(false);
    setDeviceSelection(selection);
    setisShowConflictMessage(false);
  };
  const isDisable = (protection: Service) => {
    return protection.cartConflict;
  };

  return (
    <div className={`${styles['dpContainer']} ml-3`} data-testid='device-protection'>
      <div className={styles.headerText}>{HTMLConstants.DEVICE_PROTECTION}</div>
      <div className='mt-3 mb-3'>{HTMLConstants.MONTHLY_COST_TEXT}</div>
      {isShowConflictMessage && (
        <div className='mb-3 ml-2'>
          <FontAwesomeIcon className={styles['icon']} icon={faFlag}></FontAwesomeIcon>
          <span className='ml-2'>{HTMLConstants.PROTECTION_NOT_COMPATIBLE}</span>
        </div>
      )}
      {isShowValidationMessage && (
        <div className='mb-3 ml-2'>
          <FontAwesomeIcon className={styles['icon']} icon={faFlag}></FontAwesomeIcon>
          <span className='ml-2'>{HTMLConstants.SELECT_PROTECTION_OPTION}</span>
        </div>
      )}
      <div className='my-3' style={{ display: 'none' }}>
        <span className='mr-2'>
          <FontAwesomeIcon className={styles['icon']} icon={faFlag}></FontAwesomeIcon>
        </span>
        {HTMLConstants.PROTECTION_NOT_AVAILABLE}
      </div>
      <div className='my-3' style={{ display: 'none' }}>
        <span className='mr-2'>
          <FontAwesomeIcon className={styles['icon']} icon={faFlag}></FontAwesomeIcon>
        </span>
        {HTMLConstants.NO_PROTECTION_PLAN}
      </div>
      {protectionOptions &&
        protectionOptions.services.map((protection, index) => {
          return (
            <div
              className={`${styles['device-information']} ${isDisable(protection) && styles['not-available']}`}
              key={'option' + index}
            >
              <input
                data-testid={protection.offerId}
                type='radio'
                name='deviceOption'
                value={protection.offerId}
                disabled={isDisable(protection)}
                checked={deviceSelection === protection.offerId}
                onChange={(e) => selectionChanged(e.currentTarget.value)}
              />
              <div className={`${styles['offer-name']} ${isDisable(protection) && styles['not-available']}`}>
                <div style={{ width: '400px' }} className={`${isDisable(protection) && styles['not-available']}`}>
                  {protection.offerName}
                </div>
                <div style={{ width: '100px' }} className={`${isDisable(protection) && styles['not-available']}`}>
                  {protection.price?.priceSummary?.monthlyPrice?.salePrice?.display}/month
                </div>
              </div>
              <div className={`${styles['device-description']} ${isDisable(protection) && styles['not-available']}}`}>
                <label
                  className={`${isDisable(protection) && styles['not-available']}`}
                  htmlFor={protection.offerId}
                  dangerouslySetInnerHTML={{ __html: protection.description }}
                ></label>
                <a
                  className={`${styles['c2-document']} ${isDisable(protection) && styles['not-available']}`}
                  href={DEVICE_PROTECTION_C2_LINK}
                >
                  View C2 document{' '}
                  <span className='ml-2'>
                    <FontAwesomeIcon style={{ height: '0.9rem' }} icon={faExternalLink}></FontAwesomeIcon>
                  </span>
                </a>
              </div>
            </div>
          );
        })}
      <div className={styles['device-information']} key='optionDecline'>
        <input
          data-testid='decline'
          type='radio'
          name='deviceOption'
          value={DECLINE_PROTECTION_VALUE}
          onChange={(e) => selectionChanged(e.currentTarget.value)}
        />
        <div className={`${styles['offer-name']} parent-div`}>
          <div style={{ width: '500px' }}>{HTMLConstants.DECLINE_PROTECTION}</div>
        </div>
        <div className={`${styles['decline-offer']} mt-2`}>
          <label htmlFor='decline'>{HTMLConstants.DECLINE_PROTECTION_DESC1}</label>
        </div>
        <div className={styles['decline-offer']}>
          <label style={{ fontWeight: 'bold' }}>{HTMLConstants.DECLINE_PROTECTION_DESC2}</label>
        </div>
      </div>
    </div>
  );
};

export default DeviceProtection;
