import { fireEvent, render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import React from 'react';
import HTMLConstants from '../../../../util/html.constants';
import { IDeviceProtection } from '../../interfaces/IDeviceProtection';
import DeviceProtection from './DeviceProtection';

describe('Summary method component', () => {
  const mockDPOptions: IDeviceProtection = {
    services: [
      {
        category: 'Bundles',
        offerId: 'P3605',
        offerName: 'Protection<360>®',
        description:
          "Covers accidental damage, loss/theft and hardware service/mechanical breakdown after manuf. warranty; JUMP!® Upgrades*; AppleCare Services on eligible Apple devices; screen protector replacement*; tech support and security apps. *Eligibility req's apply.For complete details, including benefits, exclusions, limitations and costs, please visit <a href=http://mytmoclaim.com/terms>mytmoclaim.com/terms</a>. Taxes are not included in the cost.",
        shortDescription:
          "Covers accidental damage, loss/theft and hardware service/mechanical breakdown after manuf. warranty; JUMP!® Upgrades*; AppleCare Services on eligible Apple devices; screen protector replacement*; tech support and security apps. *Eligibility req's apply.",
        offerType: 'AddOn',
        offerSubtype: 'SoftGood',
        tier: '5',
        socIndicator: null,
        capabilityCode: null,
        availability: 'AVAILABLE',
        serviceGroup: 'Handset Protection and Upgrade',
        cartConflict: true,
        currentSelection: false,
        existingSelection: true,
        taxInclusive: false,
        price: {
          priceSummary: {
            monthlyPrice: {
              listPrice: {
                amount: 18.0,
                display: '$18.00',
              },
              salePrice: {
                amount: 18.0,
                display: '$18.00',
              },
              contractTerm: 0,
              contractFrequency: 'MONTHLY',
            },
            payNowPrice: {
              listPrice: {
                amount: 0.0,
                display: '$0.00',
              },
              salePrice: {
                amount: 0.0,
                display: '$0.00',
              },
            },
          },
        },
        conflictSkus: ['P3606', 'NYP3604'],
      },
      {
        category: 'Device Protection',
        offerId: 'INTELMCAF',
        offerName: 'McAfee® Security for T-Mobile',
        description:
          '<p>McAfee&reg; Security suite including: &bull; Device content protection &bull; ID protection</p> Taxes are not included in the cost.',
        shortDescription:
          '<p>McAfee&reg; Security suite including: &bull; Device content protection &bull; ID protection</p>',
        offerType: 'AddOn',
        offerSubtype: 'SoftGood',
        tier: '5',
        socIndicator: null,
        capabilityCode: null,
        availability: 'AVAILABLE',
        serviceGroup: 'Handset Protection and Upgrade',
        cartConflict: false,
        currentSelection: false,
        existingSelection: false,
        taxInclusive: false,
        price: {
          priceSummary: {
            monthlyPrice: {
              listPrice: {
                amount: 5.0,
                display: '$5.00',
              },
              salePrice: {
                amount: 5.0,
                display: '$5.00',
              },
              contractTerm: 0,
              contractFrequency: 'MONTHLY',
            },
            payNowPrice: {
              listPrice: {
                amount: 0.0,
                display: '$0.00',
              },
              salePrice: {
                amount: 0.0,
                display: '$0.00',
              },
            },
          },
        },
        conflictSkus: ['P3606', 'P3605'],
      },
    ],
  };
  it('should render without crashing', () => {
    render(
      <DeviceProtection
        protectionOptions={mockDPOptions}
        setDeviceSelection={() => {
          ('');
        }}
        deviceSelection=''
        setIsShowValidationMessage={() => {
          ('');
        }}
        isShowValidationMessage={false}
      />,
    );

    expect(screen.getByTestId('device-protection')).toHaveTextContent('Protection<360>®');
    expect(screen.getByTestId('device-protection')).toHaveTextContent('$18.00');
    expect(screen.getByTestId('device-protection')).toHaveTextContent('McAfee® Security for T-Mobile');
    expect(screen.getByTestId('device-protection')).toHaveTextContent('$5.00');
  });

  it('should return checked radio button', async () => {
    render(
      <DeviceProtection
        protectionOptions={mockDPOptions}
        setDeviceSelection={() => {
          ('');
        }}
        deviceSelection='P3605'
        setIsShowValidationMessage={() => {
          ('');
        }}
        isShowValidationMessage={false}
      />,
    );
    await userEvent.click(screen.getByTestId('P3605'));
    expect(screen.getByTestId('P3605')).toBeChecked();
  });
  it('should disable conflict protection option and show conflict flag', async () => {
    render(
      <DeviceProtection
        protectionOptions={mockDPOptions}
        setDeviceSelection={() => {
          ('');
        }}
        deviceSelection=''
        setIsShowValidationMessage={() => {
          ('');
        }}
        isShowValidationMessage={false}
      />,
    );
    expect(screen.getByTestId('device-protection')).toHaveTextContent(HTMLConstants.PROTECTION_NOT_COMPATIBLE);
    expect(screen.getByTestId('P3605')).toBeDisabled();

    fireEvent.click(screen.getByTestId('INTELMCAF'), { target: { checked: true } });
    expect(screen.getByTestId('INTELMCAF')).toBeChecked();
  });
});
