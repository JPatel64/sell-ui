import { render, screen } from '@testing-library/react';
import { secureTokenService } from '@tmobile/atlas-util';
import React from 'react';
import { SecureWrapper } from './SecureWrapper';

describe('Secure Wrapper component', () => {
  it('should display child components when secure token call is successful', async () => {
    render(
      <SecureWrapper>
        <div data-testid='test'>testing 123</div>
      </SecureWrapper>,
    );
    await screen.findByTestId('test');
    expect(screen.getByTestId('test')).toHaveTextContent('testing 123');
  });

  it('should display error message if secure token call fails', async () => {
    jest.spyOn(secureTokenService, 'getToken').mockRejectedValueOnce({});
    render(
      <SecureWrapper>
        <div data-testid='test'>testing 123</div>
      </SecureWrapper>,
    );
    await screen.findByText('Could not get token details');
  });
});
