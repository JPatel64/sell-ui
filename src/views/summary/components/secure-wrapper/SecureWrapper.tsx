import { secureTokenService } from '@tmobile/atlas-util';
import React, { FunctionComponent, ReactNode } from 'react';
import { useAsync } from 'react-async-hook';
import { getAuthConfig } from '../../../../util/auth.util';

const getSecureToken = async () => {
  return await secureTokenService.getToken(getAuthConfig());
};

interface Props {
  children?: ReactNode;
}

export const SecureWrapper: FunctionComponent<Props> = ({ children }) => {
  const { loading, error } = useAsync(getSecureToken, []);

  return (
    <>
      <hr></hr>
      {!loading && <>{!error ? <>{children}</> : <p>Could not get token details</p>}</>}
    </>
  );
};
