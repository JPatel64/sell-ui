import { dispatchComplete, GlobalNavUtil, http } from '@tmobile/atlas-util';
import React, { FunctionComponent, useEffect, useState } from 'react';
import useStore from '../../../../store';
import {
  DECLINE_PROTECTION_VALUE,
  DEVICE_PROTECTION_URL,
  LINE_ID_URL,
  ZIP_CODE_URL,
} from '../../../../util/app.constants';
import { getGatewayUrl } from '../../../../util/auth.util';
import { IAddDeviceProtection, IDeviceProtection } from '../../interfaces/IDeviceProtection';
import DeviceProtection from '../deviceProtection/DeviceProtection';

const Summary: FunctionComponent = () => {
  const [deviceProtectionOptions, setDeviceProtectionOptions] = useState<IDeviceProtection>();
  const [deviceSelection, setDeviceSelection] = useState('');
  //const [previousSelection, setPreviousSelection] = useState("")
  //const [isLoaded, setIsLoaded] = useState(false)
  const [isShowValidationMessage, setIsShowValidationMessage] = useState(false);

  const { cartId, lineId } = useStore();
  const zipCode = GlobalNavUtil.getZipCode();

  useEffect(() => {
    http
      .get<IDeviceProtection>(
        `${getGatewayUrl()}` +
          DEVICE_PROTECTION_URL +
          `${cartId}` +
          LINE_ID_URL +
          `${lineId}` +
          ZIP_CODE_URL +
          `${zipCode}`,
      )
      .then((res) => {
        setDeviceProtectionOptions(res.data);
        // res.data.services.forEach(a => {
        //     if (a.currentSelection) {
        //         setPreviousSelection(a.offerId)
        //     }
        // })
        // setIsLoaded(true);
      })
      .catch((e) => console.error(e.message));
  }, []);

  const sendSaveAndCompleteEvent = async () => {
    if (deviceSelection === '') {
      setIsShowValidationMessage(true);
    } else {
      if (deviceSelection !== DECLINE_PROTECTION_VALUE) {
        try {
          await addDeviceProtection();
          dispatchComplete();
        } catch (e) {
          // eslint-disable-next-line no-console
          console.log('Failed to add/remove device protection');
        }
      } else {
        dispatchComplete();
      }
    }
  };

  const addDeviceProtection = async () => {
    const dpModel: IAddDeviceProtection = {
      addServices: { offerIds: [deviceSelection] },
      removeServices: undefined, //previousSelection === '' ? undefined : { offerIds: [previousSelection] },
      removeExistingServices: undefined,
    };

    try {
      await http.post(
        `${getGatewayUrl()}` +
          DEVICE_PROTECTION_URL +
          `${cartId}` +
          LINE_ID_URL +
          `${lineId}` +
          ZIP_CODE_URL +
          `${zipCode}`,
        dpModel,
      );
    } catch (e) {
      console.error(e);
      throw e;
    }
  };

  return (
    <div>
      {deviceProtectionOptions && (
        <div className='p-2'>
          <div className='p-grid'>
            <div className='p-col'>
              <DeviceProtection
                protectionOptions={deviceProtectionOptions}
                setDeviceSelection={setDeviceSelection}
                deviceSelection={deviceSelection}
                setIsShowValidationMessage={setIsShowValidationMessage}
                isShowValidationMessage={isShowValidationMessage}
              />
            </div>
          </div>
          <button data-testid='continue' className='btn btn-primary' type='button' onClick={sendSaveAndCompleteEvent}>
            Continue
          </button>
        </div>
      )}
    </div>
  );
};

export default Summary;
