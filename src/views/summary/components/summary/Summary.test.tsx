import { render, screen } from '@testing-library/react';
import { GlobalNavUtil, storageUtil } from '@tmobile/atlas-util';
import React from 'react';
import Summary from './Summary';

it('renders without crashing', async () => {
  jest.spyOn(GlobalNavUtil, 'getZipCode').mockReturnValue('30050');
  jest.spyOn(storageUtil, 'getCartId').mockReturnValue('12223232');
  jest.spyOn(storageUtil, 'getLineId').mockReturnValue('dd3321');

  render(<Summary />);
  await screen.findByText('Device Protection');
  await screen.findByText('Continue');
});
