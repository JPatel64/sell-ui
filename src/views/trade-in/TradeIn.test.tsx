import '@testing-library/jest-dom';
import { fireEvent, render, screen, waitFor } from '@testing-library/react';
import { GlobalNavUtil, loadExternalApplicationByUrl } from '@tmobile/atlas-util';
import React from 'react';
import HTMLConstants from '../../utils/html.constants';
import OptionSelection from './OptionSelection';

jest.mock('@tmobile/atlas-util');

describe('OptionSelection component', () => {
  it('should render without crashing', async () => {
    jest.spyOn(GlobalNavUtil, 'launchMicroApp').mockReturnValue();
    global.TRADE_IN_URL='test';
    render(<OptionSelection />);
    expect(screen.getByText(HTMLConstants.TRADE_IN_OPTION_HEADER)).toBeInTheDocument();
    expect(screen.getByText('Cancel')).toBeTruthy();
    const button = screen.getByTestId('continue-button');
    expect(button).toBeDisabled();
    fireEvent.click(screen.getByTestId('rdo-yes'));
    expect(button).not.toBeDisabled();

    await screen.findAllByTestId('cancel-button');
    await screen.findAllByTestId('continue-button');
    const buttons = screen.getAllByRole('button');
    buttons.forEach((button) => {
      fireEvent.click(button);
    });
  });
  
  it('should return checked radio button', async () => {
    render(<OptionSelection />);
    expect(screen.getByTestId('no-radioBtn')).toHaveTextContent('No');
    expect(screen.getByTestId('yes-radioBtn')).toHaveTextContent('Yes');
    fireEvent.click(screen.getByTestId('rdo-no'), { target: { checked: true } });
    fireEvent.click(screen.getByTestId('rdo-yes'), { target: { checked: true } });
  });
  
  it('should launch shop-now app on click of cancel button', async () => {
    render(<OptionSelection />);
    jest.spyOn(GlobalNavUtil, 'launchMicroApp');
    await screen.findByTestId('cancel-button');
    fireEvent.click(screen.queryByTestId('cancel-button'));
    await waitFor(() => {
      expect(GlobalNavUtil.launchMicroApp).toHaveBeenCalledWith('shop-now');
    });
  });
  
  it('should call loadExternalApplicationByUrl on click of continue button', async () => {
    global.TRADE_IN_URL='test';
    render(<OptionSelection />);
    fireEvent.click(screen.getByTestId('rdo-yes'));
    await screen.findByTestId('continue-button');
    fireEvent.click(screen.queryByTestId('continue-button'));
    await waitFor(() => {
      expect(loadExternalApplicationByUrl).toBeCalled();
    });
  });
});
