import { dispatchComplete, GlobalNavUtil, loadExternalApplicationByUrl } from '@tmobile/atlas-util';
import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router';
import useStore from '../../store';
import styles from './TradeIn.module.scss';

const TradeInView = () => {
  const [selectedOption, setSelectedOption] = useState('');
  const [showOptions, setShowOptions] = useState(true);
  const ACCOUNT_NUMBER = GlobalNavUtil.getAccountNumber();
  const EXCHANGE_TOKEN = GlobalNavUtil.getExchangeToken();
  const { flowType, cartId, lineId, msisdn } = useStore();
  const navigate = useNavigate();

  useEffect(() => {
    const tradeinElement = document.querySelector('atlas-trade-in');
    const data = {
      from: flowType,
      msisdn: msisdn,
      accountNumber: ACCOUNT_NUMBER,
      cartDetails: {
        cartId: cartId,
        lineId: lineId,
        exchangeToken: EXCHANGE_TOKEN,
      },
    };
    if (tradeinElement) {
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      (tradeinElement as any).data = JSON.stringify(data);
    }
  });

  const navigateToNextStep = async () => {
    if (selectedOption === '1') {
      setShowOptions(false);
      loadTradeInApp();
    } else {
      dispatchComplete();
    }
  };
  const loadTradeInApp = async () => {
    loadExternalApplicationByUrl('atlas_Trade_in', `${TRADE_IN_URL}`);
  };
  const navigateToShopNow = () => {
    navigate('/');
  };

  return (
    <>
      {showOptions ? (
        <div className={`${styles['os-container']} p-3`} data-testid='option-selection'>
          {/* <div className={styles['trade-in-header']}>{HTMLConstants.TRADE_IN_OPTION_HEADER}</div> */}
          <div data-testid='yes-radioBtn' className={`${styles['trade-in-option']} my-3`}>
            <input
              data-testid='rdo-yes'
              type='radio'
              name='trade-in-option'
              value='1'
              id='optionYes'
              checked={selectedOption === '1'}
              onChange={(e) => {
                setSelectedOption(e.currentTarget.value);
              }}
            />
            <label className={styles['option-text']} htmlFor='optionYes'>
              Yes
            </label>
          </div>
          <div data-testid='no-radioBtn' className={`${styles['trade-in-option']} my-3`}>
            <input
              data-testid='rdo-no'
              type='radio'
              name='trade-in-option'
              value='0'
              id='optionNo'
              checked={selectedOption === '0'}
              onChange={(e) => {
                setSelectedOption(e.currentTarget.value);
              }}
            />
            <label className={styles['option-text']} htmlFor='optionNo'>
              No
            </label>
          </div>
          <hr style={{ marginTop: '30px' }} />
          <div className={styles['buttonsDiv']}>
            <button
              data-testid='cancel-button'
              className={`btn btn-primary ${styles['buttons']}`}
              type='button'
              onClick={() => navigateToShopNow()}
            >
              Cancel
            </button>
            <button
              data-testid='continue-button'
              className={`btn btn-primary ${styles['buttons']}`}
              type='button'
              disabled={selectedOption === ''}
              onClick={() => navigateToNextStep()}
            >
              Continue
            </button>
          </div>
        </div>
      ) : (
        <>
          {/* eslint-disable-next-line @typescript-eslint/ban-ts-comment */}
          {/* @ts-ignore */}
          <atlas-trade-in flow='tradein-device'></atlas-trade-in>
        </>
      )}
    </>
  );
};

export default TradeInView;
