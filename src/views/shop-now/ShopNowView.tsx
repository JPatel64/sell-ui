import React from 'react';
import styles from '../../App.module.scss';
import { CustomerTokenProvider } from '../../context/CustomerTokenContext';
import HTMLConstants from '../../util/html.constants';
import LineDetails from '../../views/shop-now/components/line-details/LineDetails';

const ShopNowView = () => {
  return (
    <>
      <div className='flex flex-row justify-content-between align-items-center'>
        <h2 className='pl-4'>{HTMLConstants.SHOP_NOW}</h2>
        <div className='flex flex-column align-items-center pr-4'>
          <img src={'./assets/Cart.svg'} alt='cart' />
          <span className={`${styles['cart-text']} mt-2`}>Cart</span>
        </div>
      </div>
      <hr></hr>
      <CustomerTokenProvider>
        <LineDetails />
      </CustomerTokenProvider>
    </>
  );
};

export default ShopNowView;
