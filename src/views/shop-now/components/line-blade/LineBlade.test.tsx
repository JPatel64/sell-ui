import { fireEvent, render, screen, waitFor } from '@testing-library/react';
import { GlobalNavUtil, storageUtil } from '@tmobile/atlas-util';
import React from 'react';
import LineBlade from './LineBlade';

describe('Line blade', () => {
  beforeEach(() => {
    const mockLine = {
      msisdn: '1231231234',
      productType: 'Voice',
      isEligibleForUpgrade: true,
      subscriberName: 'Test Name',
    };
    render(<LineBlade line={mockLine} />);
  });

  it('should display details for line eligible for upgrade', () => {
    expect(screen.getByTestId('line-type')).toHaveTextContent('VOI');
    expect(screen.getByTestId('phone-number')).toHaveTextContent('(123) 123-1234');
    expect(screen.getByTestId('customer-name')).toHaveTextContent('Test Name');
    expect(screen.getByText('Upgrade')).toBeInTheDocument();
    expect(screen.getByText('Eligible')).toBeInTheDocument();
  });

  it('should launch upgrades shell when Eligible link is clicked', async () => {
    jest.spyOn(storageUtil, 'getDcpContextToken').mockReturnValue('token');
    jest.spyOn(storageUtil, 'setCartId');
    jest.spyOn(storageUtil, 'setLineId');
    jest.spyOn(storageUtil, 'setMsisdn');
    jest.spyOn(GlobalNavUtil, 'launchMicroApp');

    fireEvent.click(screen.getByTestId('upgrades-link'));

    await waitFor(() => {
      expect(storageUtil.getDcpContextToken).toHaveBeenCalled();
      expect(storageUtil.setCartId).toHaveBeenCalled();
      expect(storageUtil.setLineId).toHaveBeenCalled();
      expect(storageUtil.setMsisdn).toHaveBeenCalled();
      expect(GlobalNavUtil.launchMicroApp).toHaveBeenCalled();
    });
  });
});
