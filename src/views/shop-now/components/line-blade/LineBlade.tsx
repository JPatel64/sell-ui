import { GlobalNavUtil, storageUtil } from '@tmobile/atlas-util';
import React, { FunctionComponent } from 'react';
import { Outlet, useNavigate } from 'react-router';
import cartApi from '../../../../api/cart';
import tokenApi from '../../../../api/token';
import { useCustomerTokenContext } from '../../../../context/CustomerTokenContext';
import { ILine } from '../../../../models/cart.model';
import { ILineBladeDetails } from '../../../../models/line.model';
import useStore from '../../../../store';
import { UPGRADE } from '../../../../util/app.constants';
import { formatMobileNumber } from '../../../../util/format.util';
import HTMLConstants from '../../../../util/html.constants';
import styles from './LineBlade.module.scss';

export interface ILineBladeProps {
  line: ILineBladeDetails;
}

const LineBlade: FunctionComponent<ILineBladeProps> = ({ line }) => {
  const { customerTokenDetails } = useCustomerTokenContext();
  const formattedType = line.productType === 'Voice' ? 'VOI' : line.productType;
  const navigate = useNavigate();
  const { setCartId, setProfileId, setExtCartId, setMsisdn, setLineId, setFlowType } = useStore();

  // TODO: handle exceptions -- show error message?
  const launchUpgradesFlow = async () => {
    try {
      // check if token registered -- flag in shared store
      if (!storageUtil.getDcpContextToken()) {
        const { profileId } = await tokenApi.registerToken(customerTokenDetails);
        setProfileId(profileId);
      }

      const cart = await cartApi.getCart();

      setCartId(cart.id);
      setExtCartId(cart.extCartId);

      let lineId = cart?.lines?.find((cartLine: ILine) => cartLine.phoneNumber === line.msisdn)?.id;
      if (!lineId) {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        lineId = await cartApi.addLine(cart, line.msisdn, line.productType);
      }

      setLineId(lineId);
      setMsisdn(line.msisdn);
      setFlowType(UPGRADE.toUpperCase());

      navigate('/upgrades');
      GlobalNavUtil.launchMicroApp('upgrades-shell-atlas');
    } catch (error) {
      // TODO: Add error message banner or a toast
      // Show an error message
      // eslint-disable-next-line no-console
      console.error('Failed to launch the upgrades flow.');
    }
  };

  return (
    <>
      <div className='col-12' data-testid='line-blade'>
        <div className='grid'>
          <div className='col align-self-center ml-3' data-testid='line-type'>
            <span className={`${styles['product-type']} pr-3 pl-3 pb-1 pt-1`}>{formattedType}</span>
          </div>
          <div className='col'>
            <div className='pb-1 font-semibold' data-testid='phone-number'>
              {formatMobileNumber(line.msisdn)}
            </div>
            <div data-testid='customer-name'>{line.subscriberName}</div>
          </div>
          <div className='col'>
            {/* placeholder for Device name */}
            {/* placeholder for Device protection */}
          </div>
          <div className='col-1 mr-3'>
            <div className='font-semibold pb-1'>{HTMLConstants.UPGRADE}</div>
            {line.isEligibleForUpgrade ? (
              <div
                className={styles['tmo-link']}
                role='link'
                data-testid='upgrades-link'
                tabIndex={0}
                onClick={() => launchUpgradesFlow()}
              >
                {HTMLConstants.ELIGIBLE}
              </div>
            ) : (
              <div>
                <div>{HTMLConstants.NON_ELIGIBLE}</div>
                <div>{/* placeholder for reason */}</div>
              </div>
            )}
          </div>
        </div>
        <hr></hr>
      </div>
      <Outlet />
    </>
  );
};

export default LineBlade;
