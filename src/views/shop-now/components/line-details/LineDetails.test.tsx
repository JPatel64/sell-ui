import React from 'react';
import { render, screen } from '@testing-library/react';
import LineDetails from './LineDetails';

describe('Line Details', () => {
  it('should render line blades', async () => {
    render(<LineDetails />);
    await screen.findByTestId('line-blade');
    expect(screen.getAllByTestId('line-blade').length).toBe(1);
  });
});
