import { GlobalNavUtil, storageUtil } from '@tmobile/atlas-util';
import React, { useEffect } from 'react';
import { useAsync } from 'react-async-hook';
import eligibilityApi from '../../../../api/eligibility';
import { useCustomerTokenContext } from '../../../../context/CustomerTokenContext';
import { ICustomerTokenDetails } from '../../../../models/customer-token-details.model';
import { ILineBladeDetails, ILineDetails } from '../../../../models/line.model';
import { IUpgradeEligibilityDetails } from '../../../../models/upgrade-eligibility.model';
import HTMLConstants from '../../../../util/html.constants';
import LineBlade from '../line-blade/LineBlade';

const getLineDetails = async (customerTokenDetails: ICustomerTokenDetails) => {
  const ban = GlobalNavUtil.getAccountNumber();
  const lineDetails = GlobalNavUtil.getSelectedLines();
  const msisdns = lineDetails.map((line: ILineDetails) => line.lineId);
  try {
    const { upgradeEligibilityDetails } = await eligibilityApi.getUpgradeEligibility(
      ban,
      msisdns,
      customerTokenDetails,
    );
    return consolidateLineDetails(lineDetails, upgradeEligibilityDetails);
  } catch (error) {
    console.error(error);
  }
};

const consolidateLineDetails = (lineDetails: ILineDetails[], eligibilityDetails: IUpgradeEligibilityDetails[]) => {
  const lineEligibility = new Map();
  lineDetails.forEach((line) =>
    lineEligibility.set(line.lineId, { productType: line.productType, subscriberName: line.subscriberName }),
  );
  eligibilityDetails.forEach((line) =>
    lineEligibility.set(line.msisdn, { ...lineEligibility.get(line.msisdn), ...line }),
  );
  return Array.from(lineEligibility.values());
};

const LineDetails = () => {
  useEffect(() => {
    // Clear the state store when we enter the shop now ui so we don't have stale data.
    storageUtil.clear();
  }, []);

  const { customerTokenDetails } = useCustomerTokenContext();
  const { loading, result: lineDetails } = useAsync(getLineDetails, [customerTokenDetails]);

  return (
    <div className='grid pl-3 pr-3 mt-3'>
      <div className='col-12 pb-0'>
        <h5 className='mb-1 mt-1'>{HTMLConstants.LINES_AND_DEVICES}</h5>
        <hr></hr>
      </div>
      {!loading &&
        lineDetails &&
        lineDetails.map((line: ILineBladeDetails, index: number) => {
          return <LineBlade key={index} line={line} />;
        })}
    </div>
  );
};

export default LineDetails;
