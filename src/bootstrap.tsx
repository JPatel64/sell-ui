import React from 'react';
import ReactDOM from 'react-dom/client';
import { MemoryRouter, Route, Routes } from 'react-router-dom';
import App from './App';
import './index.scss';
import reportWebVitals from './reportWebVitals';
import CheckoutView from './views/checkout/CheckoutView';
import OrderConfirmation from './views/checkout/components/orderConfirmation/OrderConfirmation';
import Payment from './views/checkout/components/payment/Payment';
import Shipping from './views/checkout/components/shipping/Shipping';
import UpgradesView from './views/upgrades/UpgradesView';

const setup = async () => {
  if (process.env.NODE_ENV === 'development') {
    await import('../mocks/browser').then((browser) => {
      browser.worker.start();
    });
    await import('../mocks/mockStateStore').then((mockStateStore) => {
      mockStateStore.setupMockStateStore();
    });
  }
  return Promise.resolve();
};

setup().then(() => {
  class ShopNowElement extends HTMLElement {
    connectedCallback() {
      const root = ReactDOM.createRoot(this);

      root.render(
        <React.StrictMode>
          <MemoryRouter>
            <Routes>
              <Route path='/' element={<App />} />
              <Route path='/upgrades' element={<UpgradesView />}></Route>
              <Route path='/checkout' element={<CheckoutView />}>
                <Route path='shipping' element={<Shipping />} />
                <Route path='payment' element={<Payment />} />
                <Route path='order-confirmation' element={<OrderConfirmation />} />
              </Route>
            </Routes>
          </MemoryRouter>
        </React.StrictMode>,
      );
    }
  }

  if (!customElements.get('shop-now-element')) {
    customElements.define('shop-now-element', ShopNowElement);
  }
});

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
