export interface ISystem {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  type?: any;
  remoteEntry: string;
  exposedModule: string;
  remoteName: string;
  elementName?: string;
}
