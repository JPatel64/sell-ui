import { ICustomerTokenDetails } from './customer-token-details.model';

export interface IRegisterTokenResponse {
  exchangeToken: string;
  profileId: string;
  expiresIn: string;
}

export interface IRegisterTokenRequest {
  grantType: string;
  accountNumber: string;
  phoneNumber: string;
  customerTokenDetails: ICustomerTokenDetails;
}
