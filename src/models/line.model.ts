export interface ILineBladeDetails {
  msisdn: string;
  productType: string;
  subscriberName: string;
  isEligibleForUpgrade: boolean;
}

export interface ILineDetails {
  lineId: string;
  subscriberName: string;
  status: string;
  productType: string;
}

export interface IAddLineRequest {
  lineNumber: string;
  phoneNumber: string;
  lineType: string;
  transactionCode: string;
}

export interface IAddLineResponse {
  lineId: string;
}
