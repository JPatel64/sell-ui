/* eslint-disable @typescript-eslint/no-explicit-any */

export interface IRoute {
  name: string;
  path: string;
  ComponentName: () => JSX.Element;
  template?: any;
  command?: any;
  complete: boolean;
}
