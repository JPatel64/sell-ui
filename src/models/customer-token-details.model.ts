export interface ICustomerTokenDetailsResponse {
  customerTokenDetails: ICustomerTokenDetails;
}

export interface ICustomerTokenDetails {
  customerToken: ICustomerToken;
  hash: string;
}

interface ICustomerToken {
  accountOwnerName: string;
  accountType: string;
  applicationUserId: string;
  ban: string;
  expirationTime: number;
  interactionId: string;
  levelOfPermissions: string;
  msisdn: string;
  otpVerificationStatus: string;
  rslDealerId: string;
  rslValidationType: string;
  sourceApplication: string;
  storeId: string;
  verificationStatus: string;
  verifiedCustomerName: string;
}
