import { ICustomerTokenDetails } from './customer-token-details.model';

export interface IUpgradeEligibilityResponse {
  upgradeEligibilityDetails: IUpgradeEligibilityDetails[];
}

export interface IUpgradeEligibilityDetails {
  msisdn: string;
  isEligibleForUpgrade: boolean;
}

export interface IUpgradeEligibilityRequest {
  ban: string;
  msisdns: string[];
  customerTokenDetails?: ICustomerTokenDetails;
}
