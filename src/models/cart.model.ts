export interface ICart {
  id: string;
  extCartId: string;
  lines: ILine[];
}

export interface ILine {
  id: string;
  phoneNumber: string;
  lineNumber: string;
}
