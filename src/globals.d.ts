declare module '*.module.css';
declare module '*.module.scss';

declare namespace JSX {
  interface IntrinsicElements {
    'payments-access-management': any;
  }
}

declare module '*.png';
declare module '*.jpg';
declare module '*.jpeg';
declare module '*.svg';
declare module '*.gif';

declare let CATALOG_URL: string;
declare let SUMMARY_URL: string;
declare let TRADE_IN_URL: string;
declare let CART_URL: string;
