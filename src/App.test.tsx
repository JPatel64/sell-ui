import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';
import { secureTokenService } from '@tmobile/atlas-util';

describe('App - Shop Now Page', () => {
  it('should render page title and line details', async () => {
    render(<App />);
    const pageTitle = screen.getByText(/Shop now/);
    expect(pageTitle).toBeInTheDocument();
    await screen.findByText('Lines and devices');
    await screen.findByTestId('line-blade');
  });

  it('should not render line details if getToken fails', async () => {
    jest.spyOn(secureTokenService, 'getToken').mockRejectedValueOnce({});

    render(<App />);
    const pageTitle = screen.getByText(/Shop now/);
    expect(pageTitle).toBeInTheDocument();
    await screen.findByText('Could not get token details');
  });
});
