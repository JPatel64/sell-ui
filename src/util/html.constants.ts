// eslint-disable-next-line @typescript-eslint/no-explicit-any
const HTMLConstants: any = {
  DEVICE_PROTECTION: 'Device protection',
  MONTHLY_COST_TEXT: 'Monthly cost is determined by the device value.',
  PROTECTION_NOT_COMPATIBLE: 'Current protection plan incompatible with new device; select another protection option.',
  SELECT_PROTECTION_OPTION: 'No current device protection; select a protection option.',
  PROTECTION_NOT_AVAILABLE: 'Device protection not available for this device',
  NO_PROTECTION_PLAN: 'No protection options available',
  DECLINE_PROTECTION: 'Decline protection for this device',
  DECLINE_PROTECTION_DESC1: 'This option will result in the lack of device protection for this subscriber.',
  DECLINE_PROTECTION_DESC2: 'After 30 days this feature cannot be added on without a qualifying event.',
  SHOP_NOW: 'Shop now',
  LINES_AND_DEVICES: 'Lines and devices',
  UPGRADE: 'Upgrade',
  ELIGIBLE: 'Eligible',
  NON_ELIGIBLE: 'Non-eligible',
  TRADE_IN_OPTION_HEADER: 'Trade-in option',
};

export default HTMLConstants;
