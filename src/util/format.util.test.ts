import { formatCurrency } from './format.util';

describe('formatCurrency', () => {
  it('should format given number into formatted currency string', () => {
    expect(formatCurrency(10)).toBe('$10.00');
    expect(formatCurrency(1000)).toBe('$1,000.00');
  });
});
