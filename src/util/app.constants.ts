export const API_GATEWAY_LOCAL = 'http://localhost:8110';
export const UPGRADE_ELIGIBILITY_URL = '/eligibility/upgrade';
export const CARTS_BASE_URL = '/carts';
export const TOKEN_URL = '/token';
export const CUSTOMER_TOKEN_URL = '/extractCustomerToken';

export const LINE_TYPE_MAPPING: Record<string, string> = {
  Voice: 'HANDSET',
};
export const ACCESSORY = 'Accessory';
export const ACCESSORIES_URL = '/accessories/';
export const DEVICES_URL = '/devices/';
export const ADDITIONAL_DOWN_PAYMENT = 'Additional down payment';

export const CART_URL = '/cart/';
export const LINES_URL = '/lines/';
export const CHARGES = 'Charges';
export const CURRENT_EC_CREDIT_LIMIT = 'Current Equipment Credit limit';

export const DEPOSITS_CHARGES = 'Deposits and charges';
export const DETAILS = 'Details';
export const DISCOUNTS_AND_CREDITS = 'Discounts and credit';
export const DOWN_PAYMENT = 'Down payment:';
export const DUE_MONTHLY = 'Due monthly:';
export const DUE_TODAY = 'Due today:';

export const EC_AVAILABLE = 'Equipment Credit available';
export const EC_BALANCE = 'Equipment Credit balance';
export const EIP = 'EIP:';
export const ESTIMATED_DUE_TODAY = 'Estimated due today';
export const ESTIMATED_FINANCING = 'Estimated financing';

export const FINANCE = 'Finance';
export const FINANCED_AMOUNT = 'Financed amount:';
export const FULL_RETAIL_PRICE = 'FULL';

export const LINE_CONTRIBUTION = 'Line contribution';

export const MAX_POTENTIAL_EC = 'Maximum potential Equipment Credit';
export const MONTH = 'month';
export const MONTHS = 'months';
export const MONTHLY_PAYMENT = 'Monthly payment';
export const MONTHLY_RECURRING_CHARGES = 'Monthly recurring charges';

export const NA = 'N/A';
export const NAME = 'Name:';

export const PRICE = 'Price:';
export const PROTECTION = 'Protection:';
export const PROTECTION_DECLINE = 'Decline protection for this device';

export const QUANTITY = 'Quantity:';

export const SERVICES_AND_FEATURES = 'Services and features';
export const SUBTOTAL_PRETAX = 'Subtotal (pre-tax)';
export const SUPPORT_CHARGE = 'ACTIVATION';
export const SUPPORT_FEE = 'Support fee:';

export const TAX = 'Tax';
export const TERM = 'Term:';
export const TMOBILE = 'T-Mobile';
export const TOTAL_DUE_TODAY = 'Total due today (estimated)';
export const TOTAL_FINANCED_AMOUNT = 'Total financed amount';
export const TOTAL_DUE_MONTHLY = 'Total due monthly (estimated)';

export const UPGRADE = 'Upgrade';

export const DELETE_CONFIRMATION_PROMPT = 'Are you sure you want to delete?';
export const DEVICE_PENDING_PROMPT = 'You have pending device upgrade items for ';
export const DISCARD_WARNING_PROMPT = '. If you select continue, all items will be discarded.';
export const DEVICE_DELETED_PROMPT = 'Device upgrade has been deleted.';
export const MEG_GATEWAY_URL_EXTN = '/v1/cart-service';

export const DEVICE_PROTECTION_URL = '/summary/services?cartId=';
export const LINE_ID_URL = '&lineId=';
export const ZIP_CODE_URL = '&zipCode=';
export const DEVICE_PROTECTION_C2_LINK = 'https://c2.t-mobile.com/docs/DOC-442549';
export const DECLINE_PROTECTION_VALUE = 'decline';
