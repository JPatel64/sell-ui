import create from 'zustand';
import { IRoute } from '../models/routes';
import CartView from '../views/cart/CartView';
import AccessoryCatalog from '../views/catalog/components/accessory-catalog/AccessoryCatalog';
import DeviceCatalog from '../views/catalog/components/device-catalog/DeviceCatalog';
import SummaryView from '../views/summary/SummaryView';
import TradeInView from '../views/trade-in/TradeInView';

interface State {
  upgradeRoutes: IRoute[];
  jumpRoutes: IRoute[];
  jodRoutes: IRoute[];
  flowType: string;
  profileId: string;
  extCartId: string;
  msisdn: string;
  selectedSku: string;
  cartId: string;
  lineId: string;
  zipCode: string;
  setSelectedSku: (sku: string) => void;
  setProfileId: (id: string) => void;
  setExtCartId: (id: string) => void;
  setMsisdn: (id: string) => void;
  setFlowType: (type: string) => void;
  setCartId: (cartId: string) => void;
  setLineId: (lineId: string) => void;
  setZipCode: (zipCode: string) => void;
}

const useStore = create<State>((set) => ({
  // initial state
  flowType: '',
  cartId: '',
  extCartId: '',
  profileId: '',
  msisdn: '',
  lineId: '',
  zipCode: '',
  selectedSku: '',
  setFlowType: (type: string) => {
    set(() => ({
      flowType: type,
    }));
  },
  setSelectedSku: (sku: string) => {
    set(() => ({
      selectedSku: sku,
    }));
  },
  setCartId: (id: string) => {
    set(() => ({
      cartId: id,
    }));
  },
  setLineId: (id: string) => {
    set(() => ({
      lineId: id,
    }));
  },
  setZipCode: (zipCode: string) => {
    set(() => ({
      zipCode: zipCode,
    }));
  },
  setProfileId: (profileId: string) => {
    set(() => ({
      profileId: profileId,
    }));
  },
  setExtCartId: (extCartId: string) => {
    set(() => ({
      extCartId: extCartId,
    }));
  },
  setMsisdn: (msisdn: string) => {
    set(() => ({
      msisdn: msisdn,
    }));
  },
  upgradeRoutes: [
    {
      name: 'Devices',
      path: '/devices',
      ComponentName: DeviceCatalog,
      complete: false,
    },
    {
      name: 'Summary',
      path: '/summary',
      ComponentName: SummaryView,
      complete: false,
    },
    {
      name: 'Trade In',
      path: '/trade-in',
      ComponentName: TradeInView,
      complete: false,
    },
    {
      name: 'Accessories',
      path: '/accessories',
      ComponentName: AccessoryCatalog,
      complete: false,
    },
    {
      name: 'Cart',
      path: '/cart',
      ComponentName: CartView,
      complete: false,
    },
  ],
  jumpRoutes: [
    {
      name: 'Trade In',
      path: '/trade-in',
      ComponentName: TradeInView,
      complete: false,
    },
    {
      name: 'Devices',
      path: '/devices',
      ComponentName: DeviceCatalog,
      complete: false,
    },
    {
      name: 'Summary',
      path: '/summary',
      ComponentName: SummaryView,
      complete: false,
    },
    {
      name: 'Accessories',
      path: '/accessories',
      ComponentName: AccessoryCatalog,
      complete: false,
    },
    {
      name: 'Cart',
      path: '/cart',
      ComponentName: CartView,
      complete: false,
    },
  ],
  jodRoutes: [],
}));

export default useStore;
