import { http } from '@tmobile/atlas-util';
import { AxiosResponse } from 'axios';
import { ICart, ILine } from '../models/cart.model';
import { IAddLineResponse } from '../models/line.model';
import { CARTS_BASE_URL, LINES_URL, LINE_TYPE_MAPPING, UPGRADE } from '../util/app.constants';
import { getGatewayUrl } from '../util/auth.util';

const getCart = async () => {
  const cartResponse: AxiosResponse<ICart> = await http.get(`${getGatewayUrl()}${CARTS_BASE_URL}`);
  return cartResponse.data;
};

const addLine = async (cart: ICart, msisdn: string, productType: string) => {
  const response: AxiosResponse<IAddLineResponse> = await http.post(
    `${getGatewayUrl()}${CARTS_BASE_URL}/${cart.id}${LINES_URL}`,
    {
      lineNumber: getLineNumber(cart.lines),
      phoneNumber: msisdn,
      lineType: LINE_TYPE_MAPPING[productType],
      transactionCode: UPGRADE,
    },
  );
  return response.data?.lineId;
};

// need to manually calculate next lineNumber from UI -- pending change on DCP side to autogenerate lineNumber
const getLineNumber = (lines: ILine[]): number => {
  if (lines && lines.length > 0) {
    return Math.max(...lines.map((line) => Number(line.lineNumber))) + 1;
  } else {
    return 1;
  }
};

export default {
  getCart,
  addLine,
};
