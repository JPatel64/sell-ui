import { http } from '@tmobile/atlas-util';
import { AxiosResponse } from 'axios';
import { ICustomerTokenDetails } from '../models/customer-token-details.model';
import { IUpgradeEligibilityRequest, IUpgradeEligibilityResponse } from '../models/upgrade-eligibility.model';
import { UPGRADE_ELIGIBILITY_URL } from '../util/app.constants';
import { getGatewayUrl } from '../util/auth.util';

const getUpgradeEligibility = async (ban: string, msisdns: string[], customerTokenDetails: ICustomerTokenDetails) => {
  const request: IUpgradeEligibilityRequest = {
    ban,
    msisdns,
    customerTokenDetails,
  };
  const response: AxiosResponse<IUpgradeEligibilityResponse> = await http.post(
    `${getGatewayUrl()}${UPGRADE_ELIGIBILITY_URL}`,
    request,
  );
  return response.data;
};

export default { getUpgradeEligibility };
