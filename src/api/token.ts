import { GlobalNavUtil, http } from '@tmobile/atlas-util';
import { AxiosResponse } from 'axios';
import { ICustomerTokenDetails } from '../models/customer-token-details.model';
import { IRegisterTokenRequest, IRegisterTokenResponse } from '../models/token.model';
import { CUSTOMER_TOKEN_URL, TOKEN_URL } from '../util/app.constants';
import { getGatewayUrl } from '../util/auth.util';

const registerToken = async (customerTokenDetails: ICustomerTokenDetails) => {
  const registerTokenRequest: IRegisterTokenRequest = {
    grantType: 'password',
    accountNumber: customerTokenDetails.customerToken.ban,
    phoneNumber: customerTokenDetails.customerToken.msisdn,
    customerTokenDetails,
  };
  const response: AxiosResponse<IRegisterTokenResponse> = await http.post(
    `${getGatewayUrl()}${TOKEN_URL}`,
    registerTokenRequest,
  );

  return { exchangeToken: response.data.exchangeToken, profileId: response.data.profileId };
};

const getCustomerTokenDetails = async () => {
  const { data } = await http.post(`${getGatewayUrl()}${CUSTOMER_TOKEN_URL}`, {
    token: GlobalNavUtil.getCustomerInFocusToken(),
  });
  return data;
};

export default {
  registerToken,
  getCustomerTokenDetails,
};
