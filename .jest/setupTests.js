import '@testing-library/jest-dom';
import '@testing-library/jest-dom/extend-expect';
import { setupMockStateStore } from '../mocks/mockStateStore';
import { server } from '../mocks/server';

class LocalStorageMock {
  constructor() {
    this.store = {};
  }
  clear() {
    this.store = {};
  }
  getItem(key) {
    return this.store[key] || null;
  }
  setItem(key, value) {
    this.store[key] = value.toString();
  }
  removeItem(key) {
    delete this.store[key];
  }
}

// jest test environment expects these to exist
Object.defineProperty(window, 'localStorage', {
  value: new LocalStorageMock(),
});

beforeAll(() => {
  // Enable the mocking in tests.
  server.listen();

  // mock global nav state store
  setupMockStateStore();
});

beforeEach(() => {
  jest.resetModules();
});

afterEach(() => {
  // Reset any runtime handlers tests may use.
  server.resetHandlers();
});

afterAll(() => {
  // Clean up once the tests are done.
  server.close();
});
